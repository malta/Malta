# Malta {#PackageMalta}

Malta is the software for MALTA. It requires MaltaDAQ.

## Applications

- MaltaAnalogScan: Application to perform an analog scan on MALTA. Inject a known charge to a range of pixels.
- MaltaThresholdScan: Application to perform a threshold scan on MALTA. Inject an increasing value of charge to a range of pixels, fit an s-curve, and extract the threshold and noise from it.
- MaltaNoiseScan: Application to perform a noise scan. Iteratively run random triggers as a function of IDB until the noisy pixels have been identified and/or masked.
- MaltaTapCalibration: Application to perform a tap calibration. Change the values of the taps for the firmware and produce a plot that shows the position of the hits vs the tap value.

## Libraries

- Malta: Library containing DAQ classes for MALTA

### Malta library

- Malta: Control MALTA
- MaltaData: description of the data of MALTA
- MaltaTree: write MALTA data to root files
- MaltaModule: class to run MALTA as a plane in MaltaMultiDAQ
- MaltaSlowControl: class to encode/decode the slow control commands