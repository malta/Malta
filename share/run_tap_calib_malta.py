#!/usr/bin/env python

import os
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-a","--address",help="connection string udp://host:port",default="udp://ep-ade-gw-02.cern.ch:50008")
parser.add_argument("-c","--chip",help="WXRX",default="W12R19")
parser.add_argument("-C","--config",help="configuration file with dac values",default="configs/SPS.txt")
parser.add_argument("-o","--output",help="describe the measurement",default="longCable")
parser.add_argument("-x0",help="first column",type=int,default=510)
parser.add_argument("-x1",help="last column",type=int,default=511)
parser.add_argument("-y0",help="first row",type=int,default=504)
parser.add_argument("-y1",help="last row",type=int,default=511)
parser.add_argument("--counts",help="iterations", default=1,type=int)
args=parser.parse_args()


pixels=[]
for col in range(args.x0,args.x1+1):
  for row in range(args.y0,args.y1+1):
    pixels.append([col,row])
    pass
  pass


for count in range(args.counts):
  for p in pixels:
    cmd  = "MaltaTapCalibration -n 200 "
    cmd += " -c "+args.config
    cmd += " -a "+args.address
    cmd += " -f "+args.chip
    cmd += " -o "+args.output+"_calib_run"
    if count==0:
      cmd += " -x "+str(p[0])+" -y "+str(p[1])+" -w calib_"+str(count)+".txt"
    else:
      cmd += " -x "+str(p[0])+" -y "+str(p[1])+" -w calib_"+str(count)+".txt -i calib_"+str(count-1)+".txt"
    print(cmd)
    os.system(cmd)


