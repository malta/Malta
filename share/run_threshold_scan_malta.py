#!/usr/bin/env python

import os
import argparse

parser=argparse.ArgumentParser("MALTA threshold scan helper")
parser.add_argument("-a","--address",help="connection string udp://host:port",default="udp://ep-ade-gw-02.cern.ch:50008")
parser.add_argument("-c","--chip",help="WXRX",default="W12R19")
parser.add_argument("-C","--config",help="configuration file with dac values",default="configs/default.txt")
parser.add_argument("-T","--tag",help="describe the measurement",default="COLD_ITHR_120_IDB_120_SUB6")
parser.add_argument("-t","--triggers",help="number of triggers",default="300")
parser.add_argument("-x0","--col0", help="first column [0-511]",default="160")
parser.add_argument("-nx","--ncols",help="number of columns [1-512]",default="32")
parser.add_argument("-y0","--row0", help="first row [0-511]",default="224")
parser.add_argument("-ny","--nrows",help="number of rows [1-512]",default="32")
parser.add_argument("-v","--verbose",help="turn on verbose mode",action="store_true")
parser.add_argument("-p0","--parMin",help="VLOW (5-127)",default="30")
parser.add_argument("-p1","--parMax",help="VHIGH (5-127)",default="89")
parser.add_argument("-s","--parStep",help="Parameter step",default="1")
args=parser.parse_args()

cmd = "MaltaThresholdScan " 
cmd += " -a "+args.address # address
cmd += " -o "+args.chip+"_"+args.tag # output
cmd += " -f "+args.chip # basefolder
cmd += " -l "+args.parMin # paramMin
cmd += " -h "+args.parMax # paramMax
cmd += " -s "+args.parStep # paramStep
cmd += " -r %s %s %s %s" % (args.col0,args.ncols,args.row0,args.nrows) # region [xmin(0-511) ymin(>287) No.y (0-223)] ### a b c d : starting pixel (a c), pulsing b cols and d rows
cmd += " -t "+args.triggers # trigger
cmd += " -q" # quiet 
cmd += " -F" # fast scan
cmd += (" -v" if args.verbose else "") # verbose
cmd += " -c "+args.config # config file

print(cmd)
os.system(cmd)
