#include "cmdl/cmdline.h"
#include "Malta/MaltaTree.h"
#include "Malta/MaltaData.h"
#include <cmdl/cmdargs.h>
#include "TCanvas.h"
#include "TH2D.h"
#include "TFile.h"
#include "TROOT.h"
#include "TStyle.h"

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <utility>
#include <algorithm>
#include <libgen.h>
#include <filesystem>

using namespace std;

int main(int argc, char *argv[]){
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr     cInput(   'f',"input","input_file","output file",CmdArg::isREQ);
  CmdArgStr     cOutput(  'o',"output","output_file","output file");
  CmdArgBool    cDupRem(  'd',"duplicateRemoval","Remove duplicates");
  CmdArgBool    cNoShow(  'n',"noshow","don't show at the end");
  CmdArgIntList cMask(    'm',"mask","list","list of pairs");


  CmdLine cmdl(*argv,&cVerbose,&cInput,&cOutput,&cDupRem,&cMask,&cNoShow,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  cout << "#####################################" << endl
       << "# Welcome to MALTA Plotting         #" << endl
       << "#####################################" << endl;
  
  gROOT->SetMacroPath((getenv("ITK_INST_PATH")+string("/share/macros")).c_str());
  if(gROOT->Macro("AtlasStyle.C")){cout << "Loading ATLAS Style" << endl;}
  
  string sinput(cInput);
  filesystem::path pinput(sinput);
  if(!filesystem::exists(pinput)){
    cout << "Input file does not exist: " << sinput << endl;
    return 0;
  }
  
  string defout="Plots/PixelHitMap_"+string(pinput.stem())+".root";
  string soutput=(cOutput.flags()&CmdArg::GIVEN?string(cOutput):defout);
  
  vector<pair<uint32_t,uint32_t> > masks;
  for(uint32_t i=0; i<cMask.count(); i+=2){
    int col=cMask[i];
    int row=cMask[i+1];
    masks.push_back(make_pair<uint32_t,uint32_t>(col,row));
  }

  cout << "Creating new file: " << soutput << endl;
  filesystem::path p(soutput);
  if(!filesystem::exists(p.parent_path()) && !p.parent_path().empty()){
    cout << "Create output path: " << p.parent_path() << endl;
    filesystem::create_directories(p.parent_path());
  }
  TFile * oFile = new TFile(soutput.c_str(),"RECREATE");
  TCanvas * cPixelHitMap = new TCanvas("cPixelHitMap","PixelHitMap",600,600);
  cPixelHitMap->SetRightMargin(0.11);
  TH2D* hPixelHitMap = new TH2D("hPixelHitMap","PixelHitMap",512,-0.5,511.5,512,-0.5,511.5);
  hPixelHitMap->SetTitle("Frequency;Pixel PosX;Pixel PosY");

  MaltaTree * tree = new MaltaTree();
  tree->Open(sinput,"READ");

  chrono::steady_clock::time_point t0 = std::chrono::steady_clock::now();
  while(tree->Next()){
    if(tree->GetIsDuplicate()==1){continue;}
    MaltaData * md=tree->Get();
    for(uint32_t hit=0;hit<md->getNhits();hit++){
      bool mask=false;
      for(uint32_t p=0;p<masks.size();p++){
	if(md->getHitColumn(hit)==masks[p].first && md->getHitRow(hit)==masks[p].second){mask=true;break;}
      }
      if(mask) continue;
      hPixelHitMap->Fill(md->getHitColumn(hit),md->getHitRow(hit));
    }
  }
  chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
  cout << "Elapsed time: " << chrono::duration_cast<chrono::seconds>(t1-t0).count() << " [s]" << endl;
  tree->Close();
  delete tree;

  //print "Total event number: %s"%len(dcolumn)
  //print "Number of events after duplication: %s"%(eventsPassed)
  //print "Number of pixel hits after duplication: %s"%(hPixelHitMap.Integral())
 
  cPixelHitMap->SetLogz();
  hPixelHitMap->Draw("COLZ");
  hPixelHitMap->SetMinimum(1.);
  hPixelHitMap->SetMaximum(hPixelHitMap->GetMaximum()*0.03);
  cPixelHitMap->Update();

  string oName=p.replace_extension(".pdf");
  cout<<"Save the plot as PDF: " << oName << endl;
  cPixelHitMap->SaveAs(oName.c_str());
 
  oFile->cd();
  oFile->WriteObject(hPixelHitMap,"Map2D");
  cPixelHitMap->Write();
  oFile->Close();

  if(!cNoShow){
    ostringstream os;
    os << "evince " << oName << "&";
    system(os.str().c_str());
  }
  cout << "Cleaning the house" << endl;
  delete oFile;

  cout << "Have a nice day" << endl;
  return 0;
}

