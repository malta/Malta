/************************************
 * PyMaltaData
 * Brief: Python module for MaltaData
 *
 * Author: Carlos.Solans@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "Malta/MaltaData.h"
#include <iostream>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif


#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000


/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MaltaData *obj;
  std::vector<uint32_t> vec;
} PyMaltaData;

static int _PyMaltaData_init(PyMaltaData *self)
{
  self->obj = new MaltaData();
  return 0;
}

static void _PyMaltaData_dealloc(PyMaltaData *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyMaltaData_setHit(PyMaltaData *self, PyObject *args)
{
  uint32_t col;
  uint32_t row;
  if (!PyArg_ParseTuple(args, (char *) "II",&col,&row)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setHit(col,row);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setRefbit(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setRefbit(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setPixel(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setPixel(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setGroup(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setGroup(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setParity(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setParity(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setDelay(PyMaltaData *self, PyObject *args)
{
  uint32_t val;
  if (!PyArg_ParseTuple(args, (char *) "I",&val)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setDelay(val);
  Py_INCREF(Py_None);
  return Py_None;
}


PyObject * _PyMaltaData_setDcolumn(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setDcolumn(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setChipid(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setChipid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setChipbcid(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setChipbcid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setPhase(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
      return Py_None;
  }
  self->obj->setPhase(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setWinid(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWinid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setBcid(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setBcid(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setL1id(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setL1id(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setWord1(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWord1(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_setWord2(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->setWord2(pos);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_getRefbit(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getRefbit();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}


PyObject * _PyMaltaData_getPixel(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getPixel();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getGroup(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getGroup();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getParity(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getParity();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getDelay(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getDelay();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getDcolumn(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getDcolumn();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getBcid(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getBcid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getChipid(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getChipid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getChipbcid(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getChipbcid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getPhase(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getPhase();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getWinid(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWinid();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getL1id(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getL1id();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getWord1(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWord1();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getWord2(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getWord2();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getNhits(PyMaltaData *self)
{
  PyObject *py_ret;
  int ret = self->obj->getNhits();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getHitRow(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }
    
  PyObject *py_ret;
  int ret = self->obj->getHitRow(pos);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_getHitColumn(PyMaltaData *self, PyObject *args)
{
  uint32_t pos;
  if (!PyArg_ParseTuple(args, (char *) "I",&pos)){
    Py_INCREF(Py_None);
    return Py_None;
  }

  PyObject *py_ret;
  int ret = self->obj->getHitColumn(pos);
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaData_pack(PyMaltaData *self)
{
  self->obj->pack();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_unpack(PyMaltaData *self, PyObject *args)
{
  self->obj->unPack();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyMaltaData_toString(PyMaltaData *self)
{
    PyObject *py_ret;
    py_ret=PyUnicode_FromString(self->obj->toString().c_str());
    return py_ret;
}

PyObject * _PyMaltaData_getInfo(PyMaltaData *self)
{
    PyObject *py_ret;
    py_ret=PyUnicode_FromString(self->obj->getInfo().c_str());
    return py_ret;
}


static PyMethodDef PyMaltaData_methods[] = {
    {(char *) "setHit",(PyCFunction) _PyMaltaData_setHit, METH_VARARGS, NULL },
    {(char *) "setRefbit",(PyCFunction) _PyMaltaData_setRefbit, METH_VARARGS, NULL },
    {(char *) "setPixel",(PyCFunction) _PyMaltaData_setPixel, METH_VARARGS, NULL },
    {(char *) "setGroup",(PyCFunction) _PyMaltaData_setGroup, METH_VARARGS, NULL },
    {(char *) "setParity",(PyCFunction) _PyMaltaData_setParity, METH_VARARGS, NULL },
    {(char *) "setDelay",(PyCFunction) _PyMaltaData_setDelay, METH_VARARGS, NULL },
    {(char *) "setDcolumn",(PyCFunction) _PyMaltaData_setDcolumn,  METH_VARARGS, NULL },
    {(char *) "setChipid",(PyCFunction) _PyMaltaData_setChipid, METH_VARARGS, NULL },
    {(char *) "setChipbcid",(PyCFunction) _PyMaltaData_setChipbcid, METH_VARARGS, NULL },
    {(char *) "setPhase",(PyCFunction) _PyMaltaData_setPhase, METH_VARARGS, NULL },
    {(char *) "setWinid",(PyCFunction) _PyMaltaData_setWinid, METH_VARARGS, NULL },
    {(char *) "setBcid",(PyCFunction) _PyMaltaData_setBcid, METH_VARARGS, NULL },
    {(char *) "setL1id",(PyCFunction) _PyMaltaData_setL1id, METH_VARARGS, NULL },
    {(char *) "setWord1",(PyCFunction) _PyMaltaData_setWord1, METH_VARARGS, NULL },
    {(char *) "setWord2",(PyCFunction) _PyMaltaData_setWord2, METH_VARARGS, NULL },
    {(char *) "getRefbit",(PyCFunction) _PyMaltaData_getRefbit,  METH_NOARGS, NULL },
    {(char *) "getPixel",(PyCFunction) _PyMaltaData_getPixel,  METH_NOARGS, NULL },
    {(char *) "getGroup",(PyCFunction) _PyMaltaData_getGroup,  METH_NOARGS, NULL },
    {(char *) "getParity",(PyCFunction) _PyMaltaData_getParity,  METH_NOARGS, NULL },
    {(char *) "getDelay",(PyCFunction) _PyMaltaData_getDelay,  METH_NOARGS, NULL },
    {(char *) "getDcolumn",(PyCFunction) _PyMaltaData_getDcolumn,  METH_NOARGS, NULL },
    {(char *) "getBcid",(PyCFunction) _PyMaltaData_getBcid,  METH_NOARGS, NULL },
    {(char *) "getChipid",(PyCFunction) _PyMaltaData_getChipid,  METH_NOARGS, NULL },
    {(char *) "getChipbcid",(PyCFunction) _PyMaltaData_getChipbcid,  METH_NOARGS, NULL },
    {(char *) "getPhase",(PyCFunction) _PyMaltaData_getPhase,  METH_NOARGS, NULL },
    {(char *) "getWinid",(PyCFunction) _PyMaltaData_getWinid,  METH_NOARGS, NULL },
    {(char *) "getL1id",(PyCFunction) _PyMaltaData_getL1id,  METH_NOARGS, NULL },
    {(char *) "getWord1",(PyCFunction) _PyMaltaData_getWord1,  METH_NOARGS, NULL },
    {(char *) "getWord2",(PyCFunction) _PyMaltaData_getWord2,  METH_NOARGS, NULL },
    {(char *) "getNhits",(PyCFunction) _PyMaltaData_getNhits,  METH_NOARGS, NULL },
    {(char *) "getHitColumn",(PyCFunction) _PyMaltaData_getHitColumn,  METH_VARARGS, NULL },
    {(char *) "getHitRow",(PyCFunction) _PyMaltaData_getHitRow,  METH_VARARGS, NULL },
    {(char *) "toString",(PyCFunction) _PyMaltaData_toString,  METH_NOARGS, NULL },
    {(char *) "getInfo",(PyCFunction) _PyMaltaData_getInfo,  METH_NOARGS, NULL },
    {(char *) "pack",(PyCFunction) _PyMaltaData_pack,  METH_NOARGS, NULL },
    {(char *) "unpack",(PyCFunction) _PyMaltaData_unpack,  METH_NOARGS, NULL },
    {NULL, NULL, 0, NULL}
};

PyTypeObject PyMaltaData_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMaltaData.MaltaData",            /* tp_name */
    sizeof(PyMaltaData),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMaltaData_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    NULL,                       /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMaltaData_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMaltaData_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	,0                                   /* tp_version_tag */
#endif
};

static PyMethodDef Module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMaltaData)
{
    PyObject *m;
    MOD_DEF(m, "PyMaltaData", NULL, Module_methods);
    if(PyType_Ready(&PyMaltaData_Type)<0){
      return MOD_ERROR;
    }
    PyModule_AddObject(m, (char *) "MaltaData", (PyObject *) &PyMaltaData_Type);
    return MOD_RETURN(m);
}
