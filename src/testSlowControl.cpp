#include "ipbus/Uhal.h"
#include "Malta/MaltaSlowControl.h"

#include <iostream>
#include <bitset>

using namespace std;

int main(){

  unsigned int nClockCycles = 0;
  unsigned int registerID = 0;
  unsigned int pixelCol = 0;
  unsigned int pixelHor = 0;
  unsigned int pixelDiag = 0;
  unsigned int doubleColumn = 0;
  unsigned int picoseconds = 0;
  unsigned int DACvalue = 0;
  unsigned int LVDSvalue = 0;
  string preEmphasisType = "";
  bool on = false;
  bool powerSwitchLeftOn = false;
  bool powerSwitchRightOn = false;
  unsigned int registerValue = 0;

  MaltaSlowControl MSC;

  ////////
  // reset
  ////////
  cout << "+++++++ reset" << endl;

  // resetCMU
  cout << __PRETTY_FUNCTION__ << ": resetCMU() = " << hex << "0x" << MSC.resetCMU() << " = " << bitset<16>(MSC.resetCMU()) << endl;

  // generateReset
  nClockCycles = 37;
  cout << __PRETTY_FUNCTION__ << ": generateReset(" << dec << nClockCycles << ") = " << hex << "0x" << MSC.generateReset(nClockCycles) << " = " << bitset<16>(MSC.generateReset(nClockCycles)) << endl;

  // resetSCLRRegister, register ID < 16
  registerID = 11;
  cout << __PRETTY_FUNCTION__ << ": resetSCLRRegister(" << dec << registerID << ") = " << hex << "0x" << MSC.resetSCLRRegister(registerID) << " = " << bitset<16>(MSC.resetSCLRRegister(registerID)) << endl;

  // resetSCLRRegister, register ID >= 16
  registerID = 26;
  cout << __PRETTY_FUNCTION__ << ": resetSCLRRegister(" << dec << registerID << ") = " << hex << "0x" << MSC.resetSCLRRegister(registerID) << " = " << bitset<16>(MSC.resetSCLRRegister(registerID)) << endl;

  ///////
  // mask
  ///////
  cout << "+++++++ mask" << endl;

  // maskPixelCol
  pixelCol = 376;
  cout << __PRETTY_FUNCTION__ << ": maskPixelCol(" << dec << pixelCol << ")    = " << hex << "0x" << MSC.maskPixelCol(pixelCol) << " = " << bitset<16>(MSC.maskPixelCol(pixelCol)) << endl;

  // maskPixelHor
  pixelHor = 159;
  cout << __PRETTY_FUNCTION__ << ": maskPixelHor(" << dec << pixelHor << ")    = " << hex << "0x" << MSC.maskPixelHor(pixelHor) << " = " << bitset<16>(MSC.maskPixelHor(pixelHor)) << endl;

  // maskPixelDiag
  pixelDiag = 444;
  cout << __PRETTY_FUNCTION__ << ": maskPixelDiag(" << dec << pixelDiag << ")   = " << hex << "0x" << MSC.maskPixelDiag(pixelDiag) << " = " << bitset<16>(MSC.maskPixelDiag(pixelDiag)) << endl;

  // for(unsigned int pix=0; pix<512; pix++) {
  //     cout << "ipb.Write(5," << hex << "0x" << MSC.maskPixelCol(pix) << "); " << endl;
  //     cout << "ipb.Write(5," << hex << "0x" << MSC.maskPixelHor(pix) << "); " << endl;
  //     cout << "ipb.Write(5," << hex << "0x" << MSC.maskPixelDiag(pix) << "); " << endl;
  //   }

  for(unsigned int pix=0; pix<512; pix++) {
    cout << "ipb.Write(5," << hex << "0x" << MSC.unmaskPixelCol(pix) << "); " << endl;
    cout << "ipb.Write(5," << hex << "0x" << MSC.unmaskPixelHor(pix) << "); " << endl;
    cout << "ipb.Write(5," << hex << "0x" << MSC.unmaskPixelDiag(pix) << "); " << endl;
  }
         
  // unmaskPixelCol
  pixelCol = 376;
  cout << __PRETTY_FUNCTION__ << ": unmaskPixelCol(" << dec << pixelCol << ")  = " << hex << "0x" << MSC.unmaskPixelCol(pixelCol) << " = " << bitset<16>(MSC.unmaskPixelCol(pixelCol)) << endl;

  // unmaskPixelHor
  pixelHor = 159;
  cout << __PRETTY_FUNCTION__ << ": unmaskPixelHor(" << dec << pixelHor << ")  = " << hex << "0x" << MSC.unmaskPixelHor(pixelHor) << " = " << bitset<16>(MSC.unmaskPixelHor(pixelHor)) << endl;

  // unmaskPixelDiag
  pixelDiag = 444;
  cout << __PRETTY_FUNCTION__ << ": unmaskPixelDiag(" << dec << pixelDiag << ") = " << hex << "0x" << MSC.unmaskPixelDiag(pixelDiag) << " = " << bitset<16>(MSC.unmaskPixelDiag(pixelDiag)) << endl;

  // maskDoubleColumn
  doubleColumn = 93;
  cout << __PRETTY_FUNCTION__ << ": maskDoubleColumn(" << dec << doubleColumn << ")   = " << hex << "0x" << MSC.maskDoubleColumn(doubleColumn) << " = " << bitset<16>(MSC.maskDoubleColumn(doubleColumn)) << endl;

  // unmaskDoubleColumn
  doubleColumn = 93;
  cout << __PRETTY_FUNCTION__ << ": unmaskDoubleColumn(" << dec << doubleColumn << ") = " << hex << "0x" << MSC.unmaskDoubleColumn(doubleColumn) << " = " << bitset<16>(MSC.unmaskDoubleColumn(doubleColumn)) << endl;

  ////////
  // pulse
  //////// 
  cout << "+++++++ pulse" << endl;

  // enablePulsePixelCol
  pixelCol = 376;
  cout << __PRETTY_FUNCTION__ << ": enablePulsePixelCol(" << dec << pixelCol << ")  = " << hex << "0x" << MSC.enablePulsePixelCol(pixelCol) << " = " << bitset<16>(MSC.enablePulsePixelCol(pixelCol)) << endl;

  // enablePulsePixelHor
  pixelHor = 159;
  cout << __PRETTY_FUNCTION__ << ": enablePulsePixelHor(" << dec << pixelHor << ")  = " << hex << "0x" << MSC.enablePulsePixelHor(pixelHor) << " = " << bitset<16>(MSC.enablePulsePixelHor(pixelHor)) << endl;

  // disablePulsePixelCol
  pixelCol = 376;
  cout << __PRETTY_FUNCTION__ << ": disablePulsePixelCol(" << dec << pixelCol << ") = " << hex << "0x" << MSC.disablePulsePixelCol(pixelCol) << " = " << bitset<16>(MSC.disablePulsePixelCol(pixelCol)) << endl;


  pixelCol = 4;
  cout << __PRETTY_FUNCTION__ << ": TEST enablePulsePixelCol(" << dec << pixelCol << ")  = " << hex << "0x" << MSC.enablePulsePixelCol(pixelCol) << " = " << bitset<16>(MSC.enablePulsePixelCol(pixelCol)) << endl;
  pixelHor = 8;
  cout << __PRETTY_FUNCTION__ << ": TEST enablePulsePixelHor(" << dec << pixelHor << ")  = " << hex << "0x" << MSC.enablePulsePixelHor(pixelHor) << " = " << bitset<16>(MSC.enablePulsePixelHor(pixelHor)) << endl;

  

  // disablePulsePixelHor
  pixelHor = 159;
  cout << __PRETTY_FUNCTION__ << ": disablePulsePixelHor(" << dec << pixelHor << ") = " << hex << "0x" << MSC.disablePulsePixelHor(pixelHor) << " = " << bitset<16>(MSC.disablePulsePixelHor(pixelHor)) << endl;

  // enablePulseFixedPattern
  cout << __PRETTY_FUNCTION__ << ": enablePulseFixedPattern() = " << hex << "0x" << MSC.enablePulseFixedPattern() << " = " << bitset<16>(MSC.enablePulseFixedPattern()) << endl;

  // disablePulseFixedPattern
  for(unsigned int i=0; i<4; i++){
    cout << __PRETTY_FUNCTION__ << ": disablePulseFixedPattern()[" << dec << i << "] = " << hex << "0x" <<(MSC.disablePulseFixedPattern())[i] << " = " << bitset<16>((MSC.disablePulseFixedPattern()[i])) << endl;
  }

  ////////
  // delay
  ////////
  cout << "+++++++ delay" << endl;

  // delay, 2000 ns
  picoseconds = 2000;
  cout << __PRETTY_FUNCTION__ << ": delay(" << dec << picoseconds << ") = " << hex << "0x" << MSC.delay(picoseconds) << " = " << bitset<16>(MSC.delay(picoseconds)) << endl;

  // delay, 1000 ns
  picoseconds = 1000;
  cout << __PRETTY_FUNCTION__ << ": delay(" << dec << picoseconds << ") = " << hex << "0x" << MSC.delay(picoseconds) << " = " << bitset<16>(MSC.delay(picoseconds)) << endl;

  // delay, 750 ns
  picoseconds = 750;
  cout << __PRETTY_FUNCTION__ << ": delay(" << dec << picoseconds << ")  = " << hex << "0x" << MSC.delay(picoseconds) << " = " << bitset<16>(MSC.delay(picoseconds)) << endl;

  // delay, 500 ns
  picoseconds = 500;
  cout << __PRETTY_FUNCTION__ << ": delay(" << dec << picoseconds << ")  = " << hex << "0x" << MSC.delay(picoseconds) << " = " << bitset<16>(MSC.delay(picoseconds)) << endl;

  // delay, 1300 ns
  picoseconds = 1300;
  const unsigned int delayVal = MSC.delay(picoseconds);
  cout << __PRETTY_FUNCTION__ << ": delay(" << dec << picoseconds << ") = " << hex << "0x" << delayVal << " = " << bitset<16>(delayVal) << endl;

  //////
  // DAC
  //////
  cout << "+++++++ DAC" << endl;

  // setVCASN
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setVCASN(" << dec << DACvalue << ")      = " << hex << "0x" << MSC.setVCASN(DACvalue) << " = " << bitset<16>(MSC.setVCASN(DACvalue)) << endl;

  // setVCLIP
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setVCLIP(" << dec << DACvalue << ")      = " << hex << "0x" << MSC.setVCLIP(DACvalue) << " = " << bitset<16>(MSC.setVCLIP(DACvalue)) << endl;

  // setVPULSE_HIGH
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setVPULSE_HIGH(" << dec << DACvalue << ") = " << hex << "0x" << MSC.setVPULSE_HIGH(DACvalue) << " = " << bitset<16>(MSC.setVPULSE_HIGH(DACvalue)) << endl;

  // setVPULSE_LOW
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setVPULSE_LOW(" << dec << DACvalue << ")  = " << hex << "0x" << MSC.setVPULSE_LOW(DACvalue) << " = " << bitset<16>(MSC.setVPULSE_LOW(DACvalue)) << endl;

  // setVRESET_P
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setVRESET_P(" << dec << DACvalue << ")   = " << hex << "0x" << MSC.setVRESET_P(DACvalue) << " = " << bitset<16>(MSC.setVRESET_P(DACvalue)) << endl;

  // setVRESET_D
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setVRESET_D(" << dec << DACvalue << ")   = " << hex << "0x" << MSC.setVRESET_D(DACvalue) << " = " << bitset<16>(MSC.setVRESET_D(DACvalue)) << endl;

  // setICASN
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setICASN(" << dec << DACvalue << ")      = " << hex << "0x" << MSC.setICASN(DACvalue) << " = " << bitset<16>(MSC.setICASN(DACvalue)) << endl;

  // setIRESET
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setIRESET(" << dec << DACvalue << ")     = " << hex << "0x" << MSC.setIRESET(DACvalue) << " = " << bitset<16>(MSC.setIRESET(DACvalue)) << endl;

  // setITHR
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setITHR(" << dec << DACvalue << ")          = " << hex << "0x" << MSC.setITHR(DACvalue) << " = " << bitset<16>(MSC.setITHR(DACvalue)) << endl;
  // setITHR
  DACvalue = 110;
  cout << __PRETTY_FUNCTION__ << ": setITHR(" << dec << DACvalue << ") VALERIO  = " << hex << "0x" << MSC.setITHR(DACvalue) << " = " << bitset<16>(MSC.setITHR(DACvalue)) << endl;

  // setIBIAS
  DACvalue = 0;
  cout << __PRETTY_FUNCTION__ << ": setIBIAS(" << dec << DACvalue << ") LLUIS   = " << hex << "0x" << MSC.setIBIAS(DACvalue) << " = " << bitset<16>(MSC.setIBIAS(DACvalue)) << endl;
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setIBIAS(" << dec << DACvalue << ")         = " << hex << "0x" << MSC.setIBIAS(DACvalue) << " = " << bitset<16>(MSC.setIBIAS(DACvalue)) << endl;
  DACvalue = 60;
  cout << __PRETTY_FUNCTION__ << ": setIBIAS(" << dec << DACvalue << ")         = " << hex << "0x" << MSC.setIBIAS(DACvalue) << " = " << bitset<16>(MSC.setIBIAS(DACvalue)) << endl;
  DACvalue = 80;
  cout << __PRETTY_FUNCTION__ << ": setIBIAS(" << dec << DACvalue << ")         = " << hex << "0x" << MSC.setIBIAS(DACvalue) << " = " << bitset<16>(MSC.setIBIAS(DACvalue)) << endl;
  DACvalue = 100;
 cout << __PRETTY_FUNCTION__ << ": setIBIAS(" << dec << DACvalue << ")         = " << hex << "0x" << MSC.setIBIAS(DACvalue) << " = " << bitset<16>(MSC.setIBIAS(DACvalue)) << endl;
  DACvalue = 127;
  cout << __PRETTY_FUNCTION__ << ": setIBIAS(" << dec << DACvalue << ") VALERIO = " << hex << "0x" << MSC.setIBIAS(DACvalue) << " = " << bitset<16>(MSC.setIBIAS(DACvalue)) << endl;

  // for(unsigned int i=0; i<128; i++){
  //   //    cout << "ipb.Write(5," << hex << "0x" << MSC.setIBIAS(i) << "); " << endl;
  //   cout << "ipb.Write(5," << hex << "0x" << MSC.setITHR(i) << "); " << endl;
  //   cout << "DebugWord(ipb.Read(4)," << dec << i << ")" << endl;
  //   cout << "time.sleep(sleepTime)" << endl;
  // }

  // setIDB
  DACvalue = 47;
  cout << __PRETTY_FUNCTION__ << ": setIDB(" << dec << DACvalue << ")        = " << hex << "0x" << MSC.setIDB(DACvalue) << " = " << bitset<16>(MSC.setIDB(DACvalue)) << endl;

  // setIBUFP_MON0
  DACvalue = 2;
  cout << __PRETTY_FUNCTION__ << ": setIBUFP_MON0(" << dec << DACvalue << ")  = " << hex << "0x" << MSC.setIBUFP_MON0(DACvalue) << " = " << bitset<16>(MSC.setIBUFP_MON0(DACvalue)) << endl;

  // setIBUFN_MON0
  DACvalue = 2;
  cout << __PRETTY_FUNCTION__ << ": setIBUFN_MON0(" << dec << DACvalue << ")  = " << hex << "0x" << MSC.setIBUFN_MON0(DACvalue) << " = " << bitset<16>(MSC.setIBUFN_MON0(DACvalue)) << endl;

  // setIBUFP_MON1
  DACvalue = 2;
  cout << __PRETTY_FUNCTION__ << ": setIBUFP_MON1(" << dec << DACvalue << ")  = " << hex << "0x" << MSC.setIBUFP_MON1(DACvalue) << " = " << bitset<16>(MSC.setIBUFP_MON1(DACvalue)) << endl;

  // setIBUFN_MON1
  DACvalue = 2;
  cout << __PRETTY_FUNCTION__ << ": setIBUFN_MON1(" << dec << DACvalue << ")  = " << hex << "0x" << MSC.setIBUFN_MON1(DACvalue) << " = " << bitset<16>(MSC.setIBUFN_MON1(DACvalue)) << endl;

  // switchDACMONI
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchDACMONI(" << dec << on << ")    = " << hex << "0x" << MSC.switchDACMONI(on) << " = " << bitset<16>(MSC.switchDACMONI(on)) << endl;

  // switchDACMONV
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchDACMONV(" << dec << on << ")    = " << hex << "0x" << MSC.switchDACMONV(on) << " = " << bitset<16>(MSC.switchDACMONV(on)) << endl;

  // setIRESET_BIT
  on = true;
  cout << __PRETTY_FUNCTION__ << ": setIRESET_BIT(" << dec << on << ")    = " << hex << "0x" << MSC.setIRESET_BIT(on) << " = " << bitset<16>(MSC.setIRESET_BIT(on)) << endl;

  // switchIREF
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchIREF(" << dec << on << ")       = " << hex << "0x" << MSC.switchIREF(on) << " = " << bitset<16>(MSC.switchIREF(on)) << endl;

  // switchIDB
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchIDB(" << dec << on << ")        = " << hex << "0x" << MSC.switchIDB(on) << " = " << bitset<16>(MSC.switchIDB(on)) << endl;

  // switchITHR
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchITHR(" << dec << on << ")       = " << hex << "0x" << MSC.switchITHR(on) << " = " << bitset<16>(MSC.switchITHR(on)) << endl;

  // switchIBIAS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchIBIAS(" << dec << on << ")      = " << hex << "0x" << MSC.switchIBIAS(on) << " = " << bitset<16>(MSC.switchIBIAS(on)) << endl;

  // switchIRESET
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchIRESET(" << dec << on << ")     = " << hex << "0x" << MSC.switchIRESET(on) << " = " << bitset<16>(MSC.switchIRESET(on)) << endl;

  // switchICASN
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchICASN(" << dec << on << ")      = " << hex << "0x" << MSC.switchICASN(on) << " = " << bitset<16>(MSC.switchICASN(on)) << endl;

  // switchVRESET_D
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchVRESET_D(" << dec << on << ")   = " << hex << "0x" << MSC.switchVRESET_D(on) << " = " << bitset<16>(MSC.switchVRESET_D(on)) << endl;

  // switchVRESET_P
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchVRESET_P(" << dec << on << ")   = " << hex << "0x" << MSC.switchVRESET_P(on) << " = " << bitset<16>(MSC.switchVRESET_P(on)) << endl;

  // switchVPULSE_LOW
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchVPULSE_LOW(" << dec << on << ")  = " << hex << "0x" << MSC.switchVPULSE_LOW(on) << " = " << bitset<16>(MSC.switchVPULSE_LOW(on)) << endl;

  // switchVPULSE_HIGH
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchVPULSE_HIGH(" << dec << on << ") = " << hex << "0x" << MSC.switchVPULSE_HIGH(on) << " = " << bitset<16>(MSC.switchVPULSE_HIGH(on)) << endl;

  // switchVCLIP
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchVCLIP(" << dec << on << ")      = " << hex << "0x" << MSC.switchVCLIP(on) << " = " << bitset<16>(MSC.switchVCLIP(on)) << endl;

  // switchVCASN
  on = true;
  cout << __PRETTY_FUNCTION__ << ": switchVCASN(" << dec << on << ")      = " << hex << "0x" << MSC.switchVCASN(on) << " = " << bitset<16>(MSC.switchVCASN(on)) << endl;

  ///////
  // LVDS
  ///////
  cout << "+++++++ LVDS" << endl;

  // enableLVDS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableLVDS(" << dec << on << ")                 = " << hex << "0x" << MSC.enableLVDS(on) << " = " << bitset<16>(MSC.enableLVDS(on)) << endl;

  // configPreEmphasis, 5=>0
  LVDSvalue = 61;
  preEmphasisType = "5=>0";
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ")   = " << hex << "0x" << MSC.configPreEmphasis(preEmphasisType, LVDSvalue) << " = " << bitset<16>(MSC.configPreEmphasis(preEmphasisType, LVDSvalue)) << endl;

  // configPreEmphasis, 10=>6
  LVDSvalue = 29;
  preEmphasisType = "10=>6";
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ")  = " << hex << "0x" << MSC.configPreEmphasis(preEmphasisType, LVDSvalue) << " = " << bitset<16>(MSC.configPreEmphasis(preEmphasisType, LVDSvalue)) << endl;

  // configPreEmphasis, 15=>11
  LVDSvalue = 29;
  preEmphasisType = "15=>11";
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ") = " << hex << "0x" << MSC.configPreEmphasis(preEmphasisType, LVDSvalue) << " = " << bitset<16>(MSC.configPreEmphasis(preEmphasisType, LVDSvalue)) << endl;

  LVDSvalue = 0;
  preEmphasisType = "5=>0";
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ")   = " << hex << "0x" << MSC.configPreEmphasis(preEmphasisType, LVDSvalue) << " = " << bitset<16>(MSC.configPreEmphasis(preEmphasisType, LVDSvalue)) << endl;

  // configPreEmphasis, 10=>6
  LVDSvalue = 0;
  preEmphasisType = "10=>6";
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ")  = " << hex << "0x" << MSC.configPreEmphasis(preEmphasisType, LVDSvalue) << " = " << bitset<16>(MSC.configPreEmphasis(preEmphasisType, LVDSvalue)) << endl;

  // configPreEmphasis, 15=>11
  LVDSvalue = 0;
  preEmphasisType = "15=>11";
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ") = " << hex << "0x" << MSC.configPreEmphasis(preEmphasisType, LVDSvalue) << " = " << bitset<16>(MSC.configPreEmphasis(preEmphasisType, LVDSvalue)) << endl;

  


  // configPreEmphasis, invalid type
  LVDSvalue = 29;
  preEmphasisType = "15=>10";
  const unsigned int configValue = MSC.configPreEmphasis(preEmphasisType, LVDSvalue);
  cout << __PRETTY_FUNCTION__ << ": configPreEmphasis(" << preEmphasisType << ", " << dec << LVDSvalue << ") = " << hex << "0x" << configValue << " = " << bitset<16>(configValue) << endl;

  // configHBridge
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configHBridge(" << dec << LVDSvalue << ")             = " << hex << "0x" << MSC.configHBridge(LVDSvalue) << " = " << bitset<16>(MSC.configHBridge(LVDSvalue)) << endl;

 // configHBridge
  LVDSvalue = 0;
  cout << __PRETTY_FUNCTION__ << ": configHBridge(" << dec << LVDSvalue << ")             = " << hex << "0x" << MSC.configHBridge(LVDSvalue) << " = " << bitset<16>(MSC.configHBridge(LVDSvalue)) << endl;

  // configCMFB
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configCMFB(" << dec << LVDSvalue << ")                = " << hex << "0x" << MSC.configCMFB(LVDSvalue) << " = " << bitset<16>(MSC.configCMFB(LVDSvalue)) << endl;

  // configIBCMFB
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configIBCMFB(" << dec << LVDSvalue << ")              = " << hex << "0x" << MSC.configIBCMFB(LVDSvalue) << " = " << bitset<16>(MSC.configIBCMFB(LVDSvalue)) << endl;

  // configIVPH
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configIVPH(" << dec << LVDSvalue << ")                = " << hex << "0x" << MSC.configIVPH(LVDSvalue) << " = " << bitset<16>(MSC.configIVPH(LVDSvalue)) << endl;

  // configIVPL
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configIVPL(" << dec << LVDSvalue << ")                = " << hex << "0x" << MSC.configIVPL(LVDSvalue) << " = " << bitset<16>(MSC.configIVPL(LVDSvalue)) << endl;

  // configIVNH
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configIVNH(" << dec << LVDSvalue << ")                = " << hex << "0x" << MSC.configIVNH(LVDSvalue) << " = " << bitset<16>(MSC.configIVNH(LVDSvalue)) << endl;

  // configIVNL
  LVDSvalue = 29;
  cout << __PRETTY_FUNCTION__ << ": configIVNL(" << dec << LVDSvalue << ")                = " << hex << "0x" << MSC.configIVNL(LVDSvalue) << " = " << bitset<16>(MSC.configIVNL(LVDSvalue)) << endl;

  ////////////
  // data flow
  ////////////
  cout << "+++++++ data flow" << endl;

  // enableMerger
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableMerger(" << dec << on << ")            = " << hex << "0x" << MSC.enableMerger(on) << " = " << bitset<16>(MSC.enableMerger(on)) << endl;

  // enableCMOSRight
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableCMOSRight(" << dec << on << ")         = " << hex << "0x" << MSC.enableCMOSRight(on) << " = " << bitset<16>(MSC.enableCMOSRight(on)) << endl;

  // enableCMOSLeft
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableCMOSLeft(" << dec << on << ")          = " << hex << "0x" << MSC.enableCMOSLeft(on) << " = " << bitset<16>(MSC.enableCMOSLeft(on)) << endl;

  // enableDataflowLVDS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableDataflowLVDS(" << dec << on << ")      = " << hex << "0x" << MSC.enableDataflowLVDS(on) << " = " << bitset<16>(MSC.enableDataflowLVDS(on)) << endl;

  // enableRightCMOSTop
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableRightCMOSTop(" << dec << on << ")      = " << hex << "0x" << MSC.enableRightCMOSTop(on) << " = " << bitset<16>(MSC.enableRightCMOSTop(on)) << endl;

  // enableLeftCMOSTop
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableLeftCMOSTop(" << dec << on << ")       = " << hex << "0x" << MSC.enableLeftCMOSTop(on) << " = " << bitset<16>(MSC.enableLeftCMOSTop(on)) << endl;

  // enableRightMergerToCMOS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableRightMergerToCMOS(" << dec << on << ") = " << hex << "0x" << MSC.enableRightMergerToCMOS(on) << " = " << bitset<16>(MSC.enableRightMergerToCMOS(on)) << endl;

  // enableRightMergerToLVDS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableRightMergerToLVDS(" << dec << on << ") = " << hex << "0x" << MSC.enableRightMergerToLVDS(on) << " = " << bitset<16>(MSC.enableRightMergerToLVDS(on)) << endl;

  // enableLeftMergerToCMOS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableLeftMergerToCMOS(" << dec << on << ")  = " << hex << "0x" << MSC.enableLeftMergerToCMOS(on) << " = " << bitset<16>(MSC.enableLeftMergerToCMOS(on)) << endl;

  // enableLeftMergerToLVDS
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableLeftMergerToLVDS(" << dec << on << ")  = " << hex << "0x" << MSC.enableLeftMergerToLVDS(on) << " = " << bitset<16>(MSC.enableLeftMergerToLVDS(on)) << endl;

  // enableMergerToRight
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableMergerToRight(" << dec << on << ")     = " << hex << "0x" << MSC.enableMergerToRight(on) << " = " << bitset<16>(MSC.enableMergerToRight(on)) << endl;

  // enableMergerToLeft
  on = true;
  cout << __PRETTY_FUNCTION__ << ": enableMergerToLeft(" << dec << on << ")      = " << hex << "0x" << MSC.enableMergerToLeft(on) << " = " << bitset<16>(MSC.enableMergerToLeft(on)) << endl;

  ///////////////
  // power switch
  ///////////////
  cout << "+++++++ power switch" << endl;

  // enablePowerSwitch
  powerSwitchLeftOn = true;
  powerSwitchRightOn = true;
  cout << __PRETTY_FUNCTION__ << ": enablePowerSwitch(" << dec << powerSwitchLeftOn << ", " << powerSwitchRightOn << ") = " << hex << "0x" << MSC.enablePowerSwitch(powerSwitchLeftOn, powerSwitchRightOn) << " = " << bitset<16>(MSC.enablePowerSwitch(powerSwitchLeftOn, powerSwitchRightOn)) << endl;

  ////////////////
  // read register
  ////////////////
  cout << "+++++++ read register" << endl;

  // readRegister
  registerID = 3;
  cout << __PRETTY_FUNCTION__ << ": readRegister(" << dec << registerID << ") Valerio = " << hex << "0x" << MSC.readRegister(registerID) << " = " << bitset<16>(MSC.readRegister(registerID)) << endl;
  
  registerID = 29;
  cout << __PRETTY_FUNCTION__ << ": readRegister(" << dec << registerID << ") = " << hex << "0x" << MSC.readRegister(registerID) << " = " << bitset<16>(MSC.readRegister(registerID)) << endl;

  registerID = 38;
  cout << __PRETTY_FUNCTION__ << ": readRegister(" << dec << registerID << ") IBIAS = " << hex << "0x" << MSC.readRegister(registerID) << " = " << bitset<16>(MSC.readRegister(registerID)) << endl;

  //////////////////////////
  // write reserved register
  //////////////////////////
  cout << "+++++++ write reserved register" << endl;

  // writeReservedRegister
  registerID = 3;
  registerValue = 343;
  cout << __PRETTY_FUNCTION__ << ": writeReservedRegister(" << dec << registerID << ", " << registerValue << ") = " << hex << "0x" << MSC.writeReservedRegister(registerID, registerValue) << " = " << bitset<16>(MSC.writeReservedRegister(registerID, registerValue)) << endl;

  // writeReservedRegister, invalid register
  registerID = 8;
  registerValue = 343;
  const unsigned int writeReservedRegisterValue = MSC.writeReservedRegister(registerID, registerValue);
  cout << __PRETTY_FUNCTION__ << ": writeReservedRegister(" << dec << registerID << ", " << registerValue << ") = " << hex << "0x" << writeReservedRegisterValue << " = " << bitset<16>(writeReservedRegisterValue) << endl;

  ////////////
  // self test
  ////////////
  cout << "+++++++ self test" << endl;

  // selfTest
  cout << __PRETTY_FUNCTION__ << ": selfTest() = " << hex << "0x" << MSC.selfTest() << " = " << bitset<16>(MSC.selfTest()) << endl;

  ///////////////////
  // read SEU counter
  ///////////////////
  cout << "+++++++ read SEU counter" << endl;

  // readSEUCounter
  cout << __PRETTY_FUNCTION__ << ": readSEUCounter() = " << hex << "0x" << MSC.readSEUCounter() << " = " << bitset<16>(MSC.readSEUCounter()) << endl;

  ///////////////
  // read chip ID
  ///////////////
  cout << "+++++++ read chip ID" << endl;

  // readChipID
  cout << __PRETTY_FUNCTION__ << ": readChipID() = " << hex << "0x" << MSC.readChipID() << " = " << bitset<16>(MSC.readChipID()) << endl;

  ////////////////
  // getDiagonal()
  ////////////////

       /*
  for(unsigned int col=0; col<512; col++){
    for(unsigned int hor=0; hor<512; hor++){
      cout << __PRETTY_FUNCTION__ << dec << ": col=" << col << ", hor=" << hor << " => diag=" << MSC.getDiagonal(col, hor) << endl;
    }
  }
       */

  return 0;
}
