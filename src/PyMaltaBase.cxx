/************************************
 * Brief: Python module for Malta
 * 
 * Author: Carlos.Solans@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include "Malta/MaltaBase.h"
#include <iostream>
#include <vector>

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif

#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000

/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MaltaBase *obj;
  std::vector<uint32_t> vec;
} PyMaltaBase;

static int _PyMaltaBase_init(PyMaltaBase *self, PyObject *args)
{
  char * str;
  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
      return 0;
  }  
  self->obj = new MaltaBase(std::string(str));
  return 0;
}

static void _PyMaltaBase_dealloc(PyMaltaBase *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyMaltaBase_GetVersion(PyMaltaBase *self)
{
  self->obj->GetVersion();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_GetFIFO2WordCount(PyMaltaBase *self)
{
  PyObject *py_ret;
  uint32_t ret = self->obj->GetFIFO2WordCount();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaBase_GetL1ID(PyMaltaBase *self)
{
  PyObject *py_ret;
  uint32_t ret = self->obj->GetL1ID();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaBase_SetVerbose(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetVerbose(enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetConfigModeSleep(PyMaltaBase *self, PyObject *args)
{
  uint32_t microseconds;
  if (!PyArg_ParseTuple(args, (char *) "I",&microseconds)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->SetConfigModeSleep(microseconds);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetConfigMode(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  float dvdd;
  if (!PyArg_ParseTuple(args, (char *) "Of",&py_enable,&dvdd)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetConfigMode(enable,dvdd);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_WriteConstDelays(PyMaltaBase *self, PyObject *args)
{
  uint32_t tap1,tap2;
  if (!PyArg_ParseTuple(args, (char *) "II",&tap1, &tap2)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->WriteConstDelays(tap1, tap2);
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_WriteTap(PyMaltaBase *self, PyObject *args)
{
  uint32_t bit,tap1,tap2;
  if (!PyArg_ParseTuple(args, (char *) "III",&bit, &tap1, &tap2)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->WriteTap(bit, tap1, tap2);
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_ReadTap(PyMaltaBase *self, PyObject *args)
{
  uint32_t bit;
  if (!PyArg_ParseTuple(args, (char *) "I",&bit)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  std::vector<uint32_t> ret = self->obj->ReadTap(bit);
  PyObject *py_ret = PyList_New(ret.size()); 
  for(uint32_t i=0;i<ret.size();i++){
    PyObject * it = PyLong_FromLong(ret[i]);
    PyList_SetItem(py_ret,i,it);
  }
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaBase_Reset(PyMaltaBase *self)
{
  self->obj->Reset();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_ResetFifo(PyMaltaBase *self)
{
  self->obj->ResetFifo();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_SetHalfColumns(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetHalfColumns(enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetHalfRows(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetHalfColumns(enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetReadoutDelay(PyMaltaBase *self, PyObject *args)
{
  uint32_t delay;
  if (!PyArg_ParseTuple(args, (char *) "III",&delay)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->SetReadoutDelay(delay);
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_GetReadoutDelay(PyMaltaBase *self)
{
  PyObject *py_ret;
  uint32_t ret = self->obj->GetReadoutDelay();
  py_ret = PyLong_FromLong(ret);
  return py_ret;
}

PyObject * _PyMaltaBase_SetExternalL1A(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetExternalL1A(enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_ReadoutOn(PyMaltaBase *self)
{
  self->obj->ReadoutOn();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_ReadoutOff(PyMaltaBase *self)
{
  self->obj->ReadoutOff();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_EnableFastSignal(PyMaltaBase *self)
{
  self->obj->EnableFastSignal();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_DisableFastSignal(PyMaltaBase *self)
{
  self->obj->DisableFastSignal();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_ResetL1Counter(PyMaltaBase *self)
{
  self->obj->ResetL1Counter();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_ReadMaltaWord(PyMaltaBase *self, PyObject *args)
{
  uint32_t numwords;
  if (!PyArg_ParseTuple(args, (char *) "I",&numwords)){
    Py_RETURN_NONE;
  }
  
  self->vec.resize(numwords);
  self->obj->ReadMaltaWord(&self->vec[0],numwords);
  PyObject *py_ret = PyList_New(numwords); 
  for(uint32_t i=0;i<numwords;i++){
    PyObject * it = PyLong_FromLong(self->vec[i]);
    PyList_SetItem(py_ret,i,it);
  }
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaBase_ReadMonitorWord(PyMaltaBase *self)
{
  uint32_t numwords=37;
  self->vec.resize(numwords);
  self->obj->ReadMonitorWord(&self->vec[0]);
  PyObject *py_ret = PyList_New(numwords); 
  for(uint32_t i=0;i<numwords;i++){
    PyObject * it = PyLong_FromLong(self->vec[i]);
    PyList_SetItem(py_ret,i,it);
  }
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaBase_Sync(PyMaltaBase *self)
{
  self->obj->Sync();
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_ReadFifoStatus(PyMaltaBase *self)
{
  uint32_t ret = self->obj->ReadFifoStatus();
  return PyLong_FromLong(ret);
}

PyObject * _PyMaltaBase_IsFifo1Full(PyMaltaBase *self)
{
  bool ret = self->obj->IsFifo1Full();
  return PyBool_FromLong(ret);
}

PyObject * _PyMaltaBase_IsFifo1Empty(PyMaltaBase *self)
{
  bool ret = self->obj->IsFifo1Empty();
  return PyBool_FromLong(ret);
}

PyObject * _PyMaltaBase_IsFifo1Half(PyMaltaBase *self)
{
  bool ret = self->obj->IsFifo1Half();
  return PyBool_FromLong(ret);
}

PyObject * _PyMaltaBase_IsFifo2Full(PyMaltaBase *self)
{
  bool ret = self->obj->IsFifo2Full();
  return PyBool_FromLong(ret);
}

PyObject * _PyMaltaBase_IsFifo2Empty(PyMaltaBase *self)
{
  bool ret = self->obj->IsFifo1Empty();
  return PyBool_FromLong(ret);
}

PyObject * _PyMaltaBase_IsFifo2Half(PyMaltaBase *self)
{
  bool ret = self->obj->IsFifo1Half();
  return PyBool_FromLong(ret);
}

PyObject * _PyMaltaBase_Trigger(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_width = NULL;
  uint32_t ntimes = 1;
  if (!PyArg_ParseTuple(args, (char *) "|IO",&ntimes,&py_width)){
    Py_RETURN_NONE;
  }
  bool width = py_width ? (bool) PyObject_IsTrue(py_width) : true;
  self->obj->Trigger(ntimes,width);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelPulse(PyMaltaBase *self, PyObject *args)
{
  uint32_t row, column;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IIO",&row,&column,&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelPulse(row,column,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelPulseRow(PyMaltaBase *self, PyObject *args)
{
  uint32_t row;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IO",&row,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelPulseRow(row,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelPulseColumn(PyMaltaBase *self, PyObject *args)
{
  uint32_t column;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IO",&column,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelPulseColumn(column,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelMask(PyMaltaBase *self, PyObject *args)
{
  uint32_t row, column;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IIO",&column,&row,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelMask(column,row,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelMaskRow(PyMaltaBase *self, PyObject *args)
{
  uint32_t row;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IO",&row,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelMaskRow(row,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelMaskColumn(PyMaltaBase *self, PyObject *args)
{
  uint32_t column;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IO",&column,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelMaskColumn(column,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPixelMaskDiag(PyMaltaBase *self, PyObject *args)
{
  uint32_t diag;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IO",&diag,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetPixelMaskDiag(diag,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetDoubleColumnMask(PyMaltaBase *self, PyObject *args)
{
  uint32_t dc;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IO",&dc,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetDoubleColumnMask(dc,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetDoubleColumnMaskRange(PyMaltaBase *self, PyObject *args)
{
  uint32_t dc1, dc2;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IIO",&dc1,&dc2,&py_enable)){
    Py_RETURN_NONE;
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetDoubleColumnMaskRange(dc1,dc2,enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetFullPixelMaskFromFile(PyMaltaBase *self, PyObject *args)
{
  char * str;
  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
    Py_RETURN_NONE;
  }  
  self->obj->SetFullPixelMaskFromFile(std::string(str));
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_SetROI(PyMaltaBase *self, PyObject *args)
{
  uint32_t col1, row1, col2, row2;
  PyObject *py_mask = NULL;
  if (!PyArg_ParseTuple(args, (char *) "IIIIO",&col1,&row1,&col2,&row2,&py_mask)){
    Py_RETURN_NONE;  
  }
  bool mask = py_mask ? (bool) PyObject_IsTrue(py_mask) : false;
  self->obj->SetROI(col1,row2,col2,row2,mask);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetClock(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_RETURN_NONE;  
  }
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetClock(enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_Send(PyMaltaBase *self)
{
  self->obj->Send();
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableMerger(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;  
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableMerger(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableMonDacCurrent(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;  
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableMonDacCurrent(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableMonDacVoltage(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;  
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableMonDacVoltage(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableIDB(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;  
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableIDB(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableITHR(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;  
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableITHR(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableIBIAS(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;  
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableIBIAS(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableIRESET(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableIRESET(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableICASN(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableICASN(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableVCASN(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableVCASN(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableVCLIP(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableVCLIP(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableVPULSE_HIGH(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableVPULSE_HIGH(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableVPULSE_LOW(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableVPULSE_LOW(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableVRESET_D(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableVRESET_D(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_EnableVRESET_P(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O|O",&py_enable,&py_force)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->EnableVRESET_P(enable,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetIDB(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetIDB(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetITHR(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetITHR(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetIBIAS(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetIBIAS(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetIRESET(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetIRESET(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetICASN(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetICASN(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetVCASN(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetVCASN(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetVCLIP(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetVCLIP(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetVPULSE_HIGH(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetVPULSE_HIGH(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetVPULSE_LOW(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetVPULSE_LOW(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetVRESET_D(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetVRESET_D(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetVRESET_P(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetVRESET_P(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetPulseWidth(PyMaltaBase *self, PyObject *args)
{
  uint32_t value;
  PyObject *py_force = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&value,&py_force)){
    Py_RETURN_NONE;
  } 
  bool force = py_force ? (bool) PyObject_IsTrue(py_force) : false;
  self->obj->SetPulseWidth(value,force);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetConfigFromString(PyMaltaBase *self, PyObject *args)
{
  char * str;
  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
    Py_RETURN_NONE;
  }  
  self->obj->SetConfigFromString(std::string(str));
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_SetConfigFromFile(PyMaltaBase *self, PyObject *args)
{
  char * str;
  if(!PyArg_ParseTuple(args, (char *) "s",&str)){
    Py_RETURN_NONE;
  }  
  self->obj->SetConfigFromFile(std::string(str));
  Py_RETURN_NONE;
}

PyObject * _PyMaltaBase_SetDUT(PyMaltaBase *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "O",&py_enable)){
    Py_RETURN_NONE;
  } 
  bool enable = py_enable ? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->SetDUT(enable);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_PreConfigMaltaC(PyMaltaBase *self)
{
  self->obj->PreConfigMaltaC();
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_SetReservedRegister(PyMaltaBase *self, PyObject *args)
{
  uint32_t reg;
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "II",&reg,&value)){
    Py_RETURN_NONE;
  } 
  self->obj->SetReservedRegister(reg,value);
  Py_RETURN_NONE;
} 

PyObject * _PyMaltaBase_GetReservedRegister(PyMaltaBase *self, PyObject *args)
{
  uint32_t reg;
  if (!PyArg_ParseTuple(args, (char *) "I",&reg)){
    Py_RETURN_NONE;
  } 
  uint32_t value=self->obj->GetReservedRegister(reg);
  PyObject *py_ret = PyLong_FromLong(value);
  return py_ret;
} 

PyObject * _PyMaltaBase_GetRegister(PyMaltaBase *self, PyObject *args)
{
  uint32_t reg;
  if (!PyArg_ParseTuple(args, (char *) "I",&reg)){
    Py_RETURN_NONE;
  } 
  uint32_t value=self->obj->GetRegister(reg);
  PyObject *py_ret = PyLong_FromLong(value);
  return py_ret;
} 

////////////////////////////////////////////////////////////////


static PyMethodDef PyMaltaBase_methods[] = {
  {(char *) "GetVersion",(PyCFunction) _PyMaltaBase_GetVersion, METH_NOARGS, NULL },
  {(char *) "GetFIFO2WordCount",(PyCFunction) _PyMaltaBase_GetFIFO2WordCount, METH_NOARGS, NULL },
  {(char *) "GetL1ID",(PyCFunction) _PyMaltaBase_GetL1ID, METH_NOARGS, NULL },
  {(char *) "SetVerbose",(PyCFunction) _PyMaltaBase_SetVerbose, METH_VARARGS, NULL },
  {(char *) "SetConfigModeSleep",(PyCFunction) _PyMaltaBase_SetConfigModeSleep, METH_VARARGS, NULL },
  {(char *) "SetConfigMode",(PyCFunction) _PyMaltaBase_SetConfigMode, METH_VARARGS, NULL },
  {(char *) "WriteConstDelays",(PyCFunction) _PyMaltaBase_WriteConstDelays, METH_VARARGS, NULL },
  {(char *) "WriteTap",(PyCFunction) _PyMaltaBase_WriteTap, METH_VARARGS, NULL },
  {(char *) "ReadTap",(PyCFunction) _PyMaltaBase_ReadTap, METH_VARARGS, NULL },
  {(char *) "SetVerbose",(PyCFunction) _PyMaltaBase_SetVerbose, METH_VARARGS, NULL },
  {(char *) "Reset",(PyCFunction) _PyMaltaBase_Reset, METH_NOARGS, NULL },
  {(char *) "ResetFifo",(PyCFunction) _PyMaltaBase_ResetFifo, METH_NOARGS, NULL },
  {(char *) "SetHalfColumns",(PyCFunction) _PyMaltaBase_SetHalfColumns, METH_VARARGS, NULL },
  {(char *) "SetHalfRows",(PyCFunction) _PyMaltaBase_SetHalfRows, METH_VARARGS, NULL },
  {(char *) "SetReadoutDelay",(PyCFunction) _PyMaltaBase_SetReadoutDelay, METH_VARARGS, NULL },
  {(char *) "GetReadoutDelay",(PyCFunction) _PyMaltaBase_GetReadoutDelay, METH_NOARGS, NULL }, 
  {(char *) "SetExternalL1A",(PyCFunction) _PyMaltaBase_SetExternalL1A, METH_VARARGS, NULL },
  {(char *) "ReadoutOn",(PyCFunction) _PyMaltaBase_ReadoutOn, METH_NOARGS, NULL },
  {(char *) "ReadoutOff",(PyCFunction) _PyMaltaBase_ReadoutOff, METH_NOARGS, NULL },
  {(char *) "EnableFastSignal",(PyCFunction) _PyMaltaBase_EnableFastSignal, METH_NOARGS, NULL },
  {(char *) "DisableFastSignal",(PyCFunction) _PyMaltaBase_DisableFastSignal, METH_NOARGS, NULL },
  {(char *) "ResetL1Counter",(PyCFunction) _PyMaltaBase_ResetL1Counter, METH_VARARGS, NULL },
  {(char *) "ReadMaltaWord",(PyCFunction) _PyMaltaBase_ReadMaltaWord, METH_VARARGS, NULL },
  {(char *) "ReadMonitorWord",(PyCFunction) _PyMaltaBase_ReadMonitorWord, METH_NOARGS, NULL },
  {(char *) "Sync",(PyCFunction) _PyMaltaBase_Sync, METH_NOARGS, NULL },
  {(char *) "ReadFifoStatus",(PyCFunction) _PyMaltaBase_ReadFifoStatus, METH_NOARGS, NULL },
  {(char *) "IsFifo1Full",(PyCFunction) _PyMaltaBase_IsFifo1Full, METH_NOARGS, NULL },
  {(char *) "IsFifo1Empty",(PyCFunction) _PyMaltaBase_IsFifo1Empty, METH_NOARGS, NULL },
  {(char *) "IsFifo1Half",(PyCFunction) _PyMaltaBase_IsFifo1Half, METH_NOARGS, NULL },
  {(char *) "IsFifo2Full",(PyCFunction) _PyMaltaBase_IsFifo2Full, METH_NOARGS, NULL },
  {(char *) "IsFifo2Empty",(PyCFunction) _PyMaltaBase_IsFifo2Empty, METH_NOARGS, NULL },
  {(char *) "IsFifo2Half",(PyCFunction) _PyMaltaBase_IsFifo2Half, METH_NOARGS, NULL },
  {(char *) "Trigger",(PyCFunction) _PyMaltaBase_Trigger, METH_VARARGS, NULL },
  {(char *) "SetPixelPulse",(PyCFunction) _PyMaltaBase_SetPixelPulse, METH_VARARGS, NULL },
  {(char *) "SetPixelPulseRow",(PyCFunction) _PyMaltaBase_SetPixelPulseRow, METH_VARARGS, NULL },
  {(char *) "SetPixelPulseColumn",(PyCFunction) _PyMaltaBase_SetPixelPulseColumn, METH_VARARGS, NULL },
  {(char *) "SetPixelMask",(PyCFunction) _PyMaltaBase_SetPixelMask, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskRow",(PyCFunction) _PyMaltaBase_SetPixelMaskRow, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskColumn",(PyCFunction) _PyMaltaBase_SetPixelMaskColumn, METH_VARARGS, NULL },
  {(char *) "SetPixelMaskDiag",(PyCFunction) _PyMaltaBase_SetPixelMaskDiag, METH_VARARGS, NULL },
  {(char *) "SetDoubleColumnMask",(PyCFunction) _PyMaltaBase_SetDoubleColumnMask, METH_VARARGS, NULL },
  {(char *) "SetDoubleColumnMaskRange",(PyCFunction) _PyMaltaBase_SetDoubleColumnMaskRange, METH_VARARGS, NULL },
  {(char *) "SetFullPixelMaskFromFile",(PyCFunction) _PyMaltaBase_SetFullPixelMaskFromFile, METH_VARARGS, NULL },
  {(char *) "SetROI",(PyCFunction) _PyMaltaBase_SetROI, METH_VARARGS, NULL },
  {(char *) "SetClock",(PyCFunction) _PyMaltaBase_SetROI, METH_VARARGS, NULL },
  {(char *) "Send",(PyCFunction) _PyMaltaBase_Send, METH_NOARGS, NULL },
  {(char *) "EnableMerger",(PyCFunction) _PyMaltaBase_Send, METH_VARARGS, NULL },
  {(char *) "EnableMonDacCurrent",(PyCFunction) _PyMaltaBase_Send, METH_VARARGS, NULL },
  {(char *) "EnableMonDacVoltage",(PyCFunction) _PyMaltaBase_Send, METH_VARARGS, NULL },
  {(char *) "EnableIDB",(PyCFunction) _PyMaltaBase_EnableIDB, METH_VARARGS, NULL },
  {(char *) "EnableITHR",(PyCFunction) _PyMaltaBase_EnableITHR, METH_VARARGS, NULL },
  {(char *) "EnableIBIAS",(PyCFunction) _PyMaltaBase_EnableIBIAS, METH_VARARGS, NULL },
  {(char *) "EnableIRESET",(PyCFunction) _PyMaltaBase_EnableIRESET, METH_VARARGS, NULL },
  {(char *) "EnableICASN",(PyCFunction) _PyMaltaBase_EnableICASN, METH_VARARGS, NULL },
  {(char *) "EnableVCASN",(PyCFunction) _PyMaltaBase_EnableVCASN, METH_VARARGS, NULL },
  {(char *) "EnableVCLIP",(PyCFunction) _PyMaltaBase_EnableVCLIP, METH_VARARGS, NULL },
  {(char *) "EnableVPULSE_HIGH",(PyCFunction) _PyMaltaBase_EnableVPULSE_HIGH, METH_VARARGS, NULL },
  {(char *) "EnableVPULSE_LOW",(PyCFunction) _PyMaltaBase_EnableVPULSE_LOW, METH_VARARGS, NULL },
  {(char *) "EnableVRESET_D",(PyCFunction) _PyMaltaBase_EnableVRESET_D, METH_VARARGS, NULL },
  {(char *) "EnableVRESET_P",(PyCFunction) _PyMaltaBase_EnableVRESET_P, METH_VARARGS, NULL },
  {(char *) "SetIDB",(PyCFunction) _PyMaltaBase_SetIDB, METH_VARARGS, NULL },
  {(char *) "SetITHR",(PyCFunction) _PyMaltaBase_SetITHR, METH_VARARGS, NULL },
  {(char *) "SetIBIAS",(PyCFunction) _PyMaltaBase_SetIBIAS, METH_VARARGS, NULL },
  {(char *) "SetIRESET",(PyCFunction) _PyMaltaBase_SetIRESET, METH_VARARGS, NULL },
  {(char *) "SetICASN",(PyCFunction) _PyMaltaBase_SetICASN, METH_VARARGS, NULL },
  {(char *) "SetVCASN",(PyCFunction) _PyMaltaBase_SetVCASN, METH_VARARGS, NULL },
  {(char *) "SetVCLIP",(PyCFunction) _PyMaltaBase_SetVCLIP, METH_VARARGS, NULL },
  {(char *) "SetVPULSE_HIGH",(PyCFunction) _PyMaltaBase_SetVPULSE_HIGH, METH_VARARGS, NULL },
  {(char *) "SetVPULSE_LOW",(PyCFunction) _PyMaltaBase_SetVPULSE_LOW, METH_VARARGS, NULL },
  {(char *) "SetVRESET_D",(PyCFunction) _PyMaltaBase_SetVRESET_D, METH_VARARGS, NULL },
  {(char *) "SetVRESET_P",(PyCFunction) _PyMaltaBase_SetVRESET_P, METH_VARARGS, NULL },
  {(char *) "SetPulseWidth",(PyCFunction) _PyMaltaBase_SetPulseWidth, METH_VARARGS, NULL },
  {(char *) "SetConfigFromString",(PyCFunction) _PyMaltaBase_SetConfigFromString, METH_VARARGS, NULL },
  {(char *) "SetDUT",(PyCFunction) _PyMaltaBase_SetDUT, METH_VARARGS, NULL },
  {(char *) "PreConfigMaltaC",(PyCFunction) _PyMaltaBase_PreConfigMaltaC, METH_VARARGS, NULL },
  {(char *) "SetReservedRegister",(PyCFunction) _PyMaltaBase_SetReservedRegister, METH_VARARGS, NULL },
  {(char *) "GetReservedRegister",(PyCFunction) _PyMaltaBase_GetReservedRegister, METH_VARARGS, NULL },
  {(char *) "GetRegister",(PyCFunction) _PyMaltaBase_GetRegister, METH_VARARGS, NULL },
  {NULL, NULL, 0, NULL}
};

PyTypeObject PyMaltaBase_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyMaltaBase.MaltaBase",            /* tp_name */
    sizeof(PyMaltaBase),                 /* tp_basicsize */
    0,                                   /* tp_itemsize */
    /* methods */
    (destructor) _PyMaltaBase_dealloc,   /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    NULL,                       /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMaltaBase_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMaltaBase_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	,0                                   /* tp_version_tag */
#endif
};

static PyMethodDef PyModule_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMaltaBase)
{
  PyObject *m;
  MOD_DEF(m, "PyMaltaBase", NULL, PyModule_methods);
  if(PyType_Ready(&PyMaltaBase_Type)<0){
    return MOD_ERROR;
  }
  PyModule_AddObject(m, (char *) "MaltaBase", (PyObject *) &PyMaltaBase_Type);
  return MOD_RETURN(m);
}
