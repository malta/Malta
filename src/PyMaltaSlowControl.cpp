/************************************
 * MaltaSlowControl
 * Brief: MaltaSlowControl module 
 * 
 * Author: LLuis.Simon.Argemi@cern.ch
 *         Carlos.Solans@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN 
#include <Python.h>
#include <stddef.h>
#include "Malta/MaltaSlowControl.h"
#include <iostream>
#include <string>
using namespace std;

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)                            \
  do {                                          \
    if (op) {                                   \
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;                              \
      Py_DECREF(tmp);                           \
    }                                           \
  } while (0)

#define Py_VISIT(op)                            \
  do {                                          \
    if (op) {                                   \
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)                                 \
        return vret;                            \
    }                                           \
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif


#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000

/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  MaltaSlowControl *obj;
} PyMaltaSlowControl;

static int _PyMaltaSlowControl_init(PyMaltaSlowControl *self, PyObject *args)
{
  self->obj = new MaltaSlowControl();
  return 0;
}

static void _PyMaltaSlowControl_dealloc(PyMaltaSlowControl *self)
{
  delete self->obj;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyMaltaSlowControl_resetCMU(PyMaltaSlowControl *self)
{
  uint32_t ret = self->obj->resetCMU();
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_generateReset(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t nClockCycles;
  if (!PyArg_ParseTuple(args, (char *) "I", &nClockCycles)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->generateReset(nClockCycles);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_resetSCLRRegister(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t registerID;
  if (!PyArg_ParseTuple(args, (char *) "I", &registerID)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->resetSCLRRegister(registerID);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_maskPixelCol(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t col;
  if (!PyArg_ParseTuple(args, (char *) "I", &col)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->maskPixelCol(col);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_maskPixelHor(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t hor;
  if (!PyArg_ParseTuple(args, (char *) "I", &hor)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->maskPixelHor(hor);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}


PyObject * _PyMaltaSlowControl_maskPixelDiag(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t diag;
  if (!PyArg_ParseTuple(args, (char *) "I", &diag)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->maskPixelDiag(diag);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_unmaskPixelCol(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t col;
  if (!PyArg_ParseTuple(args, (char *) "I", &col)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->unmaskPixelCol(col);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}


PyObject * _PyMaltaSlowControl_unmaskPixelHor(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t hor;
  if (!PyArg_ParseTuple(args, (char *) "I", &hor)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->unmaskPixelHor(hor);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_unmaskPixelDiag(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t diag;
  if (!PyArg_ParseTuple(args, (char *) "I", &diag)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->unmaskPixelDiag(diag);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_maskDoubleColumn(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t doubleColumn;
  if (!PyArg_ParseTuple(args, (char *) "I", &doubleColumn)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->maskDoubleColumn(doubleColumn);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_unmaskDoubleColumn(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t doubleColumn;
  if (!PyArg_ParseTuple(args, (char *) "I", &doubleColumn)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->unmaskDoubleColumn(doubleColumn);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enablePulsePixelCol(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t col;
  if (!PyArg_ParseTuple(args, (char *) "I", &col)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->enablePulsePixelCol(col);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enablePulsePixelHor(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t hor;
  if (!PyArg_ParseTuple(args, (char *) "I", &hor)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->enablePulsePixelHor(hor);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_disablePulsePixelCol(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t col;
  if (!PyArg_ParseTuple(args, (char *) "I", &col)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->disablePulsePixelCol(col);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_disablePulsePixelHor(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t hor;
  if (!PyArg_ParseTuple(args, (char *) "I", &hor)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->disablePulsePixelHor(hor);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enablePulseFixedPattern(PyMaltaSlowControl *self)
{
  uint32_t ret = self->obj->enablePulseFixedPattern();
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_disablePulseFixedPattern(PyMaltaSlowControl *self)
{
  vector<uint32_t> ret = self->obj->disablePulseFixedPattern();

  PyObject *py_ret = PyList_New(0);  
  for (uint32_t i=0; i<ret.size(); ++i){
    PyList_Append(py_ret, PyLong_FromLong(i));
  }
  Py_INCREF(py_ret);

  return py_ret;
}

PyObject * _PyMaltaSlowControl_delay(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t picoseconds;
  if (!PyArg_ParseTuple(args, (char *) "I", &picoseconds)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->delay(picoseconds);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setVCASN(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setVCASN(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setVCLIP(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setVCLIP(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setVPULSE_HIGH(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setVPULSE_HIGH(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setVPULSE_LOW(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setVPULSE_LOW(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setVRESET_P(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setVRESET_P(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setVRESET_D(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setVRESET_D(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setICASN(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setICASN(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIRESET(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIRESET(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setITHR(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setITHR(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIBIAS(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIBIAS(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIDB(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIDB(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIBUFP_MON0(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIBUFP_MON0(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIBUFN_MON0(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIBUFN_MON0(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIBUFP_MON1(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIBUFP_MON1(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIBUFN_MON1(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIBUFN_MON1(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchDACMONI(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchDACMONI(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchDACMONV(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchDACMONV(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_setIRESET_BIT(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->setIRESET_BIT(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchIREF(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchIREF(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchIDB(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchIDB(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchITHR(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchITHR(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchIBIAS(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchIBIAS(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchIRESET(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchIRESET(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchICASN(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchICASN(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchVRESET_D(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchVRESET_D(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchVRESET_P(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchVRESET_P(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchVPULSE_LOW(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchVPULSE_LOW(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchVPULSE_HIGH(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchVPULSE_HIGH(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchVCLIP(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchVCLIP(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_switchVCASN(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->switchVCASN(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableLVDS(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t on;
  if (!PyArg_ParseTuple(args, (char *) "I", &on)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->enableLVDS(on);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configPreEmphasis(PyMaltaSlowControl *self, PyObject *type,  PyObject *args)
{
  char * typ;
  uint32_t value;
  
  if (!PyArg_ParseTuple(args, (char *) "sI", &typ,&value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  string pre(typ);
  uint32_t ret = self->obj->configPreEmphasis(pre,value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configHBridge(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configHBridge(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configCMFB(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configCMFB(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configIBCMFB(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configIBCMFB(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configIVPH(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configIVPH(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configIVPL(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configIVPL(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configIVNH(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configIVNH(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_configIVNL(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t value;
  if (!PyArg_ParseTuple(args, (char *) "I", &value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->configIVNL(value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableMerger(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableMerger(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableCMOSRight(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableCMOSRight(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableCMOSLeft(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableCMOSLeft(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableDataflowLVDS(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableDataflowLVDS(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableRightCMOSTop(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableRightCMOSTop(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableLeftCMOSTop(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableLeftCMOSTop(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableRightMergerToCMOS(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableRightMergerToCMOS(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableRightMergerToLVDS(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableRightMergerToLVDS(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableLeftMergerToCMOS(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableLeftMergerToCMOS(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableLeftMergerToLVDS(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableLeftMergerToLVDS(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableMergerToRight(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableMergerToRight(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enableMergerToLeft(PyMaltaSlowControl *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args,(char*)"O",&py_enable)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = (PyObject_IsTrue(py_enable)==1?true:false);
  uint32_t ret = self->obj->enableMergerToLeft(enable);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_enablePowerSwitch(PyMaltaSlowControl *self,  PyObject *args)
{
  PyObject *py_left = NULL;
  PyObject *py_right = NULL;
  if (!PyArg_ParseTuple(args,(char*)"OO",&py_left,&py_right)) {
	Py_INCREF(Py_None);
    return Py_None;
  }
  bool left = (PyObject_IsTrue(py_left)==1?true:false);
  bool right = (PyObject_IsTrue(py_right)==1?true:false);
  uint32_t ret = self->obj->enablePowerSwitch(left,right);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_readRegister(PyMaltaSlowControl *self, PyObject *args)
{
  uint32_t registerID;
  if (!PyArg_ParseTuple(args, (char *) "I", &registerID)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->readRegister(registerID);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_writeReservedRegister(PyMaltaSlowControl *self,  PyObject *args)
{
  uint32_t registerID;
  uint32_t value;
 
  if (!PyArg_ParseTuple(args, (char *) "II", &registerID,&value)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->writeReservedRegister(registerID,value);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_selfTest(PyMaltaSlowControl *self)
{
  uint32_t ret = self->obj->selfTest();
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_readSEUCounter(PyMaltaSlowControl *self)
{
  uint32_t ret = self->obj->readSEUCounter();
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_readChipID(PyMaltaSlowControl *self)
{
  uint32_t ret = self->obj->readChipID();
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_getDiagonal(PyMaltaSlowControl *self,  PyObject *args)
{
  uint32_t col;
  uint32_t hor;
 
  if (!PyArg_ParseTuple(args, (char *) "II", &col,&hor)) {
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->getDiagonal(col,hor);
  PyObject *py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyMaltaSlowControl_maskPixel(PyMaltaSlowControl *self,  PyObject *args)
{
  uint32_t col;
  uint32_t hor;
 
  if (!PyArg_ParseTuple(args, (char *) "II", &col,&hor)) {
    Py_INCREF(Py_None);
    return Py_None;
  }

  std::vector<unsigned int> ret = self->obj->maskPixel(col,hor);

  PyObject *py_ret = PyList_New(0);  
  for (uint32_t i=0; i<ret.size(); ++i){
    PyList_Append(py_ret, PyLong_FromLong(i));
  }
  Py_INCREF(py_ret);
  return py_ret;
}

static PyMethodDef PyMaltaSlowControl_methods[] = {
  {(char *) "resetCMU",   (PyCFunction) _PyMaltaSlowControl_resetCMU,  METH_NOARGS, NULL },
  {(char *) "generateReset",   (PyCFunction) _PyMaltaSlowControl_generateReset,  METH_VARARGS, NULL },
  {(char *) "resetSCLRRegister",   (PyCFunction) _PyMaltaSlowControl_resetSCLRRegister,  METH_VARARGS, NULL },
  {(char *) "maskPixelCol",   (PyCFunction) _PyMaltaSlowControl_maskPixelCol,  METH_VARARGS, NULL },
  {(char *) "maskPixelHor",   (PyCFunction) _PyMaltaSlowControl_maskPixelHor,  METH_VARARGS, NULL },
  {(char *) "maskPixelDiag",   (PyCFunction) _PyMaltaSlowControl_maskPixelDiag,  METH_VARARGS, NULL },
  {(char *) "unmaskPixelCol",   (PyCFunction) _PyMaltaSlowControl_unmaskPixelCol,  METH_VARARGS, NULL },
  {(char *) "unmaskPixelHor",   (PyCFunction) _PyMaltaSlowControl_unmaskPixelHor,  METH_VARARGS, NULL },
  {(char *) "unmaskPixelDiag",   (PyCFunction) _PyMaltaSlowControl_unmaskPixelDiag,  METH_VARARGS, NULL },
  {(char *) "maskDoubleColumn",   (PyCFunction) _PyMaltaSlowControl_maskDoubleColumn,  METH_VARARGS, NULL },
  {(char *) "unmaskDoubleColumn",   (PyCFunction) _PyMaltaSlowControl_unmaskDoubleColumn,  METH_VARARGS, NULL },
  {(char *) "enablePulsePixelCol",   (PyCFunction) _PyMaltaSlowControl_enablePulsePixelCol,  METH_VARARGS, NULL },
  {(char *) "enablePulsePixelHor",   (PyCFunction) _PyMaltaSlowControl_enablePulsePixelHor,  METH_VARARGS, NULL },
  {(char *) "disablePulsePixelCol",   (PyCFunction) _PyMaltaSlowControl_disablePulsePixelCol,  METH_VARARGS, NULL },
  {(char *) "disablePulsePixelHor",   (PyCFunction) _PyMaltaSlowControl_disablePulsePixelHor,  METH_VARARGS, NULL },
  {(char *) "enablePulseFixedPattern",   (PyCFunction) _PyMaltaSlowControl_enablePulseFixedPattern,  METH_NOARGS, NULL },
  {(char *) "disablePulseFixedPattern",   (PyCFunction) _PyMaltaSlowControl_disablePulseFixedPattern,  METH_NOARGS, NULL },
  {(char *) "delay",   (PyCFunction) _PyMaltaSlowControl_delay,  METH_VARARGS, NULL },
  {(char *) "setVCASN",   (PyCFunction) _PyMaltaSlowControl_setVCASN,  METH_VARARGS, NULL },
  {(char *) "setVCLIP",   (PyCFunction) _PyMaltaSlowControl_setVCLIP,  METH_VARARGS, NULL },
  {(char *) "setVPLSE_HIGH",   (PyCFunction) _PyMaltaSlowControl_setVPULSE_HIGH,  METH_VARARGS, NULL },
  {(char *) "setVPLSE_LOW",   (PyCFunction) _PyMaltaSlowControl_setVPULSE_LOW,  METH_VARARGS, NULL },
  {(char *) "setVPULSE_HIGH",   (PyCFunction) _PyMaltaSlowControl_setVPULSE_HIGH,  METH_VARARGS, NULL },
  {(char *) "setVPULSE_LOW",   (PyCFunction) _PyMaltaSlowControl_setVPULSE_LOW,  METH_VARARGS, NULL },
  {(char *) "setVRESET_P",   (PyCFunction) _PyMaltaSlowControl_setVRESET_P,  METH_VARARGS, NULL },
  {(char *) "setVRESET_D",   (PyCFunction) _PyMaltaSlowControl_setVRESET_D,  METH_VARARGS, NULL },
  {(char *) "setICASN",   (PyCFunction) _PyMaltaSlowControl_setICASN,  METH_VARARGS, NULL },
  {(char *) "setIRESET",   (PyCFunction) _PyMaltaSlowControl_setIRESET,  METH_VARARGS, NULL },
  {(char *) "setITHR",   (PyCFunction) _PyMaltaSlowControl_setITHR,  METH_VARARGS, NULL },
  {(char *) "setIBIAS",   (PyCFunction) _PyMaltaSlowControl_setIBIAS,  METH_VARARGS, NULL },
  {(char *) "setIDB",   (PyCFunction) _PyMaltaSlowControl_setIDB,  METH_VARARGS, NULL },
  {(char *) "setIBUFP_MON0",   (PyCFunction) _PyMaltaSlowControl_setIBUFP_MON0,  METH_VARARGS, NULL },
  {(char *) "setIBUFN_MON0",   (PyCFunction) _PyMaltaSlowControl_setIBUFN_MON0,  METH_VARARGS, NULL },
  {(char *) "setIBUFP_MON1",   (PyCFunction) _PyMaltaSlowControl_setIBUFP_MON1,  METH_VARARGS, NULL },
  {(char *) "setIBUFN_MON1",   (PyCFunction) _PyMaltaSlowControl_setIBUFN_MON1,  METH_VARARGS, NULL },
  {(char *) "switchDACMONI",   (PyCFunction) _PyMaltaSlowControl_switchDACMONI,  METH_VARARGS, NULL },
  {(char *) "switchDACMONV",   (PyCFunction) _PyMaltaSlowControl_switchDACMONV,  METH_VARARGS, NULL },
  {(char *) "setIRESET_BIT",   (PyCFunction) _PyMaltaSlowControl_setIRESET_BIT,  METH_VARARGS, NULL },
  {(char *) "switchIREF",   (PyCFunction) _PyMaltaSlowControl_switchIREF,  METH_VARARGS, NULL },
  {(char *) "switchIDB",   (PyCFunction) _PyMaltaSlowControl_switchIDB,  METH_VARARGS, NULL },
  {(char *) "switchITHR",   (PyCFunction) _PyMaltaSlowControl_switchITHR,  METH_VARARGS, NULL },
  {(char *) "switchIBIAS",   (PyCFunction) _PyMaltaSlowControl_switchIBIAS,  METH_VARARGS, NULL },
  {(char *) "switchIRESET",   (PyCFunction) _PyMaltaSlowControl_switchIRESET,  METH_VARARGS, NULL },
  {(char *) "switchICASN",   (PyCFunction) _PyMaltaSlowControl_switchICASN,  METH_VARARGS, NULL },
  {(char *) "switchVRESET_D",   (PyCFunction) _PyMaltaSlowControl_switchVRESET_D,  METH_VARARGS, NULL },
  {(char *) "switchVRESET_P",   (PyCFunction) _PyMaltaSlowControl_switchVRESET_P,  METH_VARARGS, NULL },  
  {(char *) "switchVPLSE_LOW",   (PyCFunction) _PyMaltaSlowControl_switchVPULSE_LOW,  METH_VARARGS, NULL },
  {(char *) "switchVPLSE_HIGH",   (PyCFunction) _PyMaltaSlowControl_switchVPULSE_HIGH,  METH_VARARGS, NULL },
  {(char *) "switchVPULSE_LOW",   (PyCFunction) _PyMaltaSlowControl_switchVPULSE_LOW,  METH_VARARGS, NULL },
  {(char *) "switchVPULSE_HIGH",   (PyCFunction) _PyMaltaSlowControl_switchVPULSE_HIGH,  METH_VARARGS, NULL },
  {(char *) "switchVCLIP",   (PyCFunction) _PyMaltaSlowControl_switchVCLIP,  METH_VARARGS, NULL },
  {(char *) "switchVCASN",   (PyCFunction) _PyMaltaSlowControl_switchVCASN,  METH_VARARGS, NULL },
  {(char *) "enableLVDS",   (PyCFunction) _PyMaltaSlowControl_enableLVDS,  METH_VARARGS, NULL },
  {(char *) "configPreEmphasis",   (PyCFunction) _PyMaltaSlowControl_configPreEmphasis,  METH_VARARGS, NULL },
  {(char *) "configHBridge",   (PyCFunction) _PyMaltaSlowControl_configHBridge,  METH_VARARGS, NULL },
  {(char *) "configCMFB",   (PyCFunction) _PyMaltaSlowControl_configCMFB,  METH_VARARGS, NULL },
  {(char *) "configIBCMFB",   (PyCFunction) _PyMaltaSlowControl_configIBCMFB,  METH_VARARGS, NULL },
  {(char *) "configIVPH",   (PyCFunction) _PyMaltaSlowControl_configIVPH,  METH_VARARGS, NULL },
  {(char *) "configIVPL",   (PyCFunction) _PyMaltaSlowControl_configIVPL,  METH_VARARGS, NULL },
  {(char *) "configIVNH",   (PyCFunction) _PyMaltaSlowControl_configIVNH,  METH_VARARGS, NULL },
  {(char *) "configIVNL",   (PyCFunction) _PyMaltaSlowControl_configIVNL,  METH_VARARGS, NULL }, 
  {(char *) "enableMerger",   (PyCFunction) _PyMaltaSlowControl_enableMerger,  METH_VARARGS, NULL },
  {(char *) "enableCMOSRight",   (PyCFunction) _PyMaltaSlowControl_enableCMOSRight,  METH_VARARGS, NULL },
  {(char *) "enableCMOSLeft",   (PyCFunction) _PyMaltaSlowControl_enableCMOSLeft,  METH_VARARGS, NULL },
  {(char *) "enableDataflowLVDS",   (PyCFunction) _PyMaltaSlowControl_enableDataflowLVDS,  METH_VARARGS, NULL },
  {(char *) "enableRightCMOSTop",   (PyCFunction) _PyMaltaSlowControl_enableRightCMOSTop,  METH_VARARGS, NULL },
  {(char *) "enableLeftCMOSTop",   (PyCFunction) _PyMaltaSlowControl_enableLeftCMOSTop,  METH_VARARGS, NULL },
  {(char *) "enableRightMergerToCMOS",   (PyCFunction) _PyMaltaSlowControl_enableRightMergerToCMOS,  METH_VARARGS, NULL },
  {(char *) "enableRightMergerToLVDS",   (PyCFunction) _PyMaltaSlowControl_enableRightMergerToLVDS,  METH_VARARGS, NULL },
  {(char *) "enableLeftMergerToCMOS",   (PyCFunction) _PyMaltaSlowControl_enableLeftMergerToCMOS,  METH_VARARGS, NULL },
  {(char *) "enableLeftMergerToLVDS",   (PyCFunction) _PyMaltaSlowControl_enableLeftMergerToLVDS,  METH_VARARGS, NULL },
  {(char *) "enableMergerToRight",   (PyCFunction) _PyMaltaSlowControl_enableMergerToRight,  METH_VARARGS, NULL },
  {(char *) "enableMergerToLeft",   (PyCFunction) _PyMaltaSlowControl_enableMergerToLeft,  METH_VARARGS, NULL },
  {(char *) "enablePowerSwitch",   (PyCFunction) _PyMaltaSlowControl_enablePowerSwitch,  METH_VARARGS, NULL },
  {(char *) "readRegister",   (PyCFunction) _PyMaltaSlowControl_readRegister,  METH_VARARGS, NULL },
  {(char *) "writeReservedRegister",   (PyCFunction) _PyMaltaSlowControl_writeReservedRegister,  METH_VARARGS, NULL },
  {(char *) "selfTest",   (PyCFunction) _PyMaltaSlowControl_selfTest,  METH_NOARGS, NULL },
  {(char *) "readSEUCounter",   (PyCFunction) _PyMaltaSlowControl_readSEUCounter,  METH_NOARGS, NULL },
  {(char *) "readChipID",   (PyCFunction) _PyMaltaSlowControl_readChipID,  METH_NOARGS, NULL },
  {(char *) "getDiagonal",   (PyCFunction) _PyMaltaSlowControl_getDiagonal,  METH_VARARGS, NULL },
  {(char *) "maskPixel",   (PyCFunction) _PyMaltaSlowControl_maskPixel,  METH_VARARGS, NULL },

{NULL, NULL, 0, NULL}
};

PyTypeObject PyMaltaSlowControl_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "MaltaSlowControl",         /* tp_name */
    sizeof(PyMaltaSlowControl),          /* tp_basicsize */
    0,                                   /* tp_itemsize */
    (destructor) _PyMaltaSlowControl_dealloc,/* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    0,                                   /* tp_reserve */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyMaltaSlowControl_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyMaltaSlowControl_init,         /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
    ,0                                   /* tp_version_tag */
#endif
};

static PyMethodDef PyModule_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyMaltaSlowControl)
{
    PyObject *m;
    MOD_DEF(m, "PyMaltaSlowControl", NULL, PyModule_methods);
    if(PyType_Ready(&PyMaltaSlowControl_Type)<0){
      return MOD_ERROR;
    }
    PyModule_AddObject(m, (char *) "MaltaSlowControl", (PyObject *) &PyMaltaSlowControl_Type);
    return MOD_RETURN(m);
}
