#include "Malta/MaltaData.h"
#include <sstream>
#include <iostream>
#include <bitset>

using namespace std;

MaltaData::MaltaData(){
  m_refbit = 0;
  m_pixel = 0;
  m_group = 0;
  m_parity = 0;
  m_delay = 0;
  m_dcolumn = 0;
  m_chipbcid = 0;
  m_chipid = 0;
  m_phase = 0;
  m_winid = 0;
  m_l1id = 0;
  m_bcid = 0;
  m_word1 = 0;
  m_word2 = 0;
  m_bitshilo = 0;
  m_wordlength = 0;
  m_rows.resize(16);
  m_columns.resize(16);
  m_nTooLong=0;
  m_nWithGaps=0;
  m_nNotContinuous=0;
}

MaltaData::~MaltaData(){
  /*
    std::cout << "\nDiagnostics for duplicate-removal studies ...\n"
	    << "\tnTooLong:\t" << m_nTooLong
	    << "\n\tnWithGaps:\t" << m_nWithGaps
	    << "\n\tnNotContinuous:\t" << m_nNotContinuous
	    << std::endl;
  */
}

void MaltaData::setRefbit(uint32_t value){
  m_refbit = value;
}

void MaltaData::setPixel(uint32_t value){
  m_pixel = value;
}

void MaltaData::setGroup(uint32_t value){
  m_group = value;
}

void MaltaData::setParity(uint32_t value){
  m_parity = value;
}

void MaltaData::setDelay(uint32_t value){
  m_delay = value;
}

void MaltaData::setDcolumn(uint32_t value){
  m_dcolumn = value;
}

void MaltaData::setChipbcid(uint32_t value){
  m_chipbcid = value;
}

void MaltaData::setChipid(uint32_t value){
  m_chipid = value;
}

void MaltaData::setPhase(uint32_t value){
  m_phase = value;
}

void MaltaData::setWinid(uint32_t value){
  m_winid = value;
}

void MaltaData::setL1id(uint32_t value){
  m_l1id = value;
}

void MaltaData::setBcid(uint32_t value){
  m_bcid = value;
}

void MaltaData::setWord1(uint32_t value){
  m_word1 = value;
}

void MaltaData::setWord2(uint32_t value){
  m_word2 = value;
}

void MaltaData::setBitsHiLo(uint32_t value){
  m_bitshilo = value;
}

void MaltaData::setWordLength(uint32_t value){
  m_wordlength = value;
}


uint32_t MaltaData::getRefbit(){
  return m_refbit;
}

uint32_t MaltaData::getPixel(){
  return m_pixel;
}

uint32_t MaltaData::getGroup(){
  return m_group;
}

uint32_t MaltaData::getParity(){
  return m_parity;
}

uint32_t MaltaData::getDelay(){
  return m_delay;
}

uint32_t MaltaData::getDcolumn(){
  return m_dcolumn;
}

uint32_t MaltaData::getChipbcid(){
  return m_chipbcid;
}

uint32_t MaltaData::getChipid(){
  //return m_chipid;
  //std::cout << "Call single chip id: " << 1 << std::endl;
  return 0; //Florian: just return 1 by default, there is only one chip in the module anyway
}

uint32_t MaltaData::getDualChipid(){
  //the dual chip board is wired to put a 2 bit chip id into the chipBCID
  //chip 0 has id 0b00
  //chip 1 has id 0b11
  //std::cout << "Call dual chip id: " << bitset<2>(m_chipbcid) << std::endl;
  return (m_chipbcid == 0)?0:1;
}

uint32_t MaltaData::getQuadChipid(){
  //the quad board is wired to put the 3 chip id LSB into the delay bits (bits 23, 24, 25)
  //and the chip id MSB into the original chip id as per default MALTA word formatting (bit 36)
  //the chipid must be concatenated form the 3 LSB of m_delay and the 1 LSB from m_chipid
  //0xABCD   chipid
  //  ^      m_chipid LSB
  //   ^^^   m_delay 3xLSB
  //std::cout << "Call quad chip id: " << bitset<3>(m_delay) << "   " << bitset<1>(m_chipid) << std::endl;
  uint32_t chipid = 0;
  chipid = (m_delay & 0x7);
  chipid |= (m_chipid & 0x1) << 3;
  return (chipid);
}

uint32_t MaltaData::getPhase(){
  return m_phase;
}

uint32_t MaltaData::getWinid(){
  return m_winid;
}

uint32_t MaltaData::getNhits(){
  return m_nhits;
}

uint32_t MaltaData::getBcid(){
  return m_bcid;
}

uint32_t MaltaData::getL1id(){
  return m_l1id;
}

uint32_t MaltaData::getWord1(){
  return m_word1;
}

uint32_t MaltaData::getWord2(){
  return m_word2;
}

uint32_t MaltaData::getHitRow(uint32_t hit){
  return m_rows[hit];
}

uint32_t MaltaData::getHitColumn(uint32_t hit){
  return m_columns[hit];
}

uint32_t MaltaData::getBitsHiLo(){
  return m_bitshilo;
}

uint32_t MaltaData::getWordLength(){
  return m_wordlength;
}

void MaltaData::pack(){

  m_word1=0;
  m_word2=0;
  m_word1 |= (m_refbit   & 0x1)    << 0;
  m_word1 |= (m_pixel    & 0xFFFF) << 1;
  m_word1 |= (m_group    & 0x1F)   << 17;
  m_word1 |= (m_parity   & 0x1)    << 22;
  m_word1 |= (m_delay    & 0x7)    << 23;
  m_word1 |= (m_dcolumn  & 0x1F)   << 26;
  m_word2 |= (m_dcolumn  >> 5) & 0x7;
  m_word2 |= (m_chipbcid & 0x3)    << (34-31);
  m_word2 |= (m_chipid   & 0x1)    << (36-31);
  m_word2 |= (m_phase    & 0x7)    << (37-31);
  m_word2 |= (m_winid    & 0xF)    << (40-31);
  m_word2 |= (m_bcid     & 0x3F)   << (44-31);
  m_word2 |= (m_l1id     & 0xFFF)  << (50-31);

}


void MaltaData::unPack(){
  unpack();
}

void MaltaData::unpack(){
  m_refbit   = (m_word1 >>  0) & 0x1;
  m_pixel    = (m_word1 >>  1) & 0xFFFF;
  m_group    = (m_word1 >> 17) & 0x1F;
  m_parity   = (m_word1 >> 22) & 0x1;
  m_delay    = (m_word1 >> 23) & 0x7;
  m_dcolumn  = (m_word1 >> 26) & 0x1F;
  m_dcolumn |= ((m_word2 & 0x7) << 5);
  m_chipbcid = (m_word2 >> (34-31) & 0x3);
  m_chipid   = (m_word2 >> (36-31) & 0x1);
  m_phase    = (m_word2 >> (37-31) & 0x7);
  m_winid    = (m_word2 >> (40-31) & 0x7); // was 0xF
  m_bcid     = (m_word2 >> (44-31) & 0x1F); // was 0x3F
  m_l1id     = (m_word2 >> (50-31) & 0xFFF);

  /*
    //want to put this back? make it faster
  m_bitshilo  = (m_word1 >> 23) & 0x7;
  m_bitshilo |= ((m_word2 >> (34-31) & 0x3) << 0x3);
  m_bitshilo |= ((m_word2 >> (36-31) & 0x1) << 0x5);
  m_bitshilo |= ((m_word2 >> (43-31) & 0x1) << 0x6);
  m_bitshilo |= ((m_word2 >> (49-31) & 0x1) << 0x7);
  if (m_bitshilo==128) m_phase=7;

  m_wordlength = countSetBits(m_bitshilo); // need to count how many high bits in bitsHiLo

  // if word is too long, increment flag
  if(m_wordlength>5) m_nTooLong++;

  // if word has holes, increment flag
  // (int)log2(N)+1 gives number of bits in N
  bool first_set_bit=false;
  bool intermediate_unset_bit=false;
  bool has_a_gap=false;
  int size_of_gap = 0;
  for(uint32_t idx=0; idx<8; idx++)
    {
      bool isSet = bitAtGivenPosSetOrUnset(m_bitshilo,idx);
      if(isSet && !first_set_bit) first_set_bit=true;
      if(first_set_bit && !isSet)
	{
	  intermediate_unset_bit=true;
	  size_of_gap++;
	}
      if(first_set_bit && intermediate_unset_bit && isSet) has_a_gap=true;
      //else size_of_gap=0;

      //std::cout << idx << "\t" << isSet << "\t" << first_set_bit << "\t" << intermediate_unset_bit << "\t"
      //<< has_a_gap << "\t" << size_of_gap << std::endl;
    }

  if(has_a_gap) m_nWithGaps++;
  if(has_a_gap && size_of_gap<3) m_nNotContinuous;
  */

  m_nhits = 0;
  for(uint32_t i=0; i<16;i++){
    if(((m_pixel>>i)&0x1)==0){continue;}

    int column = m_dcolumn*2;
    if(i>7){column+=1;}

    int row = m_group*16;
    if(m_parity==1){row+=8;}
    if     (i==0 || i== 8){row+=0;}
    else if(i==1 || i== 9){row+=1;}
    else if(i==2 || i==10){row+=2;}
    else if(i==3 || i==11){row+=3;}
    else if(i==4 || i==12){row+=4;}
    else if(i==5 || i==13){row+=5;}
    else if(i==6 || i==14){row+=6;}
    else if(i==7 || i==15){row+=7;}
    m_rows[m_nhits]=row;
    m_columns[m_nhits]=column;
    m_nhits++;
  }
}

void MaltaData::setHit(uint32_t col, uint32_t row){

  m_refbit = 1;
  row=row&0x1FF;
  col=col&0x1FF;
  m_dcolumn = col>>1;
  m_group = row >> 4; //divide by 16 (32 groups)
  uint32_t rest = row - (m_group<<4); //multiply by 16
  m_parity = (rest>7?1:0);
  uint32_t pbit = (row%8) + (col%2==1?8:0);
  m_pixel = 1 << pbit;

}

string MaltaData::toString(){
  ostringstream os;
  os << "|" << bitset<12>(m_l1id)
     << "|" << bitset< 6>(m_bcid)
     << "|" << bitset< 4>(m_winid)
     << "|" << bitset< 3>(m_phase)
     << "|" << bitset< 1>(m_chipid)
     << "|" << bitset< 8>(m_dcolumn)
     << "|" << bitset< 3>(m_delay)
     << "|" << bitset< 1>(m_parity)
     << "|" << bitset< 5>(m_group)
     << "|" << bitset<16>(m_pixel)
     << "|" << bitset< 1>(m_refbit)
     << "|";
  return os.str();

}

string MaltaData::getInfo(){
  ostringstream os;
  os << "Refbit: " << getRefbit()
     << "  Pixel: " << getPixel()
     << "  Group: " << getGroup()
     << "  Parity: " << getParity()
     << "  Delay: " << getDelay()
     << "  DoubleCol: " << getDcolumn()
     << "  CHIPBCID: " << getChipbcid()
     << "  Phase: " << getPhase()
     << "  WinID: " << getWinid()
     << "  L1ID: " << getL1id()
     << "  BCID: " << getBcid();
  return os.str();
}

void MaltaData::dump(){
  cout << getInfo() << endl;
}

// recursive function to count the number of set bits in O(logN) time
// https://www.geeksforgeeks.org/count-set-bits-in-an-integer/
uint32_t MaltaData::countSetBits(uint32_t n)
{
  // base case
  if (n == 0)
    return 0;
  else
    // if last bit set add 1 else add 0
    return (n & 1) + countSetBits(n >> 1);
}

// check if k-th bit of number n is set or unset
// function to check whether the bit
// at given position is set or unset
// https://www.geeksforgeeks.org/check-whether-bit-given-position-set-unset/
bool MaltaData::bitAtGivenPosSetOrUnset(uint32_t n,
					uint32_t k)
{
  int new_num = n >> (k - 1);

  // if it results to '1' then bit is set,
  // else it results to '0' bit is unset
  return (new_num & 1);
}
