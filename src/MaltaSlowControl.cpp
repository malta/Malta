/*
WARNING: in the current version (@ 2018-02-15), the logic of masking/unmasking is inverted:
0 => mask
1 => unmask
This arises from observations during first tsts, probably to be confirmed
*/

#include "Malta/MaltaSlowControl.h"
#include<bitset>

using namespace std;

MaltaSlowControl::MaltaSlowControl(){}

MaltaSlowControl::~MaltaSlowControl(){}

////////
// reset
////////
 
unsigned int MaltaSlowControl::resetCMU() const{
  unsigned int val = pOPCODE_RESET;
  val = (val << 2) | pRESET_CMU;
  val = (val << 10) | 0; // dummy
  return val;
}

unsigned int MaltaSlowControl::generateReset(const unsigned int nClockCycles) const{ // nClockCycles between 0 and 1023
  unsigned int val = pOPCODE_RESET;
  val = (val << 2) | pGENERATE_RST_N;
  val = (val << 10) | nClockCycles;
  return val;
}

unsigned int MaltaSlowControl::resetSCLRRegister(const unsigned int registerID) const{
  unsigned int val = pOPCODE_RESET;
  val = (val << 2) | pSCLRREGISTER;
  if(registerID < 16){
    val = (val << 1) | 0; // dummy
    val = (val << 5) | registerID;
    val = (val << 4) | 0; // dummy
  }
  else{
    val = (val << 6) | registerID;
    val = (val << 4) | 0; // dummy
  }
  return val;
}

///////
// mask
///////

unsigned int MaltaSlowControl::maskPixelCol(const unsigned int col) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_COL;
  val = (val << 1) | 1; // enable, inverted logic!
  val = (val << 9) | col;
  //std::cout << "maskPixelCol\t" << "\t" << bitset<16>(val) << std::endl;
  return val;
}

unsigned int MaltaSlowControl::maskPixelHor(const unsigned int hor) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_HOR;
  val = (val << 1) | 1; // enable, inverted logic!
  val = (val << 9) | hor;
  return val;
}

unsigned int MaltaSlowControl::maskPixelDiag(const unsigned int diag) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_DIAG;
  val = (val << 1) | 1; // enable, inverted logic!
  val = (val << 9) | diag;
  return val;
}

unsigned int MaltaSlowControl::unmaskPixelCol(const unsigned int col) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_COL;
  val = (val << 1) | 0; // disable, inverted logic!
  val = (val << 9) | col;
  return val;
}

unsigned int MaltaSlowControl::unmaskPixelHor(const unsigned int hor) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_HOR;
  val = (val << 1) | 0; // disable, not inverted logic!
  val = (val << 9) | hor;
  return val;
}

unsigned int MaltaSlowControl::unmaskPixelDiag(const unsigned int diag) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_DIAG;
  val = (val << 1) | 0; // disable, not inverted logic!
  val = (val << 9) | diag;
  return val;
}

unsigned int MaltaSlowControl::maskDoubleColumn(const unsigned int doubleColumn) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_FULLCOL;
  val = (val << 1) | 1; // enable, not inverted logic!
  val = (val << 1) | 0; // dummy
  val = (val << 8) | doubleColumn;
  return val;
}

unsigned int MaltaSlowControl::unmaskDoubleColumn(const unsigned int doubleColumn) const{
  unsigned int val = pOPCODE_MASK;
  val = (val << 2) | pMASK_FULLCOL;
  val = (val << 1) | 0; // disable, not inverted logic!
  val = (val << 1) | 0; // dummy
  val = (val << 8) | doubleColumn;
  return val;
}


////////
// pulse
////////

unsigned int MaltaSlowControl::enablePulsePixelCol(const unsigned int col) const{
  unsigned int val = pOPCODE_PULSE;
  val = (val << 2) | pPULSE_COL;
  val = (val << 1) | 1; // enable
  val = (val << 9) | col;
  return val;
}

unsigned int MaltaSlowControl::enablePulsePixelHor(const unsigned int hor) const{
  unsigned int val = pOPCODE_PULSE;
  val = (val << 2) | pPULSE_HOR;
  val = (val << 1) | 1; // enable
  val = (val << 9) | hor;
  return val;
}

unsigned int MaltaSlowControl::disablePulsePixelCol(const unsigned int col) const{
  unsigned int val = pOPCODE_PULSE;
  val = (val << 2) | pPULSE_COL;
  val = (val << 1) | 0; // disable
  val = (val << 9) | col;
  return val;
}

unsigned int MaltaSlowControl::disablePulsePixelHor(const unsigned int hor) const{
  unsigned int val = pOPCODE_PULSE;
  val = (val << 2) | pPULSE_HOR;
  val = (val << 1) | 0; // disable
  val = (val << 9) | hor;
  return val;
}

unsigned int MaltaSlowControl::enablePulseFixedPattern() const{
  unsigned int val = pOPCODE_PULSE;
  val = (val << 2) | pPULSE_FIXED;
  val = (val << 10) | 0; // dummy
  return val;
}

vector<unsigned int> MaltaSlowControl::disablePulseFixedPattern() const{
  vector<unsigned int> val;
  val.push_back(disablePulsePixelCol(5));
  val.push_back(disablePulsePixelHor(511));
  val.push_back(disablePulsePixelHor(251));
  val.push_back(disablePulsePixelHor(0));
  return val;
}

////////
// delay
////////

unsigned int MaltaSlowControl::delay(const unsigned int picoseconds) const{
  unsigned int val = pOPCODE_DELAY;
  val = (val << 8) | 0; // dummy
  if     (picoseconds == 2000) val = (val << 4) | delay2000;
  else if(picoseconds == 1000) val = (val << 4) | delay1000;
  else if(picoseconds ==  750) val = (val << 4) | delay0750;
  else if(picoseconds ==  500) val = (val << 4) | delay0500;
  else{
    cout << __PRETTY_FUNCTION__ << ": WARNING!!! - invalid picoseconds value = " << dec << picoseconds << endl;
    return 0;
  }
  return val;
}

//////
// DAC
//////

unsigned int MaltaSlowControl::setVCASN(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_VCASN;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setVCLIP(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_VCLIP;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setVPULSE_HIGH(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_VPULSE_HIGH;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setVPULSE_LOW(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_VPULSE_LOW;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setVRESET_P(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_VRESET_P;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setVRESET_D(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_VRESET_D;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setICASN(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_ICASN;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setIRESET(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IRESET;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setITHR(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_ITHR;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setIBIAS(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IBIAS;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setIDB(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IDB;
  val = (val << 7) | value;
  return val;
}

unsigned int MaltaSlowControl::setIBUFP_MON0(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IBUFP_MON0;
  val = (val << 3) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::setIBUFN_MON0(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IBUFN_MON0;
  val = (val << 3) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::setIBUFP_MON1(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IBUFP_MON1;
  val = (val << 3) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::setIBUFN_MON1(const unsigned int value) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRSET_IBUFN_MON1;
  val = (val << 3) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::switchDACMONI(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_DACMONI;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchDACMONV(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_DACMONV;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::setIRESET_BIT(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SET_IRESET_BIT;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchIREF(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_IREF;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchIDB(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_IDB;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchITHR(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_ITHR;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchIBIAS(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_IBIAS;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchIRESET(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_IRESET;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchICASN(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_ICASN;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchVRESET_D(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_VRESET_D;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchVRESET_P(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_VRESET_P;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchVPULSE_LOW(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_VPULSE_LOW;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchVPULSE_HIGH(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_VPULSE_HIGH;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchVCLIP(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_VCLIP;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::switchVCASN(const bool on) const{
  unsigned int val = pOPCODE_DAC;
  val = (val << 5) | pRDACSWITCH;
  val = (val << 4) | pPOS_SWCNTL_VCASN;
  val = (val << 2) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

///////
// LVDS
///////

unsigned int MaltaSlowControl::enableLVDS(const bool on) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_LVDS_ENABLE;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::configPreEmphasis(const string type, 
                                                 const unsigned int value) const{ // type is "5=>0", "10=>6" or "15=>11"
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_EN_PRE;
  if(type == "5=>0"){
    val = (val << 2) | pEN_PRE_5downto0;
    val = (val << 6) | value;
  }
  else if(type == "10=>6"){
    val = (val << 2) | pEN_PRE_10downto6;
    val = (val << 1) | 0; // dummy
    val = (val << 5) | value;
  }
  else if(type == "15=>11"){
    val = (val << 2) | pEN_PRE_15downto11;
    val = (val << 1) | 0; // dummy
    val = (val << 5) | value;
  }
  else{
    cout << __PRETTY_FUNCTION__ << ": WARNING!!! - invalid type = " << type << endl;
    return 0;
  }
  return val;
}

unsigned int MaltaSlowControl::configHBridge(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_EN_HBRIDGE;
  val = (val << 3) | 0; // dummy
  val = (val << 5) | value;
  return val;
}

unsigned int MaltaSlowControl::configCMFB(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_EN_CMFB;
  val = (val << 3) | 0; // dummy
  val = (val << 5) | value;
  return val;
}

unsigned int MaltaSlowControl::configIBCMFB(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_SET_IBCMFB;
  val = (val << 4) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::configIVPH(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_SET_IVPH;
  val = (val << 4) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::configIVPL(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_SET_IVPL;
  val = (val << 4) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::configIVNH(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_SET_IVNH;
  val = (val << 4) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

unsigned int MaltaSlowControl::configIVNL(const unsigned int value) const{
  unsigned int val = pOPCODE_LVDS;
  val = (val << 4) | pCONFIG_SET_IVNL;
  val = (val << 4) | 0; // dummy
  val = (val << 4) | value;
  return val;
}

////////////
// data flow
////////////

unsigned int MaltaSlowControl::enableMerger(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_ENABLEMERGER;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableCMOSRight(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_ENABLECMOSRIGHT;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableCMOSLeft(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_ENABLECMOSLEFT;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableDataflowLVDS(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_ENABLELVDS;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableRightCMOSTop(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_RIGHTCMOSTOP;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableLeftCMOSTop(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_LEFTCMOSTOP;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableRightMergerToCMOS(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_RIGHTMERGERTOCMOS;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableRightMergerToLVDS(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_RIGHTMERGERTOLVDS;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableLeftMergerToCMOS(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_LEFTMERGERTOCMOS;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableLeftMergerToLVDS(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_LEFTMERGERTOLVDS;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableMergerToRight(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_MERGERTORIGHT;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

unsigned int MaltaSlowControl::enableMergerToLeft(const bool on) const{
  unsigned int val = pOPCODE_DATAFLOW;
  val = (val << 4) | pPOS_RDATAFLOW_MERGERTOLEFT;
  val = (val << 7) | 0; // dummy
  if(on) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

///////////////
// power switch
///////////////

unsigned int MaltaSlowControl::enablePowerSwitch(const bool left,
                                                 const bool right) const{
  unsigned int val = pOPCODE_POWERSWITCH;
  val = (val << 10) | 0; // dummy
  if(left) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  if(right) val = (val << 1) | 1;
  else val = (val << 1) | 0;
  return val;
}

////////////////
// read register
////////////////

unsigned int MaltaSlowControl::readRegister(const unsigned int registerID) const{
  unsigned int val = pOPCODE_READREGISTER;
  val = (val << 3) | 0; // dummy
  val = (val << 5) | registerID; 
  val = (val << 4) | 0; // dummy
  return val;
}

//////////////////////////
// write reserved register
//////////////////////////

unsigned int MaltaSlowControl::writeReservedRegister(const unsigned int registerID,
                                                     const unsigned int value) const{
  unsigned int val = pOPCODE_WRRESERVEDREG;
  if(registerID == 0) val = (val << 3) | pWRITE_RESERVEDREG0; 
  else if(registerID == 1) val = (val << 3) | pWRITE_RESERVEDREG1; 
  else if(registerID == 2) val = (val << 3) | pWRITE_RESERVEDREG2; 
  else if(registerID == 3) val = (val << 3) | pWRITE_RESERVEDREG3; 
  else if(registerID == 4) val = (val << 3) | pWRITE_RESERVEDREG4; 
  else{
    cout << __PRETTY_FUNCTION__ << ": WARNING!!! - invalid register ID = " << registerID << endl;
    return 0;
  }
  val = (val << 9) | (0x1FF & value); 
  return val;
}

////////////
// self test
////////////

unsigned int MaltaSlowControl::selfTest() const{
  unsigned int val = pOPCODE_SELFTEST;
  val = (val << 12) | 0; // dummy
  return val;
}

///////////////////
// read SEU counter
///////////////////

unsigned int MaltaSlowControl::readSEUCounter() const{
  unsigned int val = pOPCODE_READSEUCOUNTER;
  val = (val << 12) | 0; // dummy
  return val;
}

//////////////
// read chipID
//////////////

unsigned int MaltaSlowControl::readChipID() const{
  unsigned int val = pOPCODE_READCHIPID;
  val = (val << 12) | 0; // dummy
  return val;
}

////////////////////////
// higher level commands
////////////////////////

unsigned int MaltaSlowControl::getDiagonal(const unsigned int col,
                                           const unsigned int hor) const{
  int diag = hor-col;
  if(diag < 0) diag += 512;
  return (unsigned int) diag;
}

vector<unsigned int> MaltaSlowControl::maskPixel(const unsigned int col,
                                                 const unsigned int hor,
                                                 bool mask) const{
  vector<unsigned int> commands;
  if(mask){
    commands.push_back(maskPixelCol(col));
    commands.push_back(maskPixelHor(hor));
    commands.push_back(maskPixelDiag(getDiagonal(col, hor)));
    // sticky-pixel fix
    commands.push_back(maskPixelCol(col));
    commands.push_back(maskPixelCol(col));
    commands.push_back(maskPixelCol(col));
    commands.push_back(maskPixelCol(col));

    // sticky-pixel fix                                                                              
    commands.push_back(maskPixelHor(hor));
    commands.push_back(maskPixelHor(hor));
    commands.push_back(maskPixelHor(hor));
    commands.push_back(maskPixelHor(hor));

    // stickier-pixel fix
    commands.push_back(maskPixelDiag(getDiagonal(col, hor)));
    commands.push_back(maskPixelDiag(getDiagonal(col, hor)));
    commands.push_back(maskPixelDiag(getDiagonal(col, hor)));
    commands.push_back(maskPixelDiag(getDiagonal(col, hor)));    

  }else{
    commands.push_back(unmaskPixelCol(col));
    commands.push_back(unmaskPixelHor(hor));
    commands.push_back(unmaskPixelDiag(getDiagonal(col, hor)));
  }


  return commands;
}
