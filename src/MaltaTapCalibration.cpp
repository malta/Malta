//! -*-C++-*-
#include "Malta/MaltaData.h"
#include "Malta/MaltaBase.h"
#include "Malta/MaltaTree.h"

#include <cmdl/cmdargs.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include <unistd.h>
#include <sys/stat.h>

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"
#include "TLegend.h"

using namespace std;

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}


int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to MALTA tap calibration  #" << endl
       << "#####################################" << endl;

  CmdArgInt    cNpulses ( 'n',"npulses","npulses","number of pulses default 100");
  CmdArgStr    cInput   ( 'i',"input","input","input file with tap values");
  CmdArgStr    cWrite   ( 'w',"write","write","output file with tap values");
  CmdArgInt    cTap     ( 't',"tap","tap","tap duration in picoseconds default 80");
  CmdArgInt    cOffset  ( 'O',"tapoffset","tapoffset","offset between tap1 and tap2 default 3");
  CmdArgStr    cConfig  ( 'c',"config","config","configuration file");
  CmdArgInt    cPixx    ( 'x',"pixX","pixX","pulsing pixX", CmdArg::isREQ); 
  CmdArgInt    cPixy    ( 'y',"pixY","pixY","pulsing pixY", CmdArg::isREQ); 
  CmdArgBool   cShowPlot( 'p',"plot","show plot"); 
  CmdArgStr    cAddress ( 'a',"address","address","udp://ep-ade-gw-0x.cern.ch:5000x", CmdArg::isREQ);
  CmdArgStr    cOutdir  ( 'o',"output","output","output directory", CmdArg::isREQ);
  CmdArgStr    cPath    ( 'f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgBool   cDut     ( 'd',"dut","is DUT");

  cout<< "parsing command line..."<< endl;
  CmdLine cmdl(*argv,&cNpulses,&cInput,&cWrite,&cPath,&cTap,&cOffset,&cConfig,&cPixx,&cPixy,&cShowPlot,&cAddress,&cOutdir,&cDut,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  int npulses = (cNpulses.flags() & CmdArg::GIVEN ? cNpulses:100);
  string inputFile =  (cInput.flags()  & CmdArg::GIVEN ? cInput: "");
  bool isDefault = true;
  if (inputFile!="") isDefault=false; 
 
  string baseDir=getenv("MALTA_DATA_PATH");
  baseDir+="/Malta/Results_TapCalib/";
  string outdir = baseDir+"/"+string(cPath)+"/"+string (cOutdir);
  if (outdir!="") systemcall("mkdir -p "+outdir);

  string write = (cWrite.flags()  & CmdArg::GIVEN ? cWrite: "taps.txt");
  write = outdir+"/"+write;
  bool showplot = (cShowPlot.flags()  & CmdArg::GIVEN ? cShowPlot: false);
  int tapLength = 80; //(cTap.flags() & CmdArg::GIVEN ? cTap:80);
  int tapOffset =  3; //(cOffset.flags() & CmdArg::GIVEN ? cOffset:3);

  int sampleLength=390;
  int lowerLimit  =10;

  MaltaBase * malta = new MaltaBase( string(cAddress).c_str() );
  malta->SetDUT(cDut);

  malta->Reset();
  malta->SetExternalL1A(false);
  malta->SetConfigMode(true,0.8);
  malta->PreConfigMaltaC();
  if(cConfig.flags() & CmdArg::GIVEN){malta->SetConfigFromFile(string(cConfig));}
  malta->SetVPULSE_HIGH(90);
  malta->SetVPULSE_LOW(10);
  malta->SetPixelPulseColumn(5,false);
  malta->SetPixelPulseColumn(5,false);
  malta->SetPixelPulseRow(  0,false);
  malta->SetPixelPulseRow(  0,false);
  malta->SetPixelPulseRow(251,false);
  malta->SetPixelPulseRow(251,false);
  malta->SetPixelPulseRow(511,false);
  malta->SetPixelPulseRow(511,false);
  for (int i =0; i < 256; ++i){
    malta->SetDoubleColumnMask( i,true);
    malta->SetDoubleColumnMask( i,true);
  }
  for (int i =0; i < 512; ++i){
    malta->SetPixelMaskColumn(i,true);
    malta->SetPixelMaskColumn(i,true);
    malta->SetPixelMaskDiag(i,true);
    malta->SetPixelMaskDiag(i,true);
    malta->SetPixelMaskRow(i,true);
    malta->SetPixelMaskRow(i,true);
  }

  int pixY=cPixy;
  int pixX=cPixx;

  malta->SetPixelPulse(pixY,pixX,true);
  malta->SetPixelPulse(pixY,pixX,true);
  malta->SetDoubleColumnMask( (int)(pixX/2.),false);
  malta->SetDoubleColumnMask( (int)(pixX/2.),false);
  malta->SetPixelMaskRow(pixY,false);
  malta->SetPixelMaskRow(pixY,false);
  malta->SetPixelMaskColumn(pixX,false);
  malta->SetPixelMaskColumn(pixX,false);

  malta->SetConfigMode(false);


  malta->SetReadoutDelay(80);//was35
  // malta->SetHalfColumns(true); //VD last
  // malta->SetHalfRows(false);   //VD last
  if (isDefault){
    std::cout<< "Writing default taps" << std:: endl;
    malta->WriteConstDelays(13,10);///4,1-tapOffset);
    sleep(1);  // sleep in ms 
  }
  else {
      cout << "Reading taps from: " << inputFile << endl;
      malta->ReadTapsFromFile(outdir+"/"+inputFile);   ///ANDREA to be defined
    
  }
 
  // malta->readAllDelays(ipb,True); //ANDREA TO BE defined 

  //////////////////////////////////////////////////////////
  ////////////// Collecting Datastream /////////////////////
  //////////////////////////////////////////////////////////


  TH2D * h1= new TH2D("h2",";sample;bit",16,0,16,37,0,37);
  std::vector<TLine*>  lines;
  TLine *tmpL= new TLine(0,1.0,16,1.0);
  tmpL->SetLineWidth(1);
  tmpL->SetLineStyle(2);
  lines.push_back(tmpL);

  TLine *tmpL2= new TLine(0,17,16,17);
  tmpL2->SetLineWidth(1);
  tmpL2->SetLineStyle(2);
  lines.push_back(tmpL2);

  TLine *tmpL3= new TLine(0,+22,16,22);
  tmpL3->SetLineWidth(1);
  tmpL3->SetLineStyle(2);
  lines.push_back(tmpL3);

  TLine *tmpL4= new TLine(0,+23,16,23);
  tmpL4->SetLineWidth(1);
  tmpL4->SetLineStyle(2);
  lines.push_back(tmpL4);

  TLine *tmpL5= new TLine(0,+26,16,26);
  tmpL5->SetLineWidth(1);
  tmpL5->SetLineStyle(2);
  lines.push_back(tmpL5);

  TLine *tmpL6= new TLine(0,+34,16,34);
  tmpL6->SetLineWidth(1);
  tmpL6->SetLineStyle(2);
  lines.push_back(tmpL6);

  TLine *tmpL7= new TLine(0,+36,16,36);
  tmpL7->SetLineWidth(1);
  tmpL7->SetLineStyle(2);
  lines.push_back(tmpL7);

  
  //malta->SetConfigMode(true);
  //if (mode!=2){
  //  malta->SetPixelPulseColumn(theCol, true); /// ANDREA check true
  //  if (theCol2!=-1) malta->SetPixelPulseColumn(theCol2, true); /// ANDREA check true
  //  malta->SetPixelPulseRow(theRow, true); /// ANDREA check true
  //}
  //sleep(1); //ms
  /// ANDREA check swap true false
  //malta->SetConfigMode(false);


  //malta->ReadoutOn();
 
  malta->ReadoutOff();
  malta->ResetL1Counter();
  malta->ResetFifo();
  malta->ReadoutOn();

  std::cout << "start pulsing ..."+std::to_string(npulses)+" TIMES" << std::endl;
  malta->Trigger(npulses, true);
  malta->ReadoutOff();

  malta->ReadFifoStatus();
  bool fifoMonEmpty = malta->IsFifoMonEmpty();
  bool fifo1Empty = malta->IsFifo1Empty();
  bool fifo2Empty = malta->IsFifo2Empty();
  int nEntry = 0;
  cout << endl <<"fifoMonEmpty  "  << fifoMonEmpty << endl;
  cout << endl <<"fifo1Empty  "  << fifo1Empty << endl;
  cout << endl <<"fifo2Empty  "  << fifo2Empty << endl;
  uint32_t *wordsDebug= new uint32_t [37];
  
  while ( fifoMonEmpty ==false && nEntry<5000){
    //cout << " " << nEntry << endl;
    malta->ReadMonitorWord(wordsDebug);
    //for (int c=0; c<37; c++) {
    //  std::cout << " " << c << " = " << std::bitset<32>(wordsDebug[c])  << std::endl;
    //}
    //cout << "=================================================================" << endl;

    if (wordsDebug[0]==0){ 
      std::cout <<  "Empty word ... this should never happen!!" << std::endl;
      break;
    }
    uint32_t start=0;
    for (int bit =0 ; bit < 16; ++bit){
      if (((wordsDebug[0]>>(bit))&0x1)==1){  // removed wordsDebug[0]
        start=bit;
        break;
      }
    }
    /// 37 by hand ANDREA channels
    for (int j =0 ; j< 37; ++j){
      for (int b=0; b< 16;++b){
        if (((wordsDebug[j]>>(b))&0x1)==0) continue; //removed wordsDebug[j]
        h1->Fill(b-start+2,j);
      }
    }
    nEntry+=1;
    malta->ReadFifoStatus();
    fifoMonEmpty = malta->IsFifoMonEmpty();
    //cout << endl <<"fifoMonEmpty  "  << fifoMonEmpty << endl;
  }

  std::cout <<"Number of collected hits is: "+std::to_string(nEntry) << std::endl;

  TCanvas *c1= new TCanvas("c1","c1",600,900);
  gStyle->SetOptStat(0);
  h1->Draw("COLZtext");
  for (unsigned int i =0 ; i < lines.size(); ++i ){
    lines.at(i)->Draw("SAMEL");
  }

 
  string fileName=outdir+"/CalibProcedure_M";

//  if (write!=""){
//    fileName+="_BEFORE";
//  }
//  else
//    fileName+="_AFTER";
  string filename_complete = fileName+"_pixX_"+to_string(pixX) + +"_pixY_"+to_string(pixY);
  c1->SaveAs((filename_complete+".pdf").c_str());
  TFile *oFile= new TFile((filename_complete+".root").c_str(),"RECREATE");
  c1->Write("canvas");
  h1->Write("histo");
  //oFile->Close();
  if (showplot) systemcall("evince "+filename_complete+".pdf &");
  





  //#############################################################
  //# Calculating Tap Corrections ###############################
  //#############################################################

  //#######mg1 = ROOT.TMultiGraph()
  //###g1 = {}

  std::vector<float>  mean ;
  std::vector<float>  ymean ;
  std::vector<float>  zSumPerYbin;


  for (int y =0 ; y < h1->GetNbinsY(); ++y){
    TGraph g1;
    ymean.push_back(0);
    zSumPerYbin.push_back(0);
    for (int x =0 ; x < h1->GetNbinsX(); ++x){
      //stability protection at least 5 hits per bin
      if (h1->GetBinContent(x+1,y+1)<lowerLimit) continue;
      ymean.at(y)+=h1->GetBinContent(x+1,y+1)*(x+1); //ANDREA why +1??
      zSumPerYbin.at(y)+=h1->GetBinContent(x+1,y+1);
      //cout << h1->GetBinContent(x+1,y+1) << " " << h1->GetBinContent(x+1,y+1)*(x+1) << endl;

    }


  } 
  //############################################################################################################
  //#print "second part"


  std::vector<float> distance_to_ref;
  for (int y =0 ; y < h1->GetNbinsY(); ++y){
    if (ymean.at(y)==0) mean.push_back(0);
    else mean.push_back(ymean.at(y)/zSumPerYbin.at(y));
    float tmp = (sampleLength*(mean.at(0)-mean.at(y)))/ tapLength;

    if (tmp>15)
      distance_to_ref.push_back(-99);
    else 
       distance_to_ref.push_back(tmp);

   if(nEntry==0)distance_to_ref.at(y) = -666;
  }    

  TTree* avg = new TTree("corrections_raw","corrections_raw");
  avg->Branch("pixX",&pixX,"pixX/I");
  avg->Branch("pixY",&pixY,"pixY/I");
  avg->Branch("tap_per_bit",&distance_to_ref);//,"distance_to_ref/F");

  avg->Fill();

  avg->Write();
  oFile->Close();

  //############################################################################################################
  int diff = 0;
  int count = 0;
  std::vector<float> v_taps1;

  for (unsigned int j =0; j < mean.size(); ++j){
    count+=1;
    if (j==0) {
       v_taps1.push_back(0);
       continue;
    }
    float distanceToLatest=( (mean[0]-mean[j])*sampleLength );
    string theString= ("channel: "+to_string(count)+("\ttime difference (in ps): " +to_string(distanceToLatest)));  

    if (abs(distanceToLatest)<tapLength/2){
      theString+=("\t# of taps: 0");
      v_taps1.push_back(0);
    }
    else if (abs(distanceToLatest)>(15*tapLength)){
      cout<< "WARNING: DISTANCE TOO LARGE ... I WOULD NOT BEABLE TO COMPENSATE!!!  "+to_string(distanceToLatest) << endl;
      v_taps1.push_back(0);
      continue;
    } 
    else{                              
      float tapShift=1.*distanceToLatest/tapLength;
      theString+=("\t# of taps: "+to_string(tapShift)+" translate into " +to_string( round(tapShift)) );
      v_taps1.push_back( (round(tapShift)) );
    }
    std::cout <<  theString << std::endl;
  }
  if(write!=""){
    std:: cout << ("Writing Corrected Tap Values") << endl;
    for (int chn=0; chn < 37; chn++){
        vector<uint32_t> previous=malta->ReadTap(chn);
        float tapCorrection=v_taps1.at(chn);
        float val1=previous[0]+tapCorrection;
        float val2=val1-tapOffset;
	//cout << " I am writing tapp: " << chn << " , " << val1 << " , " << val2 << endl; 
        malta->WriteTap(chn, val1, val2);
    }
    std::cout<< "Correction taps written to FPGA." << endl;
    malta->WriteTapsToFile(write);
  }

  cout << "Cleaning the house" << endl;
  delete malta;
  cout << "Have a nice day" << endl;



}
