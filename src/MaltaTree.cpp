#include "Malta/MaltaTree.h"

using namespace std;

MaltaTree::MaltaTree(){
  pixel=0;
  group=0;
  parity=0;
  delay=0;
  dcolumn=0;
  chipbcid=0;
  chipid=0;
  phase=0;
  winid=0;
  bcid=0;
  run=0;
  l1id=0;
  l1idC=0;
  isDuplicate=0;
  ithres=0;
  timer=0;
  word1=0;
  word2=0;
  coordX=0;
  coordY=0;
  vlow=0;
  bitsHiLo=0;
  wordLength=0;
  m_entry=0;
  m_file=0;
  m_readonly=false;
}

MaltaTree::~MaltaTree(){
  if(m_file) delete m_file;
}

void MaltaTree::Open(string filename, string options){
  m_file = TFile::Open(filename.c_str(),options.c_str());
  if(options=="READ"){
    m_readonly=true;
    m_tree = (TTree*)m_file->Get("MALTA");
    m_tree->SetBranchAddress("pixel", &pixel);
    m_tree->SetBranchAddress("group", &group);
    m_tree->SetBranchAddress("parity", &parity);
    m_tree->SetBranchAddress("delay", &delay);
    m_tree->SetBranchAddress("dcolumn", &dcolumn);
    m_tree->SetBranchAddress("chipbcid", &chipbcid);
    m_tree->SetBranchAddress("chipid", &chipid);
    m_tree->SetBranchAddress("phase", &phase);
    m_tree->SetBranchAddress("winid", &winid);
    m_tree->SetBranchAddress("bcid", &bcid);
    m_tree->SetBranchAddress("runNumber", &run);
    m_tree->SetBranchAddress("l1id", &l1id);
    m_tree->SetBranchAddress("l1idC", &l1idC);
    m_tree->SetBranchAddress("isDuplicate", &isDuplicate);
    m_tree->SetBranchAddress("ithres", &ithres);
    m_tree->SetBranchAddress("timer", &timer);
    m_tree->SetBranchAddress("bitsHiLo", &bitsHiLo);
    m_tree->SetBranchAddress("wordLength", &wordLength);

  }else{
    m_tree = new TTree("MALTA","DataStream");
    m_tree->Branch("pixel",&pixel,"pixel/i");
    m_tree->Branch("group",&group,"group/i");
    m_tree->Branch("parity",&parity,"parity/i");
    m_tree->Branch("delay",&delay,"delay/i");
    m_tree->Branch("dcolumn",&dcolumn,"dcolumn/i");
    m_tree->Branch("chipbcid",&chipbcid,"chipbcid/i");
    m_tree->Branch("chipid",&chipid,"chipid/i");
    m_tree->Branch("phase",&phase,"phase/i");
    m_tree->Branch("winid",&winid,"winid/i");
    m_tree->Branch("bcid",&bcid,"bcid/i");
    m_tree->Branch("runNumber",&run,"runNumber/i");
    m_tree->Branch("l1id",&l1id,"l1id/i");
    m_tree->Branch("l1idC",&l1idC,"l1idC/i"); //Valerio
    m_tree->Branch("isDuplicate",&isDuplicate,"isDuplicate/i");
    m_tree->Branch("ithres",&ithres,"ithres/i");
    m_tree->Branch("timer",&timer,"timer/f");
    m_tree->Branch("bitsHiLo",&bitsHiLo,"bitsHiLo/i");
    m_tree->Branch("wordLength",&wordLength,"wordLength/i");

    m_t0 = chrono::steady_clock::now();
  }
  m_entry = 0;
}

void MaltaTree::Set(MaltaData * data, int modulesize){
  pixel = data->getPixel();
  group = data->getGroup();
  parity = data->getParity();
  delay = data->getDelay();
  dcolumn = data->getDcolumn();
  chipbcid = data->getChipbcid();
  if(modulesize == 2){
    chipid = data->getDualChipid();
  } else if(modulesize == 4){
    chipid = data->getQuadChipid();
  } else {
    chipid = data->getChipid();
  }
  phase = data->getPhase();
  winid = data->getWinid();
  bcid = data->getBcid();
  l1id = data->getL1id();
  timer = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-m_t0).count()/1000.;
  word1 = data->getWord1();
  word2 = data->getWord2();
  bitsHiLo = data->getBitsHiLo();
  wordLength = data->getWordLength();
}

int MaltaTree::GetIthres(){
  return ithres;
}

int MaltaTree::GetRunNumber(){
  return run;
}

uint32_t MaltaTree::GetL1idC(){
  return l1idC;
}

int MaltaTree::GetIsDuplicate(){
  return isDuplicate;
}

float MaltaTree::GetTimer(){
  return timer;
}

float MaltaTree::GetBitsHiLo(){
  return bitsHiLo;
}

float MaltaTree::GetWordLength(){
  return wordLength;
}


void MaltaTree::SetIthres(int v){
  ithres = v;
}

void MaltaTree::SetRunNumber(int v){
  run = v;
}

void MaltaTree::SetL1idC(uint32_t v){
  l1idC = v;
}

void MaltaTree::SetIsDuplicate(int v){
  isDuplicate = v;
}

void MaltaTree::Fill(){
  m_tree->Fill();
}

int MaltaTree::Next(){
  if(m_tree->LoadTree(m_entry)<0) return 0;
  m_tree->GetEntry(m_entry);
  m_entry++;
  return m_entry;
}

TFile* MaltaTree::GetFile(){
  return m_file;
}

MaltaData * MaltaTree::Get(){
  m_data.setPixel(pixel);
  m_data.setGroup(group);
  m_data.setParity(parity);
  m_data.setDelay(delay);
  m_data.setDcolumn(dcolumn);
  m_data.setChipbcid(chipbcid);
  m_data.setChipid(chipid);
  m_data.setPhase(phase);
  m_data.setWinid(winid);
  m_data.setBcid(bcid);
  m_data.setL1id(l1id);
  m_data.setBitsHiLo(bitsHiLo);
  m_data.setWordLength(wordLength);
  m_data.pack();
  m_data.unpack();
  return &m_data;
}

void MaltaTree::Close(){
  if(!m_readonly){
    m_file->cd();
    m_tree->Write();
  }
  m_file->Close();
}
