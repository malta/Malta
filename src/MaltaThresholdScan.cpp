//! -*-C++-*-
#include "Malta/MaltaData.h"
#include "Malta/MaltaBase.h"
#include "Malta/MaltaTree.h"
#include "Malta/MaltaThresholdAnalysis.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>

using namespace std;
bool g_cont=true;
long int Scantime=1;//dummy
float Scantime_pix[512*512];//dummy
int rescan_first_point = 0;
int rescan_first_point_max = 3;
int target =0;
int m_thX = 1;
int m_thY= 10;
vector<int> *m_measured_points = NULL;
vector<int> *m_measured_triggers = NULL;
bool stop_scan = false;
bool find_middle = false;
float mainSleep=0.2;
TTree *m_config_tree;
std::vector<int>  config_value;
std::vector<string>  config_name;

//float mainSleep2=0.1;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

void configureChip(MaltaBase * malta, string cConfig, bool firstconfiguration=false ){

    ifstream fr;
    fr.open(string(cConfig).c_str());
    if (!fr.is_open()){
      cout << "#####################################################"<<endl;
      cout <<      "##  The configuration file cannot be opened        ##" << endl;
      cout << "Stopping script"<<endl;
      exit(0);
    }
    malta->Reset();
    malta->SetConfigMode(true,0.8);
    malta->PreConfigMaltaC();
    malta->SetConfigFromFile(string(cConfig));

    malta->SetVPULSE_HIGH(90);

    malta->SetPixelPulseColumn(5,false);
    malta->SetPixelPulseColumn(5,false);

    malta->SetPixelPulseRow(0,false);
    malta->SetPixelPulseRow(0,false);

    malta->SetPixelPulseRow(251,false);
    malta->SetPixelPulseRow(251,false);

    malta->SetPixelPulseRow(511,false);
    malta->SetPixelPulseRow(511,false);

    //current chip id is set from input args and must be preserved!
    uint32_t currentChipID = malta->GetCurrentChipID();
    std::cout << "Chip to read out: " << currentChipID << std::endl;

    for(auto id: malta->GetListOfChipIDs()){
      std::cout << "Set up mask for chip: " << id << std::endl;
      malta->SetSCIOToChip(id);
      for (int i =0; i < 256; ++i){
        malta->SetDoubleColumnMask( i,true);
        malta->SetDoubleColumnMask( i,true);
      }

      for (int i =0; i < 512; ++i){
        malta->SetPixelMaskColumn(i,true);
        malta->SetPixelMaskColumn(i,true);
        malta->SetPixelMaskDiag(i,true);
        malta->SetPixelMaskDiag(i,true);
        malta->SetPixelMaskRow(i,true);
        malta->SetPixelMaskRow(i,true);
      }
    }

    //make sure to talk to the chip that was specified in the input args
    malta->SetSCIOToChip(currentChipID);

    malta->SetConfigMode(false);


    if (firstconfiguration== true){
      std::string str;
      //std::vector<int>  config_value;
      //std::vector<string>  config_name;
      while (std::getline(fr, str)) {
        std::string col1, col3;
        int col2;
        std::istringstream ss(str);
        ss >> col1 >> col2;
        TString variable(col1);
        if (variable.BeginsWith("SC_")) {
          config_name.push_back(variable.ReplaceAll(":","").Data());
          config_value.push_back(col2);
        }
     }
     m_config_tree = new TTree("config", "config");
     for (unsigned int i =0 ; i < config_value.size(); ++i) {cout << config_name.at(i) << endl; m_config_tree->Branch(config_name.at(i).c_str(), &config_value.at(i), (config_name.at(i)+"/I").c_str());}
     m_config_tree->Fill();
  }
}

int eval_new_point(vector<int> *v_point_to_scan, vector<int> *v_counters){
   int next_point = 0;
   if (v_counters->size() ==0) next_point = v_point_to_scan->at(0);
   if (v_counters->size() ==1) {
      // CHECK THE PIXEL IS NOT DEAD, RE DO THE SCAN WITH ANOTHER INITIAL POINT
      if (v_counters->at(0) == 0) {
         ++rescan_first_point;
         next_point = v_point_to_scan->at(0)+rescan_first_point;
         v_counters->pop_back();
      }
      else next_point = v_point_to_scan->at(1);
   }
   if (v_counters->size() ==2) next_point = (int) (v_point_to_scan->at(0)+v_point_to_scan->at(1)) * 0.5 ;

   return next_point;
}

void eval_new_interval(vector<int> *v_point_to_scan, vector<int> *v_counters, int point, int counts){
   if (counts < target) {
      v_counters->at(1) = counts;
      v_point_to_scan->at(1) = point;
   }
   else {
      v_counters->at(0) = counts;
      v_point_to_scan->at(0) = point;

   }
   // PROTECTION IF YOU ARE below Y/X thresholds

   if ( abs(v_point_to_scan->at(0)-v_point_to_scan->at(1))<= m_thX) find_middle = true;
   if ( abs(v_counters->at(0)-v_counters->at(1))<= m_thY) find_middle = true;

   // PROTECTION IF YOU ARE above/below middle point on Y

   if ( (abs(v_counters->at(0)) > target ) && (abs(v_counters->at(1)) > target ) ) stop_scan = true;
   if ( (abs(v_counters->at(0)) < target ) && (abs(v_counters->at(1)) < target ) ) stop_scan = true;
}

int find_middle_element(std::vector<int> *v){

  int element = 0;
  double diff = 999999999999;
  for (unsigned int i =0 ; i < v->size(); ++i){
     if (abs(v->at(i)-target)< diff){
       element = i;
       diff = abs(v->at(i)-target);
     }
  }

  return element;
}


int main(int argc, char *argv[]){
  //time
  time_t start,end;
  time_t start_pix,end_pix;
  float Scantime_pix[512*512];

  //Switch on Power Supply and set current & voltage limits (once).
  // systemcall("SetVoltage_VPulse.py -f -s -v 1.0");

  cout << "#####################################" << endl
       << "# Welcome to MALTA Threshold Scan   #" << endl
       << "#####################################" << endl;

  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr     cAddress( 'a',"address","address","ipbus address. Default 192.168.0.1");
  CmdArgStr     cOutdir(  'o',"output","output","output directory");
  CmdArgStr     cPath(    'f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgInt     cParamMin('l',"paramMin","paramMin","Vlow value(5-127)",CmdArg::isREQ);
  CmdArgInt     cParamMax('h',"paramMax","paramMax","Vhigh value(5-127)",CmdArg::isREQ);
  CmdArgInt     cParamStp('s',"paramStp","paramStp","Step of Voltage scan(1-127)",CmdArg::isREQ);
  CmdArgIntList cRegion(  'r',"region","region","region of pixels [xmin(0-511) No.x ymin(0-61) No.y(<62)]");
  CmdArgInt     cTrigger( 't',"trigger","trigger","the Number of pulse)",CmdArg::isREQ);
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files");
  CmdArgBool    cDut( 'd',"dut","is DUT");
  CmdArgInt     cID(      'i', "id", "id", "ID is set with jumpers on board, range (0 - <number of chips -1>)");
  CmdArgInt     cNPix(    'n', "n", "n","only scan each n-th pixel in x- and y-direction");
  CmdArgBool    cFastScan( 'F',"fast scan","fast scan");
  CmdArgBool    cOnlyEvenPixel( 'e',"even pixel only","even pixel only");
  CmdArgStr     cConfig(  'c',"config","config","configuration file");
  CmdLine cmdl(*argv,&cVerbose,&cNPix,&cOutdir,&cDut,&cID,&cPath,&cAddress,&cRegion,&cParamMin,&cParamMax,&cParamStp,&cTrigger,&cQuiet,&cFastScan,&cConfig,&cOnlyEvenPixel,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  uint32_t chipID = cID;
  uint32_t nPix = cNPix;

  string address = (cAddress.flags() & CmdArg::GIVEN?cAddress:"udp://ep-ade-gw-05.cern.ch:50003");
  vector<uint32_t> cPixels;//List of Pixels odd->x even->y
  vector<uint32_t> selPixX,selPixY;

  if ( cRegion.flags() & CmdArg::GIVEN ) {
    for(int32_t i=cRegion[0]; i<(cRegion[0]+cRegion[1]); i++){
      for(int32_t j=cRegion[2]; j<(cRegion[2]+cRegion[3]); j++){
	cPixels.push_back(i);
	cPixels.push_back(j);
      }
    }
  } else {
    cout << endl << " speficy the region please " << endl << endl;
    exit(-1);
  }

  //for(uint32_t i=0; i<cPixels.count(); i++){
  for(uint32_t i=0; i<cPixels.size(); i++){
    std::cout << " val: " << cPixels[i] << std::endl;
    if(i%2==0){selPixX.push_back(cPixels[i]);}
    else      {selPixY.push_back(cPixels[i]);}
  }

  //// ANDREA maybe move this part in the .h, but there is not .h
  int maxY = (int) cTrigger*1.2; //TO BE FIXED be careful about the +20% due to some mismatch input and output
  target = (int)maxY*0.5;

  rescan_first_point = 0;

  vector<int> m_counters;
  vector<int> m_point_to_scan;

  //making output directory
  string ChipFolder;
  ChipFolder=cPath;
  string baseDir=getenv("MALTA_DATA_PATH");
  baseDir+="/Malta/Results_ThScan/"+ChipFolder+"/";
  string outdir = baseDir+(cOutdir.flags() & CmdArg::GIVEN?cOutdir:"Base");
  if (outdir!="") {
     systemcall("mkdir -p "+outdir);
     systemcall("mkdir -p "+outdir+"/hists");
  }

  bool noFile=( (cQuiet.flags() & CmdArg::GIVEN) ? cQuiet:false);
  bool fastScan=( (cFastScan.flags() & CmdArg::GIVEN) ? cFastScan:false);
  bool onlyEvenPixel=( (cOnlyEvenPixel.flags() & CmdArg::GIVEN) ? cOnlyEvenPixel:false);
  bool nthPixel = ((cNPix.flags() & CmdArg::GIVEN) ? cNPix : false);

  signal(SIGINT,handler);

  //loop param
  int paramMin=cParamMin;   //5
  int paramMax=cParamMax;   //5
  if (paramMax>=90) {
    cout << endl;
    cout << "WARNING maxVLOW should never exceed VHIGH value currently set at 90 ... pls lower your max VLOW and retry" << endl;
    cout << endl;
    exit(-1);
  }
  int paramStp=cParamStp;
  //new loop param
  m_point_to_scan.reserve(2);
  m_point_to_scan.push_back(cParamMin);
  m_point_to_scan.push_back(cParamMax);
  m_measured_points = new vector<int>;
  m_measured_triggers = new vector<int>;
  //Trigger param
  int trigger=cTrigger;

  //Connect to MALTA
  std::cout << "before creating malta" <<std::endl;
  MaltaBase * malta = new MaltaBase(address);
  malta->SetConfigModeSleep(50000);
  std::cout << "after creating malta" <<std::endl;
  malta->SetDUT(cDut);
  MaltaThresholdAnalysis * ana = new MaltaThresholdAnalysis((paramMax-paramMin)/paramStp,paramMin,paramMax,outdir);
  ana->setNPulse(trigger);
  std::cout << "after creating analysis" <<std::endl;
  //bool cConfig = true;
  malta->WriteConstDelays(4,1);
  malta->SetHalfColumns(true);
  malta->SetHalfRows(false);
  malta->SetExternalL1A(false);
  malta->SetReadoutDelay(80);//was35
  //cConfig=false;


  cout << "#####################################" << endl
       << "# Configure the fucking chip                #" << endl
       << "#####################################" << endl;


  if(cConfig.flags()&&CmdArg::GIVEN){
    configureChip (malta, string(cConfig), true);
  }

  cout << "#####################################" << endl
       << "# Start Mask loop                   #" << endl
       << "#####################################" << endl;

  malta->SetConfigMode(true,0.8);
  ///systemcall("MALTA_PSU.py -t T_ANAG_OFF.txt ");
  ///systemcall("MALTA_PSU.py -c setVoltage DVDD 0.8 ");
  //////////////////////////////////////////////////////////////malta->SetClock(true);
  //////////////////////////////////////////////////////////////usleep( (int)(mainSleep*1000000) );

  //Pixel loop
  time(&start);//all pixel scanning clock start

  int flug_Ysize=0;
  auto selPixY_min=min_element(selPixY.begin(),selPixY.end());
  auto selPixY_max=max_element(selPixY.begin(),selPixY.end());
  printf("%d %d %d\n",*selPixY_min,*selPixY_max,(*selPixY_max)-(*selPixY_min)+1);
  // if((selPixY_max-selPixY_min+1)>61)
  //   {flug_Ysize=1;}
  // if(flug_Ysize==1){exit(-1);}

  int N_pix=1;
  switch(N_pix){
  case 1:
    //VD+AS: bob set to full chip
    for(uint32_t pixX=0;pixX<512;pixX++){
      if(selPixX.size()>0){
	bool found=false;
	for(uint32_t i=0;i<selPixX.size();i++){
	  if(selPixX[i]==pixX){found=true;}
	}
	if(!found){continue;}
        if (onlyEvenPixel==true && (pixX % 2 != 0)) continue;
        if(nthPixel && pixX%nPix != 0) continue;
      }
      for(uint32_t pixY=0;pixY<512;pixY++){
	if(selPixY.size()>0){
	  bool found=false;
	  for(uint32_t i=0;i<selPixY.size();i++){
	    if(selPixY[i]==pixY){found=true;}
	  }
	  if(!found){continue;}
	  if (onlyEvenPixel==true && (pixY % 2 != 0)) continue;
    if(nthPixel && pixY%nPix != 0) continue;
	}

	if(!g_cont){break;}
	time(&start_pix);//1pixel scanning clock start
	if (rescan_first_point>0 && rescan_first_point_max){
           if(cConfig.flags()&&CmdArg::GIVEN){
	     cout << " trying to reconfigure the chip "  << endl;
             configureChip (malta, string(cConfig));
	   }
        }
	rescan_first_point = 0;

	//Lower voltage
	//systemcall("MALTA_PSU.py -c setVoltage DVDD 0.8 ");
//	systemcall("MALTA_PSU.py -t T_ANAG_OFF.txt ");
//	malta->SetClock(true);
//	sleep(1.0);
	//Unmask desired pixel
	cout << "Unmask the desired pixel: " << pixX << "," << pixY << endl;
	malta->SetPixelPulse(pixY,pixX,true);
	malta->SetPixelPulse(pixY,pixX,true);
	//////malta->SetPixelPulse(pixY+450,pixX,true);
	//malta->SetPixelMask(pixY,pixX,false);
	//malta->SetDoubleColumnMask(pixX/2-1,false);
	malta->SetDoubleColumnMask( (int)(pixX/2),false);
	malta->SetDoubleColumnMask( (int)(pixX/2),false);
	//malta->SetDoubleColumnMask(pixX/2+1,false);

	malta->SetPixelMaskRow(pixY,false);
	malta->SetPixelMaskRow(pixY,false);
	usleep(1000);
	malta->Sync();
	usleep(1000);
	//Parameter loop
	float paramVal=paramMin;

	cout << "==> Start parameter loop" << endl;
	stop_scan= false;
	find_middle= false;
	m_point_to_scan.at(0)= cParamMin;
	m_point_to_scan.at(1)= cParamMax;
        m_measured_points->clear();
        m_measured_triggers->clear();
	cout << "==> Start parameter loop" << endl;
	if (m_counters.size() > 0 ) m_counters.clear();
	//for(paramVal=paramMin; paramVal<=paramMax; paramVal+=paramStp){
	while (stop_scan==false && paramVal<=paramMax){

       	 if (fastScan && find_middle==false) paramVal = eval_new_point(&m_point_to_scan, &m_counters);
         else if  (fastScan && find_middle==true) {
	   stop_scan = true;
           std::vector<int>::iterator it = std::lower_bound(m_measured_points->begin(), m_measured_points->end(), m_measured_points->at(find_middle_element(m_measured_triggers)) +1 );
           if (it != m_measured_points->end())   continue;

           else paramVal = m_measured_points->at(find_middle_element(m_measured_triggers)) +1;
         }
         m_measured_points->push_back(paramVal);
	 cout << "Param: " << paramVal << " of " << paramMax << endl;
      	 if (rescan_first_point>=rescan_first_point_max){
      	   cout << "pixel dead, stopping the scan" << endl;
      	   break;
     	 }
	  ostringstream os;
	  ////systemcall("MALTA_PSU.py -t T_ANAG_OFF.txt ");
	  ////systemcall("MALTA_PSU.py -c setVoltage DVDD 0.8 ");
	  malta->SetConfigMode(true,0.8);
	  //usleep(100000);
	  //usleep(50000);
	  ///////////////////////////////////////malta->SetClock(true);
	  ///////////////////////////////////////usleep( (int)(mainSleep*1000000) );
	  //usleep(50000);
	  cout << "--------------- Setting vlow to: " << paramVal << " ---------------------" << endl;
	  malta->SetVPULSE_LOW(paramVal,true);
	  malta->SetVPULSE_LOW(paramVal,true);
	  ///////////////////////////////////////usleep( (int)(mainSleep*1000000) );
	  ///////////////////////////////////////malta->SetClock(false);
	  //systemcall("MALTA_PSU.py -c setVoltage DVDD 1.8 ");
	  //systemcall("MALTA_PSU.py -t T_ANAG_ON.txt ");
	  malta->SetConfigMode(false);
	  usleep(1000);

	  malta->Sync();
	  usleep(1000);
	  systemcall(os.str());
	  if(!g_cont){break;}

	  //Create ntuple
	  os.str("");
	  os << outdir  << "/thrscan_"
	     << setw(3) << setfill('0') << pixX << "_"
	     << setw(3) << setfill('0') << pixY << "_"
	     << setw(3) << setfill('0') << paramVal << ".root";
	  string fname = os.str();
	  cout << fname << endl;
	  MaltaTree *ntuple = new MaltaTree();
	  if (!noFile) ntuple->Open(fname,"RECREATE");
	  std::cout << "*************"+fname<<std::endl;

	  //Flush MALTA
	  malta->ReadoutOff();
	  malta->ResetFifo();
	  malta->ResetL1Counter();

	  //////////////////////////////////////////////////////////////////malta->SetPixelPulse(pixY,pixX,false); ????????????????

	  uint32_t event=0;
	  usleep( (int)(mainSleep*1000000) );
	  malta->ReadoutOn();
	  malta->Trigger(trigger,true);
	  malta->ReadoutOff();
	  //usleep(5000000);

	  MaltaData md,md2;
	  uint32_t words[2];
	  bool isFirst = true;

	  //chrono::time_point<chrono::system_clock> t0 = chrono::system_clock::now();

	  int numberOfHitsInMyPixel=0;
          int numberOfHits=0;
	  cout << "MOTHERFUCKER" << endl;
          int32_t nChips = malta->GetListOfChipIDs().size();
	  while(g_cont){

	    //if (event%500==0 and event!=0) cout << event << endl;

	    /*
	      chrono::time_point<chrono::system_clock> t1 = chrono::system_clock::now();
	      if(chrono::duration_cast<chrono::milliseconds>(t1-t0).count()>1000){break;}
	    */

	    if(event>80000){break;}

	    //uint32_t pulsesSeen=0;
	    bool markDuplicate=false;

	    /*
	      malta->ReadFifoStatus();
	      bool fifo2Empty = malta->IsFifo2Empty();
	      if (fifo2Empty){
	      usleep(10000);
	      break; //since now the acuisition is shut!
	      }
	    */

	    malta->ReadMaltaWord(&words[0]);

	    if (words[0]==0) break; //VD

	    if (words[0]==0 && words[1]==0) { continue;}

            if(isFirst){
              md.setWord1(words[0]);
              md.setWord2(words[1]);
              //check if word comes from the chosen chip, otherwise ignore
              md.unpack();
              int32_t newChipID = md.getChipid();
              if(nChips == 2){
                newChipID = md.getDualChipid();
              } else if(nChips == 4){
                newChipID = md.getQuadChipid();
              }
              //std::cout << "New chip id: " << newChipID << " vs chipid: " << chipID << std::endl;
              if(newChipID == chipID){
                isFirst = false;
                continue;
              } else {
                continue;
              }
            }

            //only read in words from the chosen chip
            if(  nChips == 1 ||
                (nChips == 2 && chipID == md.getDualChipid()) ||
                (nChips == 4 && chipID == md.getQuadChipid())
            ){
              md2.setWord1(md.getWord1());
              md2.setWord2(md.getWord2());
              md.setWord1(words[0]);
              md.setWord2(words[1]);
            } else {
              continue;
            }

	    md.unPack();
	    md2.unPack();
	    //std::cout<< md2.getInfo()<<std::endl;

	    //from MD
	    for (unsigned h=0; h<md.getNhits(); h++) {
	      uint32_t LpixX=md.getHitColumn(h);
	      uint32_t LpixY=md.getHitRow(h);
	      //cout << "      hit: " << h << " (" << LpixX << "," << LpixY << ")" << endl;
              numberOfHits++;
              cout << LpixX << " / " << LpixY << endl;
	      if (LpixX==pixX and LpixY==pixY) numberOfHitsInMyPixel++;
	    }

	    if (!noFile) ntuple->Set(&md2);

	    uint32_t phase1 = md2.getPhase();
	    uint32_t winid1 = md2.getWinid();
	    uint32_t bcid1 = md2.getBcid();
	    uint32_t l1id1 = md2.getL1id();

	    uint32_t phase2 = md.getPhase();
	    uint32_t winid2 = md.getWinid();
	    uint32_t bcid2 = md.getBcid();
      uint32_t l1id2 = md.getL1id();
	    bool markDuplicateNext = false;
	    bool isRealDuplicate   = false;


	    isRealDuplicate = markDuplicateNext;
	      // REWRITING DUPLICATION REOMVAL HERE
	    if( (bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 && bcid1==63)){
	      if( (winid2-winid1)==1 || (winid2==0 && winid1==7 )){
                if( phase2==0 && (l1id1==l1id2)){
	          if( phase1>=5) markDuplicate = true;

	          else if(phase1>3){
	            markDuplicateNext = true;
	            markDuplicate = false;
	          }
	          else markDuplicate = false;

                }
                else  markDuplicate = false;

              }
              else markDuplicate = false;

	    }
	    else  markDuplicate = false;

	    if( isRealDuplicate==true){
              markDuplicate = 1;
              markDuplicateNext=false;
            }
            else{
              if(markDuplicate==true)  markDuplicate = 1;
              else markDuplicate = 0;
            }


          //if(md.getDcolumn()==2 && (md.getGroup()==31 || md.getGroup()==15)){ pulsesSeen+=1; }

          //if( phase1 > 4 and phase2 == 0){
          //  if (abs(winid2-winid1 == 1) or (winid2 == 0 and (winid1 == 7))){markDuplicate = true;}
          //  else{markDuplicate = false;}
          //}else{markDuplicate = false;}
          //ntuple->SetIsDuplicate(markDuplicate);


	    if (!noFile) ntuple->SetIsDuplicate(markDuplicate);
	    if (!noFile) ntuple->Fill();
	    event+=1;
	    ana->process(&md2,pixX,pixY,paramVal,markDuplicate);
	  }

        //End DAQ loop

        //End Trigger lopp
	 cout << "Number of events: " << event << " --> in the pixel I want to pulse: " << numberOfHitsInMyPixel << endl;
         cout << "Total number of hits: " << numberOfHits<< endl;
     	 if (fastScan){
           if (m_counters.size()<2) m_counters.push_back(numberOfHitsInMyPixel);
           if (m_counters.size()==2) eval_new_interval(&m_point_to_scan, &m_counters, paramVal, numberOfHitsInMyPixel); //check difference numberOfHitsInMyPixel/numberOfHits
	 }
	 if (numberOfHits ==0 ) ana->process(&md2,pixX,pixY,paramVal,0);
         m_measured_triggers->push_back(numberOfHitsInMyPixel);
	 if (!noFile) ntuple->Close();
	 if (!fastScan)  paramVal+=paramStp;
	  //This line is needed to avoid filling the memory of the computer. Carlos
	 delete ntuple;

	} //end of VPULSE_L loop


	//End param loop
	//systemcall("MALTA_PSU.py -t T_ANAG_OFF.txt ");
	//systemcall("MALTA_PSU.py -c setVoltage DVDD 0.8 ");
	malta->SetConfigMode(true,0.8);
	///////////////////////////////////////////////////malta->SetClock(true);
	///////////////////////////////////////////////////usleep( (int)(mainSleep*1000000) );
	//usleep(100000);
	malta->SetPixelPulse(pixY    ,pixX,false);
	malta->SetPixelPulse(pixY    ,pixX,false);
	malta->SetDoubleColumnMask( (int)(pixX/2),true);
	malta->SetDoubleColumnMask( (int)(pixX/2),true);
	malta->SetPixelMaskRow(pixY,true);
	malta->SetPixelMaskRow(pixY,true);

	// getting time information
	time(&end_pix);
	////ana->end(Scantime); //VD: we need to discuss about it
	Scantime_pix[pixX*512+pixY]=end_pix-start_pix;
	cout << "!!!!!!!one pixel scantime" << Scantime_pix[pixX*512+pixY] <<endl;
      }
    }
    break;

  default:
    break;
  }
  ////////////////////////////////////////////////////////
  m_config_tree->Scan();
  time(&end);
  float Scantime=end-start;
  cout << "TOTAL scantime: " << Scantime << endl;
  ana->end(0.,Scantime_pix, m_config_tree);

  malta->SetClock(false);
  //systemcall("MALTA_PSU.py -c setVoltage DVDD 1.8 ");
  //systemcall("MALTA_PSU.py -t T_ANAG_ON.txt ");
  malta->SetConfigMode(false);

  //  printf("Scanning time is %ld\n", Scantime);
  delete m_config_tree;
  delete malta;
  delete ana;
  cout << ("python runThAnalysis.py -f "+outdir+" -e True").c_str() << endl;
  if (onlyEvenPixel) systemcall(("python runThAnalysis.py -f "+outdir+" -e True").c_str());
  else systemcall(("python runThAnalysis.py -f "+outdir+" -e True").c_str());

  cout << "Have a nice day" << endl;
  return 0;
}
