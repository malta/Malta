#include "Malta/MaltaModule.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>

#include "TH2D.h"

using namespace std;

DECLARE_READOUTMODULE(MaltaModule)

MaltaModule::MaltaModule(string name, string address, string outdir):
  ReadoutModule(name,address,outdir){
  std::cout << "MaltaModule created." << std::endl;
  m_modulesize = 1;
  m_malta = new MaltaBase(address);
  m_ntuple = 0;
  ostringstream os;
  os << m_name << "_timing";
  m_Htime=new TH1D(os.str().c_str(),"timing;time since L1A [ns]",160,0,500);
  os.str("");
  os << m_name << "_nPixel";
  m_Hsize=new TH1D(os.str().c_str(),"pixel;# pixel per event",6,-0.5,5.5);
  os.str("");
  os << m_name << "_hitMap";
  ostringstream ost;
  ost << name << ";pix X; pix Y";
  m_Hmap =new TH2D(os.str().c_str(),ost.str().c_str(),512,0,512,512,0,512);
  m_internalTrigger=false;

  ostringstream os_swapped, ost_swapped;
  os_swapped << os.str() << "_swapped";
  ost_swapped << ost.str() << " swapped";
  m_Hmap_swapped = new TH2D(os_swapped.str().c_str(), ost_swapped.str().c_str(), 512, 0, 512, 512, 0, 512);


  ostringstream os_symmetric, ost_symmetric;
  os_symmetric << os.str() << "_symmetric";
  ost_symmetric << ost.str() << " symmetric";
  m_Hmap_symmetric = new TH2D(os_symmetric.str().c_str(), ost_symmetric.str().c_str(), 512, -512, 0, 512, 0, 512);
  
  ostringstream osp;
  osp << m_name << "_pX";
  m_pX=new TH1F(osp.str().c_str(),";hit posX [mm]",50,-256*0.0368,+256*0.0368);
  ostringstream osp2;
  osp2 << m_name << "_pY";
  m_pY=new TH1F(osp2.str().c_str(),";hit posY [mm]",50,-256*0.0368,+256*0.0368);
}

MaltaModule::~MaltaModule(){
  delete m_malta;
  if(m_ntuple) delete m_ntuple;
}

void MaltaModule::Configure(){
  m_modulesize=GetConfig()->GetModuleSize();
  if(m_modulesize == 1){
    m_malta->SetVerbose(m_verbose);
    m_malta->SetTimeout(3000);
    ReadoutConfig * config=GetConfig();
    float configVoltage=0.9;
    if(config){
      m_malta->SetDUT(config->IsDUT());
      configVoltage=config->ConfigVoltage(configVoltage);
    }

    //if (!config->IsDUT()) {
    m_malta->Reset();
    m_malta->SetConfigMode(true,configVoltage);
    m_malta->SetClock(true);
    //m_malta->Reset(); //WHY BEFORE!!!!
    m_malta->ResetFifo();

    //Default for all MALTA versions
    m_malta->EnableMerger(false,true);//false
    //m_malta->SetPulseWidth(750,true); //500 from 11-03-2020
    //cout << "before pulse" << endl;
    //Default config for MALTA_C
    m_malta->EnableMonDacCurrent(false,true);//VD we should really put False 11-03-2020
    m_malta->EnableMonDacVoltage(false,true); //VD we should really put False 11-03-2020
    m_malta->EnableIDB(false,true);
    m_malta->EnableITHR(false,true);
    m_malta->EnableIRESET(false,true);
    m_malta->EnableICASN(false,true);
    m_malta->EnableVCASN(false,true);
    m_malta->EnableVCLIP(false,true);
    m_malta->EnableVPULSE_HIGH(false,true);
    m_malta->EnableVPULSE_LOW(false,true);
    m_malta->EnableVRESET_P(false,true);
    m_malta->EnableVRESET_D(false,true);

    m_malta->Send();
    //cout <<"AFTER MY SEND!!!!" << endl;
    bool tap_from_file = false;
    if(config){
      for(uint32_t i=0;i<config->GetKeys().size();i++){
        string key=config->GetKeys().at(i);
        cout << key << endl;
        if (key.find("TAP")!= string::npos) tap_from_file = true;
        m_malta->SetConfigFromString(key);
      }
    }
    m_malta->SetPulseWidth(500,true); //VD from 09-08-2021
    m_malta->Send();
    usleep(1000);
    m_malta->SetClock(false);
    m_malta->SetConfigMode(false);
    //}
    if ((tap_from_file) == false) {m_malta->WriteConstDelays(4,1);}
    //m_malta->WriteConstDelays(3,1);
    m_malta->ResetFifo();
    m_malta->SetHalfColumns(false);//ReadoutHalfColumnsOff();
    m_malta->SetHalfRows(false);//ReadoutHalfRowsOff();
    m_malta->ReadoutEnableExternalL1A();
    m_malta->ResetL1Counter();
    m_malta->SetTimeout (100);
    m_malta->ReadoutOn();
    /////m_malta->SetTimeout (100);
    cout << "=================================================================================" << endl;
  } else if(m_modulesize == 2 || m_modulesize == 4){

    //create the hitmap again with the right bins
    string name=m_Hmap->GetName();
    string name_swapped =m_Hmap_swapped ->GetName();
    string name_symmetric =m_Hmap_symmetric ->GetName();

    string title=m_Hmap->GetTitle();
    string title_swapped =m_Hmap_swapped ->GetTitle();
    string title_symmetric =m_Hmap_symmetric ->GetTitle();
    delete m_Hmap;    
    delete m_Hmap_swapped ;
    delete m_Hmap_symmetric ;    
    m_Hmap=new TH2D(name.c_str(),title.c_str(),m_modulesize*512,0,m_modulesize*512,512,0,512);
    m_Hmap_swapped = new TH2D(name_swapped.c_str(), title_swapped.c_str(),m_modulesize*512,0,m_modulesize*512,512,0,512);
    //    m_Hmap_symmetric = new TH2D(title_swapped.c_str(), title_symmetric.c_str(),m_modulesize*512,0,m_modulesize*512,512,0,512);
    m_Hmap_symmetric = new TH2D(title_swapped.c_str(), title_symmetric.c_str(),m_modulesize*512,-m_modulesize*512,0,512,0,512);

    m_malta->SetVerbose(m_verbose);
    m_malta->SetTimeout(3000);
    ReadoutConfig * config=GetConfig();
    float configVoltage=0.9;
    if(config){
      m_malta->SetDUT(config->IsDUT());
      configVoltage=config->ConfigVoltage(configVoltage);
    }

    //if (!config->IsDUT()) {
    m_malta->Reset();
    m_malta->SetConfigMode(true,configVoltage);
    m_malta->SetClock(true);
    //m_malta->Reset(); //WHY BEFORE!!!!
    m_malta->ResetFifo();

    //Default for all MALTA versions
    m_malta->EnableMerger(false,true);//false
    //m_malta->SetPulseWidth(750,true); //500 from 11-03-2020
    //cout << "before pulse" << endl;
    //Default config for MALTA_C
    m_malta->EnableMonDacCurrent(false,true);//VD we should really put False 11-03-2020
    m_malta->EnableMonDacVoltage(false,true); //VD we should really put False 11-03-2020
    m_malta->EnableIDB(false,true);
    m_malta->EnableITHR(false,true);
    m_malta->EnableIRESET(false,true);
    m_malta->EnableICASN(false,true);
    m_malta->EnableVCASN(false,true);
    m_malta->EnableVCLIP(false,true);
    m_malta->EnableVPULSE_HIGH(false,true);
    m_malta->EnableVPULSE_LOW(false,true);
    m_malta->EnableVRESET_P(false,true);
    m_malta->EnableVRESET_D(false,true);

    m_malta->Send();
    //cout <<"AFTER MY SEND!!!!" << endl;
    bool tap_from_file = false;
    if(config){
      for(uint32_t i=0;i<config->GetKeys().size();i++){
        string key=config->GetKeys().at(i);
        cout << key << endl;
        if (key.find("TAP")!= string::npos) tap_from_file = true;
        m_malta->SetConfigFromString(key);
      }
    }
    m_malta->SetPulseWidth(500,true); //VD from 09-08-2021
    m_malta->Send();
    usleep(1000);
    m_malta->SetClock(false);
    m_malta->SetConfigMode(false);
    //}
    if ((tap_from_file) == false) {m_malta->WriteConstDelays(4,1);}
    //m_malta->WriteConstDelays(3,1);
    m_malta->ResetFifo();
    m_malta->SetHalfColumns(false);//ReadoutHalfColumnsOff();
    m_malta->SetHalfRows(false);//ReadoutHalfRowsOff();
    m_malta->ReadoutEnableExternalL1A();
    m_malta->ResetL1Counter();
    m_malta->SetTimeout (100);
    m_malta->ReadoutOn();
    /////m_malta->SetTimeout (100);
    cout << "=================================================================================" << endl;
  }
}

void MaltaModule::Start(string run, bool usePath){
  string fname;
  cout << m_name << ": Start run " << run << endl;
  if(!usePath){
    ostringstream os;
    os << m_outdir << "/" << "run_" << run << ".root";
    fname = os.str();
  }
  else{
    fname = run;
  }

  //Create ntuple
  cout << m_name << ": Create ntuple" << endl;
  if(m_ntuple) delete m_ntuple;
  m_ntuple = new MaltaTree();
  m_ntuple->Open(fname,"RECREATE");
  cout << m_name << ": Prepare for run"<< endl;
  m_malta->ReadoutOff();
  m_malta->ResetL1Counter();
  m_malta->ResetFifo();
  m_malta->SetReadoutDelay(80);//-32);//was80
  //m_malta->SetMaxROWindow(100);
  m_malta->ReadoutOn();

  cout << m_name << ": Create data acquisition thread" << endl;
  m_thread = thread(&MaltaModule::Run,this);
  m_running = true;
  cout << m_name << ": Data acquisition thread running" << endl;


  //write config Tree to ntuple
  std::map<std::string,std::string> configFromModule = GetConfig()->ConfigForROOTFile();
  std::map<std::string,std::array<char,2000>> configForROOTFile;
  m_ntuple->GetFile()->cd();

  //loop through config entries and make branch for each one, then set values for the branch
  //fill tree after loop, every branch should only have one entry
  //ignore masks because this will be written to separate histograms for pixels and double columns
  //cout << "Write run info to file" << endl;
  TTree* info = new TTree("INFO","Info about run parameters");
  for(auto entry: configFromModule){
    if(entry.second.size() < 1999){
      info->Branch(entry.first.c_str(),&configForROOTFile[entry.first],(entry.first+"/C").c_str());
      strcpy(configForROOTFile[entry.first].data(), entry.second.c_str());
    } else {
      cout << "Char array too large: " << endl
           << entry.second << endl
           << endl << "Entry will be ignored!" << endl;
      continue;
    }
  }
  info->Fill();
  info->Write();

  TH1D* dcMaskHist = m_malta->GetDColMask(); dcMaskHist->SetDirectory(m_ntuple->GetFile()->GetDirectory(""));
  TH2I* pixelMaskHist = m_malta->GetPixelMask();  pixelMaskHist->SetDirectory(m_ntuple->GetFile()->GetDirectory(""));
  TH2I* maskedPixelHist = m_malta->GetMaskedPixels();  maskedPixelHist->SetDirectory(m_ntuple->GetFile()->GetDirectory(""));

  m_ntuple->GetFile()->Write();
  //m_malta->ReadoutOn();
}

void MaltaModule::Stop(){
  cout << m_name << ": Stop" << endl;
  m_cont=false;
  m_thread.join();
  cout << m_name << ": Join data acquisition thread" << endl;
  DisableFastSignal();        //to be removed from here
  m_malta->ReadoutOff();
  m_malta->ResetL1Counter();
  //m_thread.join();
  m_running=false;
  //m_malta->ResetFifo();
  cout << m_name << ": Close tree" << endl;
  m_ntuple->GetFile()->WriteObject(m_Htime,"Timing");
  m_ntuple->GetFile()->WriteObject(m_Hsize,"NHit"  );
  m_ntuple->GetFile()->WriteObject(m_Hmap ,"HitMap");
  m_ntuple->GetFile()->WriteObject(m_Hmap_swapped,"HitMap_swapped");
  m_ntuple->GetFile()->WriteObject(m_Hmap_symmetric,"HitMap_symmetric");
  m_ntuple->Close();
}

void MaltaModule::Run(){

  //300 DOES NOT WORK!!! //280 DOES NOT WORK
  uint32_t words[250]; //was 250
  uint32_t numwords=250;
  uint32_t numwordsCurrent=0;
  for (uint32_t i=0;i<numwords;i+=2) {
    words[i+0]=0;
    words[i+1]=0;
  }
  MaltaData md,md2;


  bool isFirstTEST=true;
  bool isFirst=true;
  uint32_t phase1, phase2, winid1, winid2, l1id1, chipid1, l1id2, bcid1, bcid2;
  uint32_t lastL1id=0;
  //uint32_t extL1id=0;
  m_extL1id=0;
  uint32_t prevL1id=123456;
  uint32_t lastCount=0;
  uint32_t overflow=0;
  bool markDuplicate;
  uint32_t coutCounter=0;

  uint32_t C_TOT =0;
  uint32_t C_WOW =0;
  uint32_t C_PROB=0;
  uint32_t C_REF0=0;
  uint32_t C_JUMP=0;

  uint32_t readAttempt=0;
  bool print=false;
  int nHit=0;

  m_cont=true;

  int countV=0;
  std::vector<std::pair<uint32_t,uint32_t>> event;

  while(m_cont){
    //cout << m_name << " bMefore" << endl;
    if (m_resetL1Counters) {
      m_malta->ReadoutOff();
      usleep(1000);
      m_malta->ResetL1Counter();
      //m_malta->ResetFifo();
      usleep(1000);
      m_malta->ReadoutOn();
      //prevL1id=4096;
      m_resetL1Counters=false;
      cout<<"reset L1 counters from socket command" <<endl;
    }
    //m_malta->Sync(); //moved lower ...
    //m_malta->ReadFifoStatus();
    numwordsCurrent = m_malta->GetFIFO2WordCount();
    m_malta->ReadFifoStatus();
    //if (numwordsCurrent>10000) m_malta->ReadFifoStatus();
    if(numwordsCurrent >= 2) numwordsCurrent -= 2;
    //cout << "FIFO2 word count: " << numwordsCurrent << endl;
    if(numwordsCurrent > numwords) numwordsCurrent = numwords;
    //cout << " L1ID from FPGA : " << m_malta->GetL1ID() << endl;
    //cout << m_name << " after" << endl;
    bool fifo2Empty = m_malta->IsFifo2Empty() or (numwordsCurrent<2);
    bool fifo1Full  = m_malta->IsFifo1Full();
    bool fifo2Full  = m_malta->IsFifo2Full();

    if (fifo2Empty || (numwordsCurrent<4) ){
      coutCounter++;
      if (coutCounter%20==0 && false) {
        cout << m_name << ": Number of events since last EmptyFifo (" << coutCounter << "): " << lastCount
             << "   and last L1id is: " << lastL1id
             << " (" << overflow << ") --> cumulative: " << m_extL1id
             << endl;
      }

      lastCount=0;
      std::this_thread::sleep_for(0.01s);
      m_malta->Sync();
      for (uint32_t i=0;i<numwords;i+=2) {
        words[i+0]=0;
        words[i+1]=0;
      }

      /*
      if ( m_maxEvt!=-1) {
        if ( m_extL1id>m_maxEvt or C_TOT>m_maxEvt ) {
          m_cont=false;
	  m_running=false;
        }
      }
      */

      //generate some random L1a (note that the IPBus can't generate L1A which are short enough)
      if (m_internalTrigger) {
        //m_malta->Trigger(0,false);
      }
      continue;
    }
    if(fifo1Full){
      cout << m_name << " :: FIFO1 FULL : AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << endl;
    }
    if(fifo2Full){
      cout << m_name << " :: FIFO2 FULL : OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO" << endl;
    }

    if(m_debug) cout << m_name << " Before READ m_cont=" << m_cont << endl;

    m_malta->ReadMaltaWord(words,numwordsCurrent);
    countV++;

    if(m_debug) cout << m_name << " After READ m_cont=" << m_cont << endl;

    // this is maybe outdated atm....
    uint32_t goodW =0;
    int NoRef =0;
    int emptyW=0;
    int badW  =0;
    for (uint32_t i=0;i<numwordsCurrent;i+=2) {
      if ( words[i+0]!=0 and words[i+1]!=0 )   {
        if ( words[i]%2!=0) goodW++;
        else                NoRef++;
      }
      else  if ( (words[i+0]+words[i+1])==0 )  emptyW++;
      else                                     badW++;
    }
    if (goodW!=(numwordsCurrent/2) ) cout << m_name << " :: PAIRS summary: " << goodW << " / " << NoRef << "/ " << emptyW << " / " << badW << " / " << numwordsCurrent << endl;
    readAttempt++;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if(m_debug) cout << m_name << " Before processing m_cont=" << m_cont << endl;

    for(uint32_t i=0;i<numwordsCurrent;i+=2){

      if( m_maxEvt>0 ) {
        //if( (int)m_extL1id>m_maxEvt) Stop();
      }

      if(lastCount%1000000==0 && lastCount>0){
        cout << m_name << " :: Events so far: " << lastCount << endl;
      }

      m_extL1id = (lastL1id+overflow*4096);

      if(words[i+0]==0 or words[i+1]==0){
        cout << "FUCK ME ONE OF THE WORDS IS EMPTY: " << words[i+0] << " / " << words[i+1] << endl;
        if ( (words[i+0]+words[i+1])!=0 ) {
          if ( words[i+0]==0 ) print=true;
          if ( words[i+1]==0 ) print=false;
          cout << m_name << " :: WOW " << i/2 << " (" << readAttempt << ") :: " << words[i+0] << " , " << words[i+1] << endl;
          C_WOW+=1;
        } else {
          if (print) cout << m_name << " :: T: " << i/2 << " (" << readAttempt << ") :: EMPTY " << endl;
        }
        continue;
      } else {
        if (print) cout << m_name << " :: T: " << i/2 << " (" << readAttempt << ") :: " << words[i+0] << " , " << words[i+1] << endl;
      }
      C_TOT++;

      if (words[i+0]%2==0) {
	cout << "EMPTY REF BIT!" << endl;
        C_REF0++;
        continue;
      }

      lastCount++;
      if(isFirst){
        md.setWord1(words[i+0]);
        md.setWord2(words[i+1]);
        isFirst = false;
        continue;
      }

      md2.setWord1(md.getWord1());
      md2.setWord2(md.getWord2());
      md.setWord1(words[i+0]);
      md.setWord2(words[i+1]);

      md.unpack();
      md2.unpack();
      //cout << "         FOUND word with L1id: " << md.getL1id() << endl;

      m_ntuple->Set(&md2, m_modulesize);
      if (isFirstTEST) {
        isFirstTEST=false;
        m_extL1id=md2.getL1id();
      }

      //if(md.getPixel() == 0){
      //  cout << "Zero hit word: " << std::bitset<32>(words[i+0]) << " | " << std::bitset<32>(words[i+1]) << endl;
      //  cout << "Pixel block: " << std::bitset<16>(md.getPixel()) << endl;
      //}
      m_ntuple->SetL1idC(m_extL1id);

      bcid1  = md2.getBcid();
      phase1 = md2.getPhase();
      winid1 = md2.getWinid();
      l1id1  = md2.getL1id();
      if(m_modulesize == 2){
        chipid1 = md2.getDualChipid();
      } else if(m_modulesize == 4){
        chipid1 = md2.getQuadChipid(); 
      } else {
        chipid1 = md2.getChipid();
      }

      //std::cout << "MALTAWORD CHIPID: " << chipid1 << std::endl;

      bcid2  = md.getBcid();
      phase2 = md.getPhase();
      winid2 = md.getWinid();
      l1id2  = md.getL1id();

     // if (md.getPixel()==0) cout << m_name  << "########################### Pixels of event " << l1id2 << ": " << std::bitset<32>(md.getPixel()) << " " << md.getGroup() << " " << md.getDcolumn() << endl;

      //if(abs((long int)l1id1-l1id2) > 1) cout << "########################### L1ID jump from " << l1id1 << " to " << l1id2 << endl;

      int del=md.getDelay();
      int cID=1;
      if(m_modulesize == 2){
        cID=md.getDualChipid();
      } else if(m_modulesize == 4){
        cID=md.getQuadChipid();
      } else {
        cID=md.getChipid();
      }
     
      //treat del and cID differently in multi-chip modules
      if(m_modulesize == 2){
        if (bcid1>40 or (bcid1==0 and l1id1==0) ) C_PROB++;
      } else if(m_modulesize == 4){
        if (bcid1>40 or (bcid1==0 and l1id1==0) ) C_PROB++;
      } else {
        if (del!=0 or cID!=0 or bcid1>40 or (bcid1==0 and l1id1==0) ) C_PROB++;
      }
      

      bool isBad=false;
      lastL1id = l1id2;
      if (lastL1id!=prevL1id){
	if (prevL1id!=123456) {
	  //cout << "   previous L1id: " << prevL1id << " has: " << nHit << " hists" << endl;
          if(nHit == 0 && false){
            cout << "########################### Zero hit event: " << endl;
            for(auto maltaword: event) cout << std::bitset<32>(maltaword.first) << " | " << std::bitset<32>(maltaword.second) << endl;
            cout << endl << endl;
            //event.clear();
            //m_cont = false;
	    //Stop();
          }
	  m_Hsize->Fill(nHit);
	  if ( lastL1id!=(prevL1id+1) ) {
	    //cout << " I am missing smoe L1ID: " << lastL1id << " != " << (prevL1id+1) << endl;
	    if ( lastL1id>prevL1id ) {
	      //cout << " .... adding: " << (lastL1id-prevL1id)-1 << " empty events!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
	      for (uint32_t c=0; c< (lastL1id-prevL1id)-1; c++) {
		m_Hsize->Fill(0.);
	      }
	    } else if (prevL1id>4000) {
	      int newL1=lastL1id+4096;
	      //cout << " .... adding Special: " <<  (newL1-prevL1id)-1 << " empty events!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
	      for (uint32_t c=0; c< (newL1-prevL1id)-1; c++)  m_Hsize->Fill(0.);
	    }
	  }
	}
	nHit=0;
	event.clear();
        if (lastL1id<prevL1id and prevL1id!=123456){
          if (prevL1id>lastL1id and (prevL1id-lastL1id)<3000) {// VD : was 2000
            isBad=true;
            C_JUMP++;
	    cout << m_name << "VERY BAD L1A jumps!!!!" << endl;
          } else {
            cout << m_name << " :: I am going to increase LAST: " << lastL1id << "  prev: " << prevL1id << endl;
            //cout << m_name << " :: " << md2.toString()  << endl;
            //cout << m_name << " :: " << md.toString()  << endl;
          }
          if (not isBad) overflow+=1;
        }
        if (not isBad) prevL1id=lastL1id;
        else           lastL1id=prevL1id;

      }


      /////////////////////////////////////////////////////////////////////////////////
      //duplication calculation .....
      if((bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 and bcid1==63)){
        if ((winid2-winid1)==1 || (winid2==0 and winid1==7 )){
          if (phase2==0 && (l1id1==l1id2)){
            if (phase1>5){
              markDuplicate = true;
            }else if(phase1>3){
              markDuplicate = true;
            }else{
              markDuplicate = false;
            }
          }else{
            markDuplicate = false;
          }
        }else{
          markDuplicate = false;
        }
      }else{
        markDuplicate = false;
      }
      m_ntuple->SetIsDuplicate(markDuplicate);

      //if(markDuplicate) cout << "Found duplicate word: " << std::bitset<32>(words[i+0]) << " | " << std::bitset<32>(words[i+1]) << endl;

      m_ntuple->Fill();
      //if (not isBad) m_ntuple->Fill();

      if (m_pX->GetEntries()>20000) {
	m_pX->Reset();
	m_pY->Reset();
      }

      nHit+=md.getNhits();
      std::pair<uint32_t,uint32_t> mword; mword.first = words[i+0]; mword.second = words[i+1]; event.push_back(mword);
      if (not markDuplicate) {
	m_Htime->Fill(bcid2*25+winid2*3.125);
	for (unsigned int h=0; h<md2.getNhits(); ++h) {
	  m_pX->Fill( ((float)md2.getHitRow(h)-256.)   *0.0368);
	  m_pY->Fill( ((float)md2.getHitColumn(h)-256.)*0.0368);
          //chip ID is treated as xPix MSB and shifts x coordinate by 512
	  m_Hmap->Fill((chipid1+1)*md2.getHitColumn(h),md2.getHitRow(h));
	}
      }
    }

    //swapped
    for (int i = 1; i <= m_Hmap->GetNbinsX(); ++i) {
      for (int j = 1; j <= m_Hmap->GetNbinsY(); ++j) {
	m_Hmap_swapped->SetBinContent(j, i, m_Hmap->GetBinContent(i, j));
      }
    }

    //symmetric
    for (int i = 1; i <= m_Hmap_swapped->GetNbinsX(); ++i) {
      for (int j = 1; j <= m_Hmap_swapped->GetNbinsY(); ++j) {
	//m_Hmap_symmetric->SetBinContent(513 - i, j, m_Hmap_swapped->GetBinContent(i, j));
	m_Hmap_symmetric->SetBinContent(513 - i, j, m_Hmap_swapped->GetBinContent(i, j));
	//	cout<<"m_Hmap_symmetric = "<<"x = "<<i<<"\t y = "<<j<<"\t value = "<<m_Hmap_swapped->GetBinContent(i, j)<<endl;
      }
    }
    
    if(m_debug) cout << m_name << " After processing m_cont=" << m_cont << endl;

  }

  if(md.getWord1()!=0){
    m_ntuple->Set(&md);
    m_ntuple->SetIsDuplicate(false);
    m_ntuple->Fill();
  }
}


uint32_t MaltaModule::GetNTriggers(){ return  m_extL1id; }

void MaltaModule::EnableFastSignal(){
  m_malta->EnableFastSignal();
  //m_malta->Sync();
  m_malta->EnableFastSignal();
  m_malta->EnableFastSignal();
  m_malta->SetTimeout(100);
}

void MaltaModule::DisableFastSignal(){ m_malta->DisableFastSignal(); }

