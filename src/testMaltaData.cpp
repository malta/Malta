#include <iostream>
#include <ctime>
#include "Malta/MaltaData.h"

using namespace std;

int main() {
  
  MaltaData md;
 
  cout << "Test MaltaData reco" << endl;
  uint32_t word1=0x4FFFFFFF;
  uint32_t word2=0x0000FFFC;
  
  md.setWord1(word1);
  md.setWord2(word2);
  
  md.unpack();
  cout << md.toString() << endl;
  cout << md.getInfo() << endl;
  md.pack();

  cout << "word1: 0x" << hex << word1 << " = 0x" << md.getWord1() << endl
       << "word2: 0x" << hex << word2 << " = 0x" << md.getWord2() << endl
       << endl;

  cout << "Test MaltaData unpack" << endl;
  
  md.setPixel(17);
  md.setBcid(4);
  md.setGroup(2);
  md.setParity(1);
  md.setDcolumn(238);
  
  md.pack();

  cout << "Load Malta word: " << md.toString() << endl;

  md.unpack();
  cout << md.getInfo() << endl;

  clock_t start = clock();
  uint32_t N_TEST=1E8;

  for(uint32_t i=0;i<N_TEST; i++){

    md.unpack();
    
  }

  double duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  double frequency = (double) N_TEST / duration;

  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [Hz]: " << frequency << endl;
  cout << "Have a nice day" << endl;
  return 0;
}
