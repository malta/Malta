#include "Malta/MaltaBase.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <string>
#include <unistd.h>
#include <signal.h>

using namespace std;

bool g_cont;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){
  
  cout << "#####################################" << endl
       << "# Welcome to MALTA Register test    #" << endl
       << "#####################################" << endl;
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr     cAddress( 'a',"address","address","ipbus address",CmdArg::isREQ);
  CmdArgStr     cOutdir(  'o',"output","output","output directory");
  CmdArgInt     cPattern( 'p',"pattern","pattern","pattern to write");
  CmdArgStr     cConfig(  'c',"config","config","configuration file");

  CmdLine cmdl(*argv,&cVerbose,&cOutdir,&cAddress,&cConfig,&cPattern,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  
  cout << "Connect to MALTA: " << cAddress << endl;
  MaltaBase * malta = new MaltaBase(string(cAddress));

  if(cConfig.flags() && CmdArg::GIVEN){
    cout << "Configure MALTA" << endl;
    malta->SetConfigMode(true);
    malta->PreConfigMaltaC();
    malta->SetConfigFromFile(string(cConfig));
    malta->SetConfigMode(false);
  }

  g_cont=true;
  uint32_t w_pattern=(cPattern.flags() && CmdArg::GIVEN?cPattern:0x1FF);
  uint32_t r_pattern=0;
  uint32_t counter=0;

  cout << "Start loop (press ctrl+c to quit)" << endl;

  signal(SIGKILL, handler);
  signal(SIGINT, handler);

  while(g_cont){
    malta->SetConfigMode(true);
    for (uint32_t i=0;i<5;i++){
      if(!g_cont) break;
      malta->SetReservedRegister(i,w_pattern);
      usleep(200000);
      r_pattern=malta->GetReservedRegister(i);
      cout << "Register: " << i 
	   << hex
	   << " write: 0x" << w_pattern 
	   << " read: 0x" << r_pattern
	   << dec
	   << (w_pattern!=r_pattern?" DO NOT ":" ") << "MATCH"
	   << endl;
      if(w_pattern!=r_pattern)counter++;
    }
    malta->SetConfigMode(false);
    for (uint32_t i=0;i<10;i++){
      usleep(100000);
      if(!g_cont) break;
    }
    cout << endl;
  }

  cout << "Cleaning the house" << endl;
  delete malta;

  cout << "Have a nice day" << endl;
  return 0;
}
