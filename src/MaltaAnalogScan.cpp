//! -*-C++-*-
#include "Malta/MaltaData.h"
#include "Malta/MaltaBase.h"
#include "Malta/MaltaTree.h"
#include "TCanvas.h"
#include "TStyle.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>

using namespace std;
bool g_cont=true;
long int Scantime=1;//dummy
float Scantime_pix[512*512];//dummy
int rescan_first_point = 0;
int rescan_first_point_max = 3;
int target =0;
int m_thX = 1;
int m_thY= 10;
vector<int> *m_measured_points = NULL;
vector<int> *m_measured_triggers = NULL;
bool stop_scan = false;
bool find_middle = false;
float mainSleep=0.2;
TTree *m_config_tree;
std::vector<int>  config_value;
std::vector<string>  config_name;

int offset=80;//248;//504;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

void configureChip(MaltaBase * malta, string cConfig, bool firstconfiguration=false ){
  
  ifstream fr;
  fr.open(string(cConfig).c_str());
  if (!fr.is_open()){
    cout << "#####################################################"<<endl;
    cout <<      "##  The configuration file cannot be opened        ##" << endl;
    cout << "Stopping script"<<endl;
    exit(0);
  }
  malta->Reset();
  malta->SetConfigMode(true,0.8);
  usleep( 500000 );
  malta->PreConfigMaltaC();
  malta->SetConfigFromFile(string(cConfig));
  
  malta->SetVPULSE_HIGH(90);
  malta->SetVPULSE_HIGH(90);
  malta->SetVPULSE_LOW(10);
  malta->SetVPULSE_LOW(10);
  
  malta->SetPixelPulseColumn(5,false);
  malta->SetPixelPulseColumn(5,false);
  
  malta->SetPixelPulseRow(0,false);
  malta->SetPixelPulseRow(0,false);
  malta->SetPixelPulseRow(251,false);
  malta->SetPixelPulseRow(251,false);
  malta->SetPixelPulseRow(511,false);
  malta->SetPixelPulseRow(511,false);
  
  for (int i =0; i < 256; ++i){
    malta->SetDoubleColumnMask( i,true);
    malta->SetDoubleColumnMask( i,true);
  }
  for (int i =0; i < 512; ++i){
    malta->SetPixelMaskColumn(i,true);
    malta->SetPixelMaskColumn(i,true);
    malta->SetPixelMaskDiag(i,true);
    malta->SetPixelMaskDiag(i,true);
    malta->SetPixelMaskRow(i,true);
    malta->SetPixelMaskRow(i,true);
  }
    
  if (firstconfiguration== true){
    std::string str;
    while (std::getline(fr, str)) {
      std::string col1, col3;
      int col2;
      std::istringstream ss(str);
      ss >> col1 >> col2;
      TString variable(col1);
      if (variable.BeginsWith("SC_")) {
	config_name.push_back(variable.ReplaceAll(":","").Data());
	config_value.push_back(col2);
      }
    }
    m_config_tree = new TTree("config", "config");
    for (unsigned int i =0 ; i < config_value.size(); ++i) {cout << config_name.at(i) << endl; m_config_tree->Branch(config_name.at(i).c_str(), &config_value.at(i), (config_name.at(i)+"/I").c_str());}
    m_config_tree->Fill();
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]){
  //time
  time_t start,end;
  time_t start_pix,end_pix;
  
  cout << "#####################################" << endl
       << "# Welcome to MALTA Analog Scan   #" << endl
       << "#####################################" << endl;
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr     cAddress( 'a',"address","address","ipbus address. Default 192.168.0.1");
  CmdArgStr     cOutdir(  'o',"output","output","output directory");
  CmdArgStr     cPath(    'f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgInt     cParamMin('l',"paramMin","paramMin","Vlow value(5-127)");
  //CmdArgInt     cParamMax('h',"paramMax","paramMax","Vhigh value(5-127)",CmdArg::isREQ);
  CmdArgIntList cRegion(  'r',"region","region","region of pixels [xmin(0-511) No.x ymin(0-61) No.y(<62)]");
  CmdArgInt     cTrigger( 't',"trigger","trigger","the Number of pulse)",CmdArg::isREQ);
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files");
  CmdArgBool    cOnlyEvenPixel( 'e',"even pixel only in X","even pixel only in X");
  CmdArgBool    cDoubleYPixel( 'y',"pulsing the pixel with y=y+offset","double y pixels");
  CmdArgBool    cGroup( 'g',"pulsing in 2x4 blocks","pulsing in 2x4 blocks");
  CmdArgStr     cConfig(  'c',"config","config","configuration file");
  CmdArgStr     cTapFile(  'T',"tapfile","tapfile","tap calibration file");
  CmdArgBool    cDut( 'd',"dut","is DUT");
  CmdArgBool    cExternalTrigger( 'E',"external trigger","external trigger for pico delta time");

  CmdLine cmdl(*argv,&cVerbose,&cOutdir,&cDut,&cPath,&cAddress,&cRegion,&cParamMin,&cTrigger,&cQuiet,&cConfig,&cOnlyEvenPixel,&cDoubleYPixel,&cGroup,&cTapFile,&cExternalTrigger,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
 
  string address = (cAddress.flags() & CmdArg::GIVEN?cAddress:"udp://ep-ade-gw-05.cern.ch:50003");
  vector<uint32_t> cPixels;//List of Pixels odd->x even->y
  vector<uint32_t> selPixX,selPixY;
  
  bool onlyEvenPixel= ( (cOnlyEvenPixel.flags() & CmdArg::GIVEN) ? cOnlyEvenPixel:false);
  bool onlyGroup= ( (cGroup.flags() & CmdArg::GIVEN) ? cGroup:false);

  if ((onlyEvenPixel and onlyGroup) or (cDoubleYPixel and onlyGroup) ){
    cout<<"Stopping command, please do not use use -g at same time as-e and/or -y options"<<endl;
    return 0;
  }

  int nPix = cRegion[1]*cRegion[3]; 
  int estimatedScanTime = nPix;
  if (cDoubleYPixel) nPix*=2;
  if (onlyEvenPixel) estimatedScanTime = estimatedScanTime/2; 
  cout<<"The estimated scan time for ~"<< nPix <<" pixels  is: "<<estimatedScanTime<<" seconds"<<endl;
  
  for(int32_t i=cRegion[0]; i<(cRegion[0]+cRegion[1]); i++){
    for(int32_t j=cRegion[2]; j<(cRegion[2]+cRegion[3]); j++){
      cPixels.push_back(i);
      cPixels.push_back(j);
    }
  }
  
  //for(uint32_t i=0; i<cPixels.count(); i++){
  for(uint32_t i=0; i<cPixels.size(); i++){
   // std::cout << " val: " << cPixels[i] << std::endl;
    if(i%2==0){selPixX.push_back(cPixels[i]);}
    else      {selPixY.push_back(cPixels[i]);}
  }
  
  //// ANDREA maybe move this part in the .h, but there is not .h
  int maxY = (int) cTrigger*1.2; //TO BE FIXED be careful about the +20% due to some mismatch input and output
  target = (int)maxY*0.5;

  //making output directory
  string ChipFolder;
  ChipFolder=cPath;
  string baseDir=getenv("MALTA_DATA_PATH");
  baseDir+="/Malta/Results_AnalogScan/"+ChipFolder+"/";
  string outdir = baseDir+(cOutdir.flags() & CmdArg::GIVEN?cOutdir:"Base");
  if (outdir!="") {
     systemcall("mkdir -p "+outdir);
     systemcall("mkdir -p "+outdir+"/hists");
  }
  
  bool noFile=( (cQuiet.flags() & CmdArg::GIVEN) ? cQuiet:false);  
  bool tapfromfile = ( (cTapFile.flags() & CmdArg::GIVEN) ? true:false);
  bool externaltrigger = ( (cExternalTrigger.flags() & CmdArg::GIVEN) ? true:false); 
  signal(SIGINT,handler);
  
  //loop param
  int paramMin= 10;
  if ( (cParamMin.flags() & CmdArg::GIVEN)) paramMin = cParamMin;
  offset=paramMin;
  int paramMax=90;
  if (paramMax>=91) {
    cout << endl;
    cout << "WARNING maxVLOW should never exceed VHIGH value currently set at 90 ... pls lower your max VLOW and retry" << endl;
    cout << endl;
    exit(-1);
  }

  //Trigger param
  int trigger=cTrigger;
  
  //Connect to MALTA
  std::cout << "before creating malta" <<std::endl;
  MaltaBase * malta = new MaltaBase(address);
  std::cout << "after creating malta" <<std::endl;
  malta->SetDUT(cDut);

  if (tapfromfile) malta->ReadTapsFromFile(string (cTapFile));
  else             malta->WriteConstDelays(4,1);
 
  malta->SetHalfColumns(false);
  malta->SetHalfRows(false);

  malta->SetExternalL1A(externaltrigger);   //REMIND VALERIO THAT THIS SHOOULD BE PUT BACK ON!!!!
  					    //THIS IS TRUE ONLY FOR THE PULSING TIMING STUDIES WITH PICO
  malta->SetReadoutDelay(80);//was35
  
  std::string nTrigger = std::to_string(trigger);
  std::string charge  = "hight";//std::to_string(int(tempCharge));
  std::string title = "Number of hits, N="+nTrigger+", Q="+charge+" e;Pix X; Pix Y";
  TH2I* nHits_h2= new TH2I("Hits",title.c_str(), 512,-0.5,511.5,512,-0.5,511.5);
  TH2I* pPulsed_h2= new TH2I("pulsedPixels","Pulsed Pixels", 512,-0.5,511.5,512,-0.5,511.5);

  cout << "#####################################" << endl
       << "# Configure the fucking chip                #" << endl
       << "#####################################" << endl;
  

  if(cConfig.flags()&&CmdArg::GIVEN){
    configureChip (malta, string(cConfig), true); 
  }
 
  cout << "#####################################" << endl
       << "# Start Mask loop                   #" << endl
       << "#####################################" << endl;
  
  malta->SetConfigMode(true,0.8);
 
  time(&start);//all pixel scanning clock start
  //Pixel loop
  int flug_Ysize=0;
  auto selPixY_min=min_element(selPixY.begin(),selPixY.end());
  auto selPixY_max=max_element(selPixY.begin(),selPixY.end());
  printf("%d %d %d\n",*selPixY_min,*selPixY_max,(*selPixY_max)-(*selPixY_min)+1);
  if((selPixY_max-selPixY_min+1)>61)
    {flug_Ysize=1;}
  //if(flug_Ysize==1){exit(-1);}
  int checker{1};
  int pixels=0;
  int N_pix=1;
  switch(N_pix){
  case 1: 
    //VD+AS: bob set to full chip
    for(uint32_t pixX=0;pixX<512;pixX++){
      if(selPixX.size()>0){
	bool found=false;
	for(uint32_t i=0;i<selPixX.size();i++){
	  if(selPixX[i]==pixX){found=true;}
	}
	if(!found){continue;}
	if (onlyEvenPixel==true && (pixX % 2 != 0)) continue;
	if (onlyGroup==true) { 
	  if (pixX%2 == 0) checker = 1;
	  if (pixX%2 == 1) checker = -1;
	}
      }

      for(uint32_t pixY=0;pixY<512;pixY++){
	if(selPixY.size()>0){
	  bool found=false;
	  for(uint32_t i=0;i<selPixY.size();i++){
	    if(selPixY[i]==pixY){found=true;}
	  }
	  if(!found){continue;}
	  //if (onlyEvenPixel==true && (pixY % 2 != 0)) continue;
	  if (onlyGroup    ==true && (pixY % 4 != 0)) continue;
	}
	
	if(!g_cont){break;}
	pixels+=1;
	time(&start_pix);//1pixel scanning clock start
	if (rescan_first_point>0 && rescan_first_point_max){
	  if(cConfig.flags()&&CmdArg::GIVEN){
	    cout << " trying to reconfigure the chip "  << endl;
	    configureChip (malta, string(cConfig));
	  }
        }
	rescan_first_point = 0;

	//Unmask desired pixel
	cout << "Unmask the desired pixel: " << pixX << "," << pixY << endl;
	malta->SetPixelPulse(pixY,pixX,true);
	malta->SetPixelPulse(pixY,pixX,true);
	pPulsed_h2->Fill(pixX, pixY);
	if (onlyEvenPixel) {
	  malta->SetPixelPulse(pixY,pixX+1,true);
	  malta->SetPixelPulse(pixY,pixX+1,true);
	  pPulsed_h2->Fill(pixX+1, pixY);
	  //malta->SetPixelPulse(pixY+1,pixX, true);
	  //malta->SetPixelPulse(pixY+1,pixX, true);
	  //pPulsed_h2->Fill(pixX, pixY+1);
	  //malta->SetPixelPulse(pixY+1,pixX+1,true);
	  //malta->SetPixelPulse(pixY+1,pixX+1,true);
	  //pPulsed_h2->Fill(pixX+1, pixY+1);
	}
	if (cDoubleYPixel) {
	  malta->SetPixelPulse(pixY+offset, pixX, true);
	  malta->SetPixelPulse(pixY+offset, pixX, true);
	  pPulsed_h2->Fill((pixX+256)%512, pixY+offset);
	  /*
	  if (onlyEvenPixel) {
	    malta->SetPixelPulse(pixY+256,pixX+1,true);
	    malta->SetPixelPulse(pixY+256,pixX+1,true);
	    pPulsed_h2->Fill(pixX+1, pixY+256);
	    malta->SetPixelPulse(pixY+256+1,pixX,true);
	    malta->SetPixelPulse(pixY+256+1,pixX,true);
	    pPulsed_h2->Fill(pixX, pixY+256+1);
	    malta->SetPixelPulse(pixY+256+1,pixX+1,true);
	    malta->SetPixelPulse(pixY+256+1,pixX+1,true);
	    pPulsed_h2->Fill(pixX+1, pixY+256+1);
	  }
	  */
	}
	/*
	if (onlyGroup) {
	  malta->SetPixelPulse(pixY,pixX,true);
	  malta->SetPixelPulse(pixY,pixX,true);
	  pPulsed_h2->Fill(pixX, pixY);
	  malta->SetPixelPulse(pixY+2,pixX,true);
	  malta->SetPixelPulse(pixY+2,pixX,true);
	  pPulsed_h2->Fill(pixX, pixY+2);
	  malta->SetPixelPulse(pixY+4,pixX,true);
	  malta->SetPixelPulse(pixY+4,pixX,true);
	  pPulsed_h2->Fill(pixX, pixY+4);
	  malta->SetPixelPulse(pixY+6,pixX,true);
	  malta->SetPixelPulse(pixY+6,pixX,true);
	  pPulsed_h2->Fill(pixX, pixY+6);
	  
	  malta->SetPixelPulse(pixY+1,pixX+checker, true);
	  malta->SetPixelPulse(pixY+1,pixX+checker, true);
	  pPulsed_h2->Fill(pixX+checker, pixY+1);
	  malta->SetPixelPulse(pixY+3,pixX+checker, true);
	  malta->SetPixelPulse(pixY+3,pixX+checker, true);
	  pPulsed_h2->Fill(pixX+checker, pixY+3);
	  malta->SetPixelPulse(pixY+5,pixX+checker, true);
	  malta->SetPixelPulse(pixY+5,pixX+checker, true);
	  pPulsed_h2->Fill(pixX+checker, pixY+5);
	  malta->SetPixelPulse(pixY+7,pixX+checker, true);
	  malta->SetPixelPulse(pixY+7,pixX+checker, true);
	  pPulsed_h2->Fill(pixX+checker, pixY+7);
	 
	}
	*/
	
	malta->SetDoubleColumnMask( (int)(pixX/2),false);
	malta->SetDoubleColumnMask( (int)(pixX/2),false);		

	malta->SetPixelMaskRow(pixY,false);
	malta->SetPixelMaskRow(pixY,false);
	if (onlyEvenPixel) {
	  //malta->SetPixelMaskRow(pixY+1,false);
	  //malta->SetPixelMaskRow(pixY+1,false);
	}   
	if (cDoubleYPixel) {
	  malta->SetPixelMaskRow(pixY+offset,false);
	  malta->SetPixelMaskRow(pixY+offset,false);
	  //malta->SetDoubleColumnMask( (int)((pixX+256)%512/2),false);
	  //malta->SetDoubleColumnMask( (int)((pixX+256)%512/2),false);	
	  //if (onlyEvenPixel) {
	  //malta->SetPixelMaskRow(pixY+256+1,false);
	  //malta->SetPixelMaskRow(pixY+256+1,false);
	  //}
	}
	/*
	if (onlyGroup) {
	  malta->SetPixelMaskRow(pixY+1,false);
	  malta->SetPixelMaskRow(pixY+1,false);
	  malta->SetPixelMaskRow(pixY+2,false);
	  malta->SetPixelMaskRow(pixY+2,false);
	  malta->SetPixelMaskRow(pixY+3,false);
	  malta->SetPixelMaskRow(pixY+3,false);
	  malta->SetPixelMaskRow(pixY+4,false);
	  malta->SetPixelMaskRow(pixY+4,false);
	  malta->SetPixelMaskRow(pixY+5,false);
	  malta->SetPixelMaskRow(pixY+5,false);
	  malta->SetPixelMaskRow(pixY+6,false);
	  malta->SetPixelMaskRow(pixY+6,false);
	  malta->SetPixelMaskRow(pixY+7,false);
	  malta->SetPixelMaskRow(pixY+7,false);
	}  
	*/
  
	//Parameter loop
	if (rescan_first_point>=rescan_first_point_max){
	  cout << "pixel dead, stopping the scan" << endl;
	  break;
	}
	usleep( (int)(mainSleep*100000) );
    
	malta->SetConfigMode(false);
	if(!g_cont){break;}
	
	//Create ntuple
	ostringstream os;
	os.str("");
	os << outdir  << "/thrscan_"
	   << setw(3) << setfill('0') << pixX << "_"
	   << setw(3) << setfill('0') << pixY << "_"
	   << setw(3) << setfill('0') << paramMin << ".root";
	string fname = os.str();
	cout << fname << endl;
	MaltaTree *ntuple = new MaltaTree();
	if (!noFile) ntuple->Open(fname,"RECREATE");
	std::cout << "*************"+fname<<std::endl;
	
	//Flush MALTA
	malta->ReadoutOff();
	malta->ResetFifo();
	malta->ResetL1Counter();	  
	uint32_t event=0;
	//////////usleep( (int)(mainSleep*1000000) );
	malta->ReadoutOn();
	malta->Trigger(trigger,true);
	malta->ReadoutOff();
	
	MaltaData md,md2;
	uint32_t words[2];
	bool isFirst=true;
	
	int numberOfHitsInMyPixel=0;       
	int numberOfHits=0;       
	while(g_cont){
	  
	  if(event>80000){break;}
	  bool markDuplicate=false;
	  malta->ReadMaltaWord(&words[0]);
	    
	  if (words[0]==0) {
	    cout << " ... no more words after word: " << event << endl;
	    break; //VD
	  }
	  if (words[0]==0 && words[1]==0) { continue;}
	    
	  if (isFirst){
	    md.setWord1(words[0]);
	    md.setWord2(words[1]);
	    isFirst = false;
	    continue;
	  }
	  
	  md2.setWord1(md.getWord1());
	  md2.setWord2(md.getWord2());
	  md.setWord1(words[0]);
	  md.setWord2(words[1]);
	  
	  md.unPack();
	  md2.unPack();
	  if (!noFile) ntuple->Set(&md2);
	  
	  uint32_t phase1 = md2.getPhase();
	  uint32_t winid1 = md2.getWinid();
	  uint32_t bcid1 = md2.getBcid();          
	  uint32_t l1id1 = md2.getL1id();          
	  
	  uint32_t phase2 = md.getPhase();
	  uint32_t winid2 = md.getWinid();
	  uint32_t bcid2 = md.getBcid();
	  uint32_t l1id2 = md.getL1id();	    
	  bool markDuplicateNext = false;
	  bool isRealDuplicate   = false;
	  
	  /*
	  for (unsigned h=0; h<md2.getNhits(); h++) {
	    uint32_t LpixX=md2.getHitColumn(h);
	    uint32_t LpixY=md2.getHitRow(h);
	    cout << event << " :: " << LpixX << " / " << LpixY << " ||||| " << md2.getPhase() << " / " << md2.getWinid() << " / " << md2.getBcid() << " / " << md2.getL1id() << endl;  
	  }
	  */

	  isRealDuplicate = markDuplicateNext;
	  // REWRITING DUPLICATION REOMVAL HERE
	  if( (bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 && bcid1==63)){
	    if( (winid2-winid1)==1 || (winid2==0 && winid1==7 )){
	      if( phase2==0 && (l1id1==l1id2)){
		if( phase1>=5) markDuplicate = true;
		else if(phase1>3){
		  markDuplicateNext = true;
		  markDuplicate = false;
		}
		else markDuplicate = false;  
	      }
	      else  markDuplicate = false;
	    }
	    else markDuplicate = false;
	  }
	  else  markDuplicate = false;  
	  if( isRealDuplicate==true){ 
	    markDuplicate = 1;
	    markDuplicateNext=false;
	  }
	  else{
	    if(markDuplicate==true)  markDuplicate = 1;
	    else                     markDuplicate = 0;
	  }
	  if (!noFile) ntuple->SetIsDuplicate(markDuplicate);
	  if (!noFile) ntuple->Fill();
	  event+=1;
	  
	  double timing=md2.getBcid()*25+md2.getWinid()*3.125;
	  if (markDuplicate==0 and (timing>300 and timing<350) ) {
	    for (unsigned h=0; h<md2.getNhits(); h++) {
	      uint32_t LpixX=md2.getHitColumn(h);
	      uint32_t LpixY=md2.getHitRow(h);
	      numberOfHits++;  
	      //cout << " hit: " << LpixX << " , " << LpixY << endl;
	      if (LpixX==pixX and LpixY==pixY){ 
		numberOfHitsInMyPixel++;
		nHits_h2->Fill(pixX, pixY);
		//numberOfHitsInMyPixel = numberOfHitsInMyPixel - markDuplicate;
	      }
	      if (onlyEvenPixel) {
		if (LpixX==pixX+1 and LpixY==pixY) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX+1, pixY);
		}
		//if (LpixX==pixX and LpixY==pixY+1) nHits_h2->Fill(pixX, pixY+1);
		//if (LpixX==pixX+1 and LpixY==pixY+1) nHits_h2->Fill(pixX+1, pixY+1);
	      }
	      if (cDoubleYPixel) {
		if ( LpixX==pixX and LpixY==pixY+offset){
		  nHits_h2->Fill( pixX, pixY+offset);
		}
		/*
		if (onlyEvenPixel) {
		  if (LpixX==(pixX+256+1)%512 and LpixY==pixY+256) nHits_h2->Fill((pixX+256+1)%512, pixY+256);
		  if (LpixX==(pixX+256)%512 and LpixY==pixY+256+1) nHits_h2->Fill((pixX+256)%512, pixY+256+1);
		  if (LpixX==(pixX+256+1)%512 and LpixY==pixY+256+1) nHits_h2->Fill((pixX+256+1)%512, pixY+256+1);
		}
		*/
	      }
	      /*
	      if (onlyGroup) {
<<<<<<< HEAD
		if (LpixX==pixX and LpixY==pixY+2) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX, pixY+2);
		}
		if (LpixX==pixX and LpixY==pixY+4) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX, pixY+4);
		}
		if (LpixX==pixX and LpixY==pixY+6) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX, pixY+6);
		}
		if (LpixX==pixX+checker and LpixY==pixY+1) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX+checker, pixY+1);
		}
		
		if (LpixX==pixX+checker and LpixY==pixY+3) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX+checker, pixY+3);
		}
		
		if (LpixX==pixX+checker and LpixY==pixY+5) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX+checker, pixY+5);
		}
		
		if (LpixX==pixX+checker and LpixY==pixY+7) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX+checker, pixY+7);
		}
		if (LpixX==pixX+1 and LpixY==pixY) {
		  numberOfHitsInMyPixel++;
		  nHits_h2->Fill(pixX+1, pixY);
		}
		if (LpixX==pixX and LpixY==pixY+1) nHits_h2->Fill(pixX, pixY+1);
		if (LpixX==pixX+1 and LpixY==pixY+1) nHits_h2->Fill(pixX+1, pixY+1);
		
		if (LpixX==pixX and LpixY==pixY+2) nHits_h2->Fill(pixX, pixY+2);
		if (LpixX==pixX+1 and LpixY==pixY+2) nHits_h2->Fill(pixX+1, pixY+2);
		if (LpixX==pixX and LpixY==pixY+3) nHits_h2->Fill(pixX, pixY+3);
		if (LpixX==pixX+1 and LpixY==pixY+3) nHits_h2->Fill(pixX+1, pixY+3);
	      }
	      */
	    }
	  } 
	}
        //End DAQ loop
        //End Trigger lopp
	cout << endl << "Number of events: " << numberOfHits << " --> in the pixel I want to pulse: " << numberOfHitsInMyPixel << endl;
	
	if (!noFile) ntuple->Close();
	//This line is needed to avoid filling the memory of the computer. Carlos
	delete ntuple;
	
	//End param loop
	malta->SetConfigMode(true,0.8);
       
	usleep( (int)(mainSleep*1000000) );
	malta->SetPixelPulse(pixY    ,pixX,false);
	malta->SetPixelPulse(pixY    ,pixX,false);
	if (onlyEvenPixel) {
	  malta->SetPixelPulse(pixY   ,pixX+1,false);
	  malta->SetPixelPulse(pixY   ,pixX+1,false);
	  //malta->SetPixelPulse(pixY+1 ,pixX  ,false);
	  //malta->SetPixelPulse(pixY+1 ,pixX  ,false);
	  //malta->SetPixelPulse(pixY+1 ,pixX+1,false);
	  //malta->SetPixelPulse(pixY+1 ,pixX+1,false);
	}
	if (cDoubleYPixel) {
	  malta->SetPixelPulse(pixY+offset,pixX,false);
	  malta->SetPixelPulse(pixY+offset,pixX,false);
	  /*
	  if (onlyEvenPixel) {
	    malta->SetPixelPulse(pixY+256,pixX+1,false);
	    malta->SetPixelPulse(pixY+256,pixX+1,false);
	    malta->SetPixelPulse(pixY+256+1,pixX,false);
	    malta->SetPixelPulse(pixY+256+1,pixX,false);
	    malta->SetPixelPulse(pixY+256+1,pixX+1,false);
	    malta->SetPixelPulse(pixY+256+1,pixX+1,false);
	  }
	  */
	}
	/*
	if (onlyGroup) {
	  malta->SetPixelPulse(pixY    ,pixX,false);
	  malta->SetPixelPulse(pixY    ,pixX,false);
	  malta->SetPixelPulse(pixY+2    ,pixX,false);
	  malta->SetPixelPulse(pixY+2    ,pixX,false);
	  malta->SetPixelPulse(pixY+4    ,pixX,false);
	  malta->SetPixelPulse(pixY+4    ,pixX,false);
	  malta->SetPixelPulse(pixY+2    ,pixX,false);
	  malta->SetPixelPulse(pixY+2    ,pixX,false);
	  
	  malta->SetPixelPulse(pixY +1    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +1    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +3    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +3    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +5    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +5    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +7    ,pixX +checker,false);
	  malta->SetPixelPulse(pixY +7    ,pixX +checker,false);
	}
	*/
	malta->SetDoubleColumnMask( (int)(pixX/2),true);
	malta->SetDoubleColumnMask( (int)(pixX/2),true);
	malta->SetPixelMaskRow(pixY,true);
	malta->SetPixelMaskRow(pixY,true);
	if (onlyEvenPixel) {
	  //malta->SetPixelMaskRow(pixY+1,true);
	  //malta->SetPixelMaskRow(pixY+1,true);
	}
	if (cDoubleYPixel) {
	  malta->SetPixelMaskRow(pixY+offset,true);
	  malta->SetPixelMaskRow(pixY+offset,true);
	  //malta->SetDoubleColumnMask( (int)((pixX+256)%512/2),true);
	  //malta->SetDoubleColumnMask( (int)((pixX+256)%512/2),true);
	  //if (onlyEvenPixel) {
	  //malta->SetPixelMaskRow(pixY+256+1,true);
	  //  malta->SetPixelMaskRow(pixY+256+1,true);
	}
	/*
	if (onlyGroup) {
		malta->SetPixelMaskRow(pixY+1,true);
	    malta->SetPixelMaskRow(pixY+1,true);
	    malta->SetPixelMaskRow(pixY+2,true);
	    malta->SetPixelMaskRow(pixY+2,true);
	    malta->SetPixelMaskRow(pixY+3,true);
	    malta->SetPixelMaskRow(pixY+3,true);
	    malta->SetPixelMaskRow(pixY+4,true);
	    malta->SetPixelMaskRow(pixY+4,true);
	    malta->SetPixelMaskRow(pixY+5,true);
	    malta->SetPixelMaskRow(pixY+5,true);
	    malta->SetPixelMaskRow(pixY+6,true);
	    malta->SetPixelMaskRow(pixY+6,true);
	    malta->SetPixelMaskRow(pixY+7,true);
	    malta->SetPixelMaskRow(pixY+7,true);
	  malta->SetPixelMaskRow(pixY+1,true);
	  malta->SetPixelMaskRow(pixY+1,true);
	  malta->SetPixelMaskRow(pixY+2,true);
	  malta->SetPixelMaskRow(pixY+2,true);
	  malta->SetPixelMaskRow(pixY+3,true);
	  malta->SetPixelMaskRow(pixY+3,true);
	}
	*/
	// getting time information 
	time(&end_pix);
      }
    }
    break;
    
  default:
    break;
  }
  //////////////////////////////////////////////////////// end pixel loop
  m_config_tree->Scan();
  time(&end);
  float Scantime=end-start;
  if (onlyEvenPixel) pixels*=4;
  if (cDoubleYPixel) pixels*=2;
  if (onlyGroup) pixels*=4;
  cout << endl << "TOTAL scantime for " << pixels << " : " << Scantime << endl << endl;
  
  malta->SetClock(false);
  malta->SetConfigMode(false);
  usleep(1000);    
  //  printf("Scanning time is %ld\n", Scantime);
  delete m_config_tree;
  delete malta;
  
  //plot for analog scan
  TCanvas* can = new TCanvas("canN","caN",800,800);
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  nHits_h2->GetXaxis()->SetRangeUser(cRegion[0],cRegion[0] + cRegion[1]); 
  nHits_h2->GetYaxis()->SetRangeUser(cRegion[2],cRegion[2] + cRegion[3]); 
  if (cDoubleYPixel){
    nHits_h2->GetYaxis()->SetRangeUser(cRegion[2],cRegion[2] + cRegion[3]+offset); 
    nHits_h2->GetXaxis()->SetRangeUser(cRegion[0],min(cRegion[0] + cRegion[1] +offset, 512)); ; 
  }
  nHits_h2->SetMinimum(trigger*0.90);
  nHits_h2->SetMaximum(trigger*1.01);
  nHits_h2->Draw("colz");
  can->Print( (outdir+"/nHits"+charge+"e_2D.pdf").c_str() );
  
  
  TFile *outputF = new TFile((outdir+"/analog_sum.root").c_str(), "RECREATE");
  can->Write();
  nHits_h2->Write();
  pPulsed_h2->Write();
  outputF->Close();
  
  int n0{0}, nFewer{0}, nMore{0}, nClose{0}, nCloser{0};
  
  for (int i =1; i<513; i++){
    for (int j =1; j<513; j++){
      if (pPulsed_h2->GetBinContent(i,j)==0) continue;
      if (nHits_h2->GetBinContent(i,j)==0) n0++;
      if (nHits_h2->GetBinContent(i,j)<trigger * 0.9 ) nFewer++;
      if (nHits_h2->GetBinContent(i,j)>trigger *1.1)  nMore++;
      if ((abs(nHits_h2->GetBinContent(i,j)- trigger) <= 0.1*trigger) and abs(nHits_h2->GetBinContent(i,j)- trigger) > 0.05*trigger  ) nClose++;
      if (abs(nHits_h2->GetBinContent(i,j)- trigger) <= 0.05*trigger ) nCloser++;
    }
  }
  
  cout <<"# of pixels with 0 hits: "<<n0 <<endl;
  cout <<"# of pixels with  # of hits < 90%  nTrigger: "<<nFewer <<endl;
  cout <<"# of pixels with # of hits > 110% nTrigger: "<<nMore <<endl;
  cout <<"# of pixels # of hits within 10% of nTriggger: "<<nClose <<endl;  
  cout <<"# of pixels # of hits within 5% of nTriggger: "<<nCloser <<endl;  
  
  delete outputF;
  delete pPulsed_h2;
  delete can;
  delete nHits_h2;
  cout << "Have a nice day" << endl;
  return 0;
}

