#include "Malta/MaltaThresholdAnalysis.h"
#include "Malta/MaltaData.h"
#include <sstream>
#include <iostream>
#include <iomanip>
#include "TCanvas.h"
#include "TH1.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

//float nPulse=500;
int VHIGH_ref=90;

void MaltaThresholdAnalysis::setNPulse(int pulse) {
  m_npulse = pulse;
}

float MaltaThresholdAnalysis::getValueEl(int VL, int VH) {
  return (m_conversion[VH]-m_conversion[VL])*1.43*1000;
}

MaltaThresholdAnalysis::MaltaThresholdAnalysis(uint32_t nbins, float minVal, float maxVal, string folderName){
  m_folderName=folderName;
  m_nbins=nbins;
  m_minVal=minVal;
  m_maxVal=maxVal;
  //m_file=TFile::Open(filename.c_str(),"RECREATE");
  for(uint32_t x=0;x<512;x++){
    for(uint32_t y=0;y<512;y++){
      m_histos[x][y]=0;
    }
  }

  //ugly as fuck
  m_conversion[5]=0.470536589622;
  m_conversion[6]=0.483547687531;
  m_conversion[7]=0.496614396572;
  m_conversion[8]=0.50971031189;
  m_conversion[9]=0.522888302803;
  m_conversion[10]=0.536034822464;
  m_conversion[11]=0.549173116684;
  m_conversion[12]=0.562351584435;
  m_conversion[13]=0.575547575951;
  m_conversion[14]=0.588795006275;
  m_conversion[15]=0.601973772049;
  m_conversion[16]=0.615173399448;
  m_conversion[17]=0.628306210041;
  m_conversion[18]=0.641517221928;
  m_conversion[19]=0.65463912487;
  m_conversion[20]=0.667873382568;
  m_conversion[21]=0.681168675423;
  m_conversion[22]=0.694335281849;
  m_conversion[23]=0.707544386387;
  m_conversion[24]=0.720710277557;
  m_conversion[25]=0.733884692192;
  m_conversion[26]=0.747146606445;
  m_conversion[27]=0.7603713274;
  m_conversion[28]=0.773562729359;
  m_conversion[29]=0.786770820618;
  m_conversion[30]=0.799954771996;
  m_conversion[31]=0.813241422176;
  m_conversion[32]=0.826474010944;
  m_conversion[33]=0.839749693871;
  m_conversion[34]=0.85285282135;
  m_conversion[35]=0.86614048481;
  m_conversion[36]=0.879237294197;
  m_conversion[37]=0.892526686192;
  m_conversion[38]=0.905772328377;
  m_conversion[39]=0.919007599354;
  m_conversion[40]=0.932211875916;
  m_conversion[41]=0.945459127426;
  m_conversion[42]=0.958622395992;
  m_conversion[43]=0.972051024437;
  m_conversion[44]=0.985207080841;
  m_conversion[45]=0.998256623745;
  m_conversion[46]=1.01185202599;
  m_conversion[47]=1.02502298355;
  m_conversion[48]=1.03817403316;
  m_conversion[49]=1.05153799057;
  m_conversion[50]=1.06465196609;
  m_conversion[51]=1.07807099819;
  m_conversion[52]=1.09128904343;
  m_conversion[53]=1.10454201698;
  m_conversion[54]=1.11772596836;
  m_conversion[55]=1.13060200214;
  m_conversion[56]=1.14382994175;
  m_conversion[57]=1.15714299679;
  m_conversion[58]=1.17058300972;
  m_conversion[59]=1.18362498283;
  m_conversion[60]=1.19692003727;
  m_conversion[61]=1.21019101143;
  m_conversion[62]=1.22334802151;
  m_conversion[63]=1.2362960577;
  m_conversion[64]=1.24973201752;
  m_conversion[65]=1.26270294189;
  m_conversion[66]=1.2759629488;
  m_conversion[67]=1.28909504414;
  m_conversion[68]=1.30228900909;
  m_conversion[69]=1.31523704529;
  m_conversion[70]=1.32855200768;
  m_conversion[71]=1.34175503254;
  m_conversion[72]=1.35489094257;
  m_conversion[73]=1.36789298058;
  m_conversion[74]=1.38094103336;
  m_conversion[75]=1.39407598972;
  m_conversion[76]=1.40724599361;
  m_conversion[77]=1.42025005817;
  m_conversion[78]=1.43341195583;
  m_conversion[79]=1.44616997242;
  m_conversion[80]=1.459010005;
  m_conversion[81]=1.47182500362;
  m_conversion[82]=1.48450803757;
  m_conversion[83]=1.49717497826;
  m_conversion[84]=1.50973403454;
  m_conversion[85]=1.52167499065;
  m_conversion[86]=1.53381705284;
  m_conversion[87]=1.54540598392;
  m_conversion[88]=1.55748999119;
  m_conversion[89]=1.56867098808;
  m_conversion[90]=1.57997202873;
  m_conversion[91]=1.59100401402;
  m_conversion[92]=1.60204899311;
  m_conversion[93]=1.61239099503;
  m_conversion[94]=1.62228703499;
  m_conversion[95]=1.63252401352;
  m_conversion[96]=1.64201998711;
  m_conversion[97]=1.65092897415;
  m_conversion[98]=1.65975797176;
  m_conversion[99]=1.66795694828;
  m_conversion[100]=1.67538702488;
  m_conversion[101]=1.68260002136;
  m_conversion[102]=1.68940901756;
  m_conversion[103]=1.69523894787;
  m_conversion[104]=1.69989895821;
  m_conversion[105]=1.70421802998;
  m_conversion[106]=1.70840895176;
  m_conversion[107]=1.71099495888;
  m_conversion[108]=1.71366298199;
  m_conversion[109]=1.7154289484;
  m_conversion[110]=1.71689796448;
  m_conversion[111]=1.7177759409;
  m_conversion[112]=1.71827995777;
  m_conversion[113]=1.71903300285;
  m_conversion[114]=1.71925795078;
  m_conversion[115]=1.71955299377;
  m_conversion[116]=1.7197060585;
  m_conversion[117]=1.72003304958;
  m_conversion[118]=1.72035694122;
  m_conversion[119]=1.72043204308;
  m_conversion[120]=1.72048795223;
  m_conversion[121]=1.72008395195;
  m_conversion[122]=1.72045302391;
  m_conversion[123]=1.72032701969;
  m_conversion[124]=1.72055101395;
  m_conversion[125]=1.72063994408;
  m_conversion[126]=1.72040498257;
  m_conversion[127]=1.72025501728;

}

MaltaThresholdAnalysis::~MaltaThresholdAnalysis(){
  /*m_file->cd();
  for(uint32_t x=0;x<512;x++){
    for(uint32_t y=0;y<512;y++){
      if(m_histos[x][y]==0) continue;
      m_histos[x][y]->Write();
    }
  }
  //m_file->ls();
  m_file->Close();
  //delete m_file;
  */
}

void MaltaThresholdAnalysis::process(MaltaData *md, uint32_t column, uint32_t row, float param, bool isDuplicate){

    //cout << __PRETTY_FUNCTION__ << ": processing col,row = " << column << "," << row  << " " <<param <<endl;

  //get the timing ... and eventually cut on it
  //float timing=25.*md->getBcid()+3.125*md->getWinid();
  if (isDuplicate==true) return;
  
  for (uint32_t i=0;i<md->getNhits();i++) {
    if ( column!=md->getHitColumn(i) ) continue;

    if ( row!=md->getHitRow(i)) continue; // and  row+450!=md->getHitRow(i) ) continue;

    uint32_t y=md->getHitRow(i);
    uint32_t x=md->getHitColumn(i);
    if(!m_histos[x][y]||m_histos[x][y]==0){
      ostringstream os;
      os << "hits_" 
         << setw(3) << setfill('0') << x << "_"
         << setw(3) << setfill('0') << y;
      //m_file->cd();
      m_histos[x][y] = new TH1F(os.str().c_str(),";Parameter;Entries",m_nbins+1,m_minVal,m_maxVal+1);
      m_histos[x][y]->SetDirectory(0);
    }
    //cout << "x,y=" << x << "," << y << endl;
    m_histos[x][y]->Fill(param);
  }
  if (m_histos[column][row]!=NULL) m_histos[column][row]->Fill(param,0.000001);
}

void MaltaThresholdAnalysis::end(float Scantime, float mScantime_pix[], TTree *config_tree){

  ///there is an issue in this method! it's called every time but it re-loop!
  // at the same time is bad to call it always at the end
  // solution for the time: store it in the histo GetMaximum()
  // need to see how easily I can append things to an ntuple!

  /////////////////////01.07.2019/////////////////////////////////////////////
  // ntuple to store th scan data
  TFile *th_ntuple = new TFile( (m_folderName+"/th_sum_IDB_"+".root").c_str(),"RECREATE");
  TTree *th_tree   = new TTree("Thres","DataSeream");
  int tree_pixX, tree_pixY, tree_DoF;
  float tree_chi2;
  float tree_scale,tree_sigma,tree_vel;//vel=Vh-Vl
  float tree_thres; //threshold & Thre error after calib
  float tree_Scantime; 
  int tree_PN1stvlow=0;//Nomber of get pulses at 1st Vlow

  th_tree->Branch("pixX", &tree_pixX , "pixX/i");
  th_tree->Branch("pixY", &tree_pixY , "pixY/i");
  th_tree->Branch("DoF",  &tree_DoF  , "DoF/i");
  th_tree->Branch("chi2", &tree_chi2 , "chi2/f");
  th_tree->Branch("scale",&tree_scale, "Scale/f");
  th_tree->Branch("thres",&tree_vel  , "Vh_Vl/f");
  th_tree->Branch("sigma",&tree_sigma, "sigma/f"); 
  //th_tree->Branch("vlow"    ,&tree_vlow    ,"vlow/f"); 
  // th_tree->Branch("thres"   ,&tree_thres   ,"thres/f");
  //th_tree->Branch("therr"   ,&tree_therr   ,"therr/f");
  th_tree->Branch("scantime",&tree_Scantime,"scantime/f");
  th_tree->Branch("PN1st",&tree_PN1stvlow, "PN1st/i"); 
  /////////////////////////////////////////////////////////////////////////////////////

  TH1F* h_base=new TH1F("base","base;V_H-V_L (el);counts",m_nbins, getValueEl(m_maxVal,VHIGH_ref), getValueEl(m_minVal,VHIGH_ref));
  h_base->SetDirectory(0);
  TCanvas* can=new TCanvas("blah","blah",800,600);

  for(uint32_t x=0;x<512;x++){
    for(uint32_t y=0;y<512;y++){

      if(!m_histos[x][y]||m_histos[x][y]==0){continue;}
      float integral = m_histos[x][y]->Integral(0,-1);
      //if(integral <1000){continue;}
      if(integral ==  0){continue;}

      string name=m_histos[x][y]->GetName();
      
      //raw data --- Vlow vs Count --- 
      TGraphErrors* tmpGraph_raw=new TGraphErrors();//before convertion
      tmpGraph_raw -> SetName(("raw_"+name+"_graph").c_str());
      int count=-1;
      for (int bin=1;bin<=m_histos[x][y]->GetNbinsX(); bin++) {
        float content=m_histos[x][y]->GetBinContent(bin);
        count+=1;
        if (content!=0) {
	  tmpGraph_raw->SetPoint(count, (int)m_histos[x][y]->GetXaxis()->GetBinCenter(bin), content );     
          if (content < 1 ) tmpGraph_raw->SetPointError(count, 0, 1 );  
          else tmpGraph_raw->SetPointError(count, 0, sqrt(content) );
	}
 	else count--;
	//else if ( check_next_empty( bin, m_histos[x][y] ) == false ) count--;	
	//else if ( check_next_empty( bin, m_histos[x][y] ) == true ) {
	//  tmpGraph_raw->SetPoint(count, (int)m_histos[x][y]->GetXaxis()->GetBinCenter(bin), content );
	//  tmpGraph_raw->SetPointError(count, 0, 1 );
      	//}
        if(bin==1){
	  tree_PN1stvlow=(int)content;
	  std::cout << "pulse No. I got is "<< tree_PN1stvlow <<std::endl;
	}
      }
      tmpGraph_raw->GetXaxis()->SetTitle("V_L");
      tmpGraph_raw->GetYaxis()->SetTitle("Count");
      tmpGraph_raw->SetMarkerStyle(20);
      tmpGraph_raw->SetMarkerSize(0.9);
      tmpGraph_raw->SetLineWidth(2);
      tmpGraph_raw->Draw("P");
      tmpGraph_raw->Write();
      
      //conversion to el -- moved up
      //TCanvas* can=new TCanvas(m_histos[x][y]->GetName(),m_histos[x][y]->GetName(),800,600);
      gStyle->SetOptStat(0);
      gStyle->SetOptFit(1111);
      //      h_base->SetMaximum(nPulse*1.80);
      h_base->SetMaximum(m_npulse*1.80);
      h_base->SetMinimum(0.0);
      h_base->Draw("AXIS");

      TGraphErrors* tmpGraph=new TGraphErrors();
      tmpGraph -> SetName((name+"_graph").c_str());
      count=-1;
      for (int bin=1;bin<=m_histos[x][y]->GetNbinsX(); bin++) {
        float content=m_histos[x][y]->GetBinContent(bin);
        count+=1;
        if (content!=0) {
          tmpGraph->SetPoint(count, getValueEl((int)m_histos[x][y]->GetXaxis()->GetBinCenter(bin),VHIGH_ref), content );
          if (content < 1 ) tmpGraph->SetPointError(count, 0, 1 );
          else tmpGraph->SetPointError(count, 0, sqrt(content) );
	}
 	else count--; //if ( check_next_empty( bin, m_histos[x][y] ) == false ) count--;	
	//else if ( check_next_empty( bin, m_histos[x][y] ) == true ) {
        //  tmpGraph->SetPoint(count, getValueEl((int)m_histos[x][y]->GetXaxis()->GetBinCenter(bin),VHIGH_ref), content );
	//  tmpGraph->SetPointError(count, 0, 1 );
	//}
      }

      tmpGraph->SetMarkerStyle(20);
      tmpGraph->SetMarkerSize(0.9);
      tmpGraph->SetLineWidth(2);
      tmpGraph->Draw("P");
      double max_gr = TMath::MaxElement(tmpGraph->GetN(),tmpGraph->GetY());
    tmpGraph->Print("v"); 
      //fitting
      float start=getValueEl(m_histos[x][y]->GetXaxis()->GetXmax(),VHIGH_ref);
      float end  =getValueEl(m_histos[x][y]->GetXaxis()->GetXmin(),VHIGH_ref);

      TF1 fit("fit","[0]/2*(1+TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))", start, end);
      //      fit.SetParameter(0,nPulse);
      //      fit.SetParLimits(0,nPulse*0.5,nPulse*1.5);
      fit.SetParameter(0,max_gr);
      fit.SetParError(0,sqrt(max_gr));
      //      fit.SetParLimits(0,m_npulse*0.5,m_npulse*1.5);
      //fit.SetParLimits(0,m_npulse*0.5,m_npulse*1.5);      
      fit.SetParLimits(0,max_gr*0.8,max_gr*1.2);

      fit.SetParameter(1, find_middlePoint(tmpGraph));//(start+end)/2);
      fit.SetParLimits(1,start,end);
      //fit.SetParLimits(1,find_middlePoint(tmpGraph)-100,find_middlePoint(tmpGraph)+100);
      fit.SetParError(1,50);
      
      fit.SetParLimits(2,2,30);
      fit.SetParameter(2,10);
      tmpGraph->Fit(&fit,"RE0");
      fit.SetLineStyle(2);
      fit.SetLineColor(2);
      TLine l=TLine(fit.GetParameter(1),0,fit.GetParameter(1),h_base->GetMaximum()*0.95);
      l.SetLineWidth(1);
      l.Draw("SAMEC");
      fit.Draw("SAMEC");

      //Filling fit parameter to th_tree
      tree_pixX=x;
      tree_pixY=y;
      tree_DoF=fit.GetNDF();
      tree_chi2 =fit.GetChisquare();
      tree_scale=fit.GetParameter(0);

      tree_vel  =fit.GetParameter(1); //VD: this is actually the real threshold      
      tree_sigma=fit.GetParameter(2); //VD: this it actually the noise

      // tree_thres = (1580-tree_vlow)*1.43;       //VD: this is not needed
      // tree_vlow=800+(fit.GetParameter(1)-30)*13.2; //?????? //VD: this is not needed
      // tree_therr=fit.GetParameter(2)*13.2*1.43; //VD:this is not needed
      tree_Scantime=mScantime_pix[x*512+y];

      can->Update();

      th_tree->Fill();
      can -> Write();
      //tmpGraph->Write();

      std::cout << "For bin " << x << "_" << y << " threshold is: "<< tree_thres << std::endl << std::endl;
      
     
      // TLatex lx;
      // lx.SetNDC();
      // //lx.DrawLatex(0.68 , 0.6 , Form("#scale[0.7]{Thres: %3.0f e}",threshold));
      // //lx.DrawLatex(0.68 , 0.52, Form("#scale[0.7]{Noise: %3.0f e}",noise));
      // lx.DrawLatex(0.68,  0.40, (name).c_str() );

      can->Print( (m_folderName+"/hists/"+name+".pdf").c_str() );
      delete tmpGraph;
      delete tmpGraph_raw;
      delete m_histos[x][y];
    }
  }
  delete can;
  
  th_ntuple->cd();
  th_tree->Write();
  
  config_tree->Write();
  th_ntuple->Close();
  //delete th_tree;
  //delete config_tree;
  delete th_ntuple;
}


bool MaltaThresholdAnalysis::check_next_empty(int bin, TH1F* h){
  bool empty = false;
  if (h->Integral(bin, h->GetNbinsX()) == 0) empty = true;
  return empty;
}

double MaltaThresholdAnalysis::find_middlePoint(TGraphErrors *g){

  double mid = (TMath::MaxElement(g->GetN(),g->GetY())+TMath::MinElement(g->GetN(),g->GetY()))*0.5;
  double x=-99999;  double y=-99999;
  double x_tmp=0;  double y_tmp=0;

  for (int i =0; i <  g->GetN(); ++i){
     g->GetPoint(i,x_tmp,y_tmp);
     if ( abs(y_tmp-mid) < abs(y-mid) )  {
	y = y_tmp;
	x = x_tmp;
     }
     //cout << "x " << x << "  " << x_tmp << endl;
     //cout << "y " << y << "  " << y_tmp << endl;     cout << "mid " << mid<< endl;
  }
  return x;
}

