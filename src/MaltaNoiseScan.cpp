//! -*-C++-*-
#include "Malta/MaltaData.h"
#include "Malta/MaltaBase.h"
#include "Malta/MaltaTree.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include <unistd.h>
#include <sys/stat.h>

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"
#include "TLegend.h"


/*
MaltaNoiseScan.cpp
patrick.moriishi.freeman@cern.ch
Command to run a noise scan of a single MALTA chip
Examples usage:
MaltaNoiseScan -o test -f W7R12 -a udp://ep-ade-gw-07.cern.ch:50002 -l 39 -h 40 -t 100 -r 10 -s 10 -c configs/config_W7R12_IDB10_ITHR10.txt -y
*/

float unit=1000;

using namespace std;
bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

stringstream systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  cout << ss.str();
  return ss;
}

void configureChip(MaltaBase * malta, string cConfig, bool firstconfiguration=false){
    ifstream fr;
    fr.open(string(cConfig).c_str());
    if (!fr.is_open()){
      cout << "#####################################################" << endl;
      cout << "##  The configuration file cannot be opened        ##" << endl;
      cout << "##  Stopping script.                               ##" << endl;
      cout << "#####################################################" << endl;
      exit(0);
    }

    malta->PreConfigMaltaC();
    malta->SetConfigFromFile(string(cConfig));

    malta->SetVPULSE_HIGH(90);

    malta->SetPixelPulseColumn(5,false);
    malta->SetPixelPulseColumn(5,false);

    malta->SetPixelPulseRow(0,false);
    malta->SetPixelPulseRow(0,false);

    malta->SetPixelPulseRow(251,false);
    malta->SetPixelPulseRow(251,false);

    malta->SetPixelPulseRow(511,false);
    malta->SetPixelPulseRow(511,false);

    //check which chip of the module is read out and mask the matrix of all others
    //else the other chip(s) may fill up the fifo and mess with the noise measurement

    //current chip id is set from input args and must be preserved!
    uint32_t currentChipID = malta->GetCurrentChipID();
    std::cout << "Chip to read out: " << currentChipID << std::endl;

    for(auto id: malta->GetListOfChipIDs()){
      //don't mask the chip that is scanned!
      if(id == currentChipID) continue;

      //silence all other chips in the module
      malta->SetSCIOToChip(id);
      std::cout << "Set up mask for chip: " << id << std::endl;
      for (int i =0; i < 256; ++i){
        malta->SetDoubleColumnMask( i,true);
        malta->SetDoubleColumnMask( i,true);
      }
      for (int i =0; i < 512; ++i){
        malta->SetPixelMaskColumn(i,true);
        malta->SetPixelMaskColumn(i,true);
        malta->SetPixelMaskDiag(i,true);
        malta->SetPixelMaskDiag(i,true);
        malta->SetPixelMaskRow(i,true);
        malta->SetPixelMaskRow(i,true);
      }
    }

    //make sure to talk to the chip that was specified in the input args
    malta->SetSCIOToChip(currentChipID);

    malta->SetConfigMode(false);
}

int main(int argc, char *argv[]){
  time_t start,end;
  time_t start_pix,end_pix;

  cout << "#####################################" << endl
       << "# Welcome to MALTA Noise Scan       #" << endl
       << "#####################################" << endl;

  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr     cAddress( 'a',"address","address","ipbus address. Default 192.168.0.1");
  CmdArgStr     cConfig(  'c',"config","config","configuration file");
  CmdArgStr     cMask(    'm',"mask" ,"mask", ".txt file for masking pixels");
  CmdArgStr     cOutdir(  'o',"output","output","output directory");
  CmdArgStr     cPath(    'f',"basefolder","basefolder","output basePath directory",CmdArg::isREQ);
  CmdArgInt     cVlow(    'l',"idblow" ,"idblow", "idblow value (5-255)",CmdArg::isREQ);
  CmdArgInt     cVhigh(   'h',"idbhigh" ,"idbhigh", "idbhigh value (5-255)",CmdArg::isREQ);
  CmdArgInt     cStep(    's',"idbstep" ,"idbstep", "step of idb scan (1-255)",CmdArg::isREQ);
  CmdArgBool    cDut( 'd',"dut","is DUT");
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files files");
  CmdArgBool    cQuietT(   'y',"quiteTime","do not write timestamped text files");
  CmdArgInt     cNdaq(    't',"time","time","time for the single repetition in us (should be above 2!!)",CmdArg::isREQ);
  CmdArgInt     cID(      'i', "id", "id", "ID is set with jumpers on board, range (0 - <number of chips -1>)");
  CmdArgInt     cRep(     'r',"repetition" ,"repetition", "number of repetitions",CmdArg::isREQ);
  CmdArgInt     cNpix(    'n',"npix","npix","number of printed noisy pixels (20 by default)");
  CmdArgStr     cTapFile(  'T',"tapfile","tapfile","tap calibration file");
  cout<< "parsing command line..."<< endl;
  CmdLine cmdl(*argv,&cVerbose,&cOutdir,&cPath,&cAddress,&cDut,&cVlow,&cVhigh,&cNdaq,&cRep,&cQuietT,&cQuiet,&cStep,&cConfig,&cMask,&cNpix,&cTapFile,&cID,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  cout<< "creating output directories..."<< endl;
  string address = (cAddress.flags() & CmdArg::GIVEN?cAddress:"udp://ep-ade-gw-07.cern.ch:50001");
  uint32_t chipID = (cID.flags() & CmdArg::GIVEN)?cID:0;
  //making output directory
  string ChipFolder;
  ChipFolder=cPath;
  string baseDir=getenv("MALTA_DATA_PATH");
  baseDir+="/Malta/Results_NoiseScan/"+ChipFolder+"/";
  string outdir = baseDir+(cOutdir.flags() & CmdArg::GIVEN?cOutdir:"Base");
  if (outdir!="") systemcall("mkdir -p "+outdir);

  bool noFile=( (cQuiet.flags() & CmdArg::GIVEN) ? cQuiet:false);
  bool noTimeFile=( (cQuietT.flags() & CmdArg::GIVEN) ? cQuietT:false);
  bool tapfromfile = ( (cTapFile.flags() & CmdArg::GIVEN) ? true:false);

  int nPixels=( (cNpix.flags() & CmdArg::GIVEN) ? cNpix:20);
  float scale = 1e-6;

   int vLow = cVlow;
   if(vLow < 5 || vLow > 127){
    cout<<"you gave me vlow = "<<vLow<<", please give valid cLow number!!!"<<endl;
    return 1;
  }

  int vHigh = cVhigh;
  if(vHigh < 0 || vHigh > 127 || vLow >= vHigh){
    cout<<"you gave me vHigh = "<<vHigh<<", please give valid cHigh number!!!"<<endl;
    return 1;
  }

  int vStep = cStep;
  if(vStep == 0 ) { //> (vHigh-vLow)){
    cout<<"you gave me vStep = "<<vStep<<", please give valid cStep number!!!"<<endl;
    return 1;
  } else {
    cout<<"vStep = "<<vStep<<endl;
  }

   if(cNdaq < 3){
    cout<<"you gave me time = "<<cNdaq*scale<<", please give time above 2.5 us!!"<<endl;
    return 1;
  }

  //////////////////////////////////////////////////////////////////////////////
  //instantiating stuff
  TFile *noise_sumf = new TFile( (outdir+"/noise_summary.root").c_str(), "RECREATE");
  noise_sumf->cd();

  TH2F* h2_base=new TH2F("Occupancy","Occupancy;x;y;counts",512,-0.5,511.5,512,-0.5,511.5);
  h2_base->SetDirectory(0);
  TH1F* h_base=new TH1F("base","base;IDB (DAC);Noise Rate ",100,vLow-vStep,vHigh+vStep);
  h_base->SetDirectory(0);
  vector<TGraphErrors*> v_plots;

  TH1F* h_time=new TH1F("time","Time of single repetition [ms];;Time",1,0.,1.);
  h_time->Fill(0., (double)cNdaq);
  TH1F* h_rep=new TH1F("nrep","Number of repetitions;;Repetitions",1,0.,1.);
  h_rep->Fill(0., (double)cRep);

  TGraphErrors* nhits_vs_idb =new TGraphErrors();
  nhits_vs_idb->SetName("hits_vs_idb"); nhits_vs_idb->SetTitle("Total number of hits vs IDB;IDB;Total number of hits;");
  nhits_vs_idb->SetMarkerStyle(20); nhits_vs_idb->SetMarkerSize(0.9); nhits_vs_idb->SetLineWidth(2);
  TGraphErrors* npix_vs_idb =new TGraphErrors();
  npix_vs_idb->SetName("npix_vs_idb"); npix_vs_idb->SetTitle("Number of pixels with >0 entries vs IDB;IDB;Number of pixels with >0 entries;");
  npix_vs_idb->SetMarkerStyle(20); npix_vs_idb->SetMarkerSize(0.9); npix_vs_idb->SetLineWidth(2);
  TGraphErrors* rate_vs_idb =new TGraphErrors();
  rate_vs_idb->SetName("rate_vs_idb"); rate_vs_idb->SetTitle("rate vs IDB;IDB;Rate [kHz];");
  rate_vs_idb->SetMarkerStyle(20); rate_vs_idb->SetMarkerSize(0.9); rate_vs_idb->SetLineWidth(2);


  TGraphErrors* nhits_vs_idb_l =new TGraphErrors();
  nhits_vs_idb_l->SetName("hits_vs_idb_l"); nhits_vs_idb_l->SetTitle("Total number of hits vs IDB;IDB;Total number of hits;");
  nhits_vs_idb_l->SetMarkerStyle(20); nhits_vs_idb_l->SetMarkerSize(0.9); nhits_vs_idb_l->SetLineWidth(2);
  TGraphErrors* npix_vs_idb_l =new TGraphErrors();
  npix_vs_idb_l->SetName("npix_vs_idb_l"); npix_vs_idb_l->SetTitle("Number of pixels with >0 entries vs IDB;IDB;Number of pixels with >0 entries;");
  npix_vs_idb_l->SetMarkerStyle(20); npix_vs_idb_l->SetMarkerSize(0.9); npix_vs_idb_l->SetLineWidth(2);
  TGraphErrors* rate_vs_idb_l =new TGraphErrors();
  rate_vs_idb_l->SetName("rate_vs_idb_l"); rate_vs_idb_l->SetTitle("rate vs IDB;IDB;Rate [kHz];");
  rate_vs_idb_l->SetMarkerStyle(20); rate_vs_idb_l->SetMarkerSize(0.9); rate_vs_idb_l->SetLineWidth(2);

  TGraphErrors* nhits_vs_idb_r =new TGraphErrors();
  nhits_vs_idb_r->SetName("hits_vs_idb_r"); nhits_vs_idb_r->SetTitle("Total number of hits vs IDB;IDB;Total number of hits;");
  nhits_vs_idb_r->SetMarkerStyle(20); nhits_vs_idb_r->SetMarkerSize(0.9); nhits_vs_idb_r->SetLineWidth(2);
  TGraphErrors* npix_vs_idb_r =new TGraphErrors();
  npix_vs_idb_r->SetName("npix_vs_idb_r"); npix_vs_idb_r->SetTitle("Number of pixels with >0 entries vs IDB;IDB;Number of pixels with >0 entries;");
  npix_vs_idb_r->SetMarkerStyle(20); npix_vs_idb_r->SetMarkerSize(0.9); npix_vs_idb_r->SetLineWidth(2);
  TGraphErrors* rate_vs_idb_r =new TGraphErrors();
  rate_vs_idb_r->SetName("rate_vs_idb_r"); rate_vs_idb_r->SetTitle("rate vs IDB;IDB;Rate [kHz];");
  rate_vs_idb_r->SetMarkerStyle(20); rate_vs_idb_r->SetMarkerSize(0.9); rate_vs_idb_r->SetLineWidth(2);

  TLine l1( 512/8, 0, 512/8, 512);
  TLine l2(512/8*2, 0,512/8*2, 512);
  TLine l3(512/8*3, 0,512/8*3, 512);
  TLine l4(512/8*4, 0,512/8*4, 512);
  TLine l5(512/8*5, 0,512/8*5, 512);
  TLine l6(512/8*6, 0,512/8*6, 512);
  TLine l7(512/8*7, 0,512/8*7, 512);

  map<int,TH1D*> noiseDistribution;
  map<int,TH1D*> rateDistribution;

  //Connect to MALTA
  std::cout << "before creating malta" <<std::endl;
  MaltaBase * malta = new MaltaBase(address);

  malta->SetDUT(cDut);
  if (tapfromfile) malta->ReadTapsFromFile(string (cTapFile));
  else malta->WriteConstDelays(4,1);
  malta->SetHalfRows(false);
  malta->SetExternalL1A(false);
  malta->Reset();
  malta->SetConfigMode(true,0.8);
  malta->ResetFifo();

  cout << "#####################################" << endl
       << "# Configure the chip                #" << endl
       << "#####################################" << endl;
  std::string line;
  malta->PreConfigMaltaC();
  std::vector<string> configText;
  if(cConfig.flags()&&CmdArg::GIVEN){
    ifstream fr;
    fr.open(string(cConfig).c_str());
    if (!fr.is_open()){
      cout << "#####################################################"<<endl;
      cout << "##  The configuration file cannot be opened        ##" << endl;
      cout << "Stopping script"<<endl;
      return 0;
    }
    configureChip (malta, string(cConfig), true);
    while (std::getline(fr, line)){
        configText.push_back(line);
      //  cout<<"Config line: "<<line <<endl;
    }
    //write the config to the output
    systemcall("cp "+ string(cConfig)+" "+outdir+"/" );
  }else{
    cout << "#####################################################"<<endl;
    cout << "## Using Default Configuration                     ##" << endl;
    cout << "#####################################################"<<endl;
    //default config
    cout  << "# Using  default configuration " << endl;
    uint32_t currentChipID = malta->GetCurrentChipID();
    for(auto id: malta->GetListOfChipIDs()){
      malta->SetSCIOToChip(id);
      malta->SetICASN(10,false);
      malta->SetIBIAS(80,false);
      malta->SetITHR(40,false);
      malta->SetIDB(110,false);
      malta->SetIRESET(30,false);
      malta->SetVCASN(64,false);
      malta->SetVRESET_P(45,false);
      malta->SetVRESET_D(65,false);
      malta->SetVPULSE_LOW(5,false);
      malta->SetVPULSE_HIGH(90,false);
      malta->SetVCLIP(127,false);
      malta->Send();

      configText.push_back("SC_ICASN: 10 \n");
      configText.push_back("SC_IBIAS: 80 \n");
      configText.push_back("SC_ITHR: 40 \n");
      configText.push_back("SC_IDB: 110 \n");
      configText.push_back("SC_IRESET: 30 \n");
      configText.push_back("SC_VCASN: 64 \n");
      configText.push_back("SC_VRESET_P: 45 \n");
      configText.push_back("SC_VRESET_D: 65 \n");
      configText.push_back("SC_VPULSE_LOW: 5 \n");
      configText.push_back("SC_VPULSE_HIGH: 90 \n");
      configText.push_back("SC_VCLIP: 127 \n");
    }
    malta->SetSCIOToChip(currentChipID);
  }


  if(cMask.flags()&&CmdArg::GIVEN){
    string maskTxt = cMask.flags() & CmdArg::GIVEN?cMask:"";
    malta->SetFullPixelMaskFromFile(maskTxt);
  }

  malta->Sync();

  malta->SetConfigMode(false);
  cout << "#####################################" << endl
       << "# Start IDB loop                   #" << endl
       << "#####################################" << endl;

  //IDB loop
  time(&start);//all pixel scanning clock start
  int count=-1;
  ostringstream thresh;
  ostringstream vacant;
  bool abort=false;

  cout << "==> Start IDB loop" << endl;
  for (int IDB=vHigh; IDB>=vLow; IDB-=vStep){
    //full_counter=0;
    count++;
    thresh.str("");
    thresh << "Occupancy_IDB_" << IDB;
    vacant.str("");
    vacant << "Vacancy_IDB_" << IDB;
    TH2F* the_h2= (TH2F*)h2_base->Clone(thresh.str().c_str());
    the_h2->SetDirectory(0);
    the_h2->Reset();

    TH2F* the_h2_l= (TH2F*)h2_base->Clone(thresh.str().c_str());
    the_h2_l->SetDirectory(0);
    the_h2_l->Reset();

    TH2F* the_h2_r= (TH2F*)h2_base->Clone(thresh.str().c_str());
    the_h2_r->SetDirectory(0);
    the_h2_r->Reset();
    //  the_h2->Write();
    TH2I* the_h2_0= (TH2I*)h2_base->Clone(vacant.str().c_str());
    the_h2_0->SetDirectory(0);
    the_h2_0->Reset();

    if(!g_cont){break;}
    time(&start_pix);//1pixel scanning clock start

    malta->Sync();
    //Lower voltage
    malta->SetConfigMode(true,0.8);
    cout << "Set IDB to " << IDB << endl;
    malta->SetIDB(IDB);
    malta->Send();
    sleep(1); //necessary?
    malta->SetConfigMode(false);

    cout << "==> Start repetition loop" << endl;
    float DAQwindow = 2500e-9;
    float maxrate = 1e3;
    float totTime = cNdaq*scale*cRep;
    int nTrigger=(int) ( (float)(cNdaq)*scale/DAQwindow );
    cout << "nTrigger = " << nTrigger<< endl;

    //Create ntuple

    ostringstream os;
    os << outdir  << "/noisescan_" << setw(3) << setfill('0') << IDB << ".root";
    string fname = os.str();
    cout << fname << endl;
    MaltaTree *ntuple = new MaltaTree();
    if (!noFile) ntuple->Open(fname,"RECREATE");
    std::cout << "*************"+fname<<std::endl;

    for( int r=0; r<cRep; r++) {
      //for(IDB=paramMin; IDB<=paramMax; IDB+=paramStp){
      int singleRep   = 0;
      if(!g_cont){break;}

      //Flush MALTA
      malta->ReadoutOff();
      malta->ResetFifo();
      malta->ResetL1Counter();

      uint32_t event=0;
      malta->Sync();
      malta->ReadoutOn();
      malta->Trigger(nTrigger,false);
      malta->ReadoutOff();
      MaltaData md,md2;
      uint32_t words[200];
      uint32_t numwords=200;
      uint32_t numwordsCurrent=0;
      for (uint32_t i=0;i<numwords;i+=2) {
        words[i+0]=0;
        words[i+1]=0;
      }
      bool isFirst=true;

      //chrono::time_point<chrono::system_clock> t0 = chrono::system_clock::now();

      int32_t nChips = malta->GetListOfChipIDs().size();
      //std::cout << "######################## N chips: " << nChips << std::endl;
      while(g_cont){
        malta->Sync();

	bool markDuplicate;

        malta->ReadFifoStatus();
        numwordsCurrent = malta->GetFIFO2WordCount();
        if(numwordsCurrent >= 2) numwordsCurrent -= 2;
      //  cout << "FIFO2 word count: " << numwordsCurrent << endl;
        if(numwordsCurrent > numwords) numwordsCurrent = numwords;

	malta->ReadMaltaWord(words,numwords);

	if (words[0]==0) break; //VD

	if (words[0]==0 && words[1]==0) {continue;}

	for (uint32_t i = 0; i < numwords; i+=2){
	  if(isFirst){
	    md.setWord1(words[i]);
	    md.setWord2(words[i+1]);
	    //check if word comes from the chosen chip, otherwise ignore
	    md.unpack();
	    uint32_t newChipID = md.getChipid();
	    if(nChips == 2){
	      newChipID = md.getDualChipid();
	    } else if(nChips == 4){
	      newChipID = md.getQuadChipid();
	    }
	    //std::cout << "New chip id: " << newChipID << " vs chipid: " << chipID << std::endl;
	    if(newChipID == chipID){
	      isFirst = false;
	      continue;
	    } else {
	      continue;
	    }
	  }
	  
	  //only read in words from the chosen chip
	  if(  nChips == 1 ||
	       (nChips == 2 && chipID == md.getDualChipid()) ||
	       (nChips == 4 && chipID == md.getQuadChipid())
	       ){
	    md2.setWord1(md.getWord1());
	    md2.setWord2(md.getWord2());
	    md.setWord1(words[i]);
	    md.setWord2(words[i+1]);
	  } else {
	    continue;
	  }
	  
	  md.unPack();
	  md2.unPack();
	  
	  if (!noFile) ntuple->Set(&md2);
	  
	  ntuple->SetRunNumber(r);
	  
	  uint32_t phase1 = md2.getPhase();
	  uint32_t winid1 = md2.getWinid();
	  uint32_t bcid1 = md2.getBcid();
	  uint32_t l1id1 = md2.getL1id();
	  
	  uint32_t phase2 = md.getPhase();
	  uint32_t winid2 = md.getWinid();
	  uint32_t bcid2 = md.getBcid();
	  uint32_t l1id2 = md.getL1id();
	  
	  bool markDuplicateNext = false;
	  bool isRealDuplicate   = false;
	  
	  
	  isRealDuplicate = markDuplicateNext;
	  // REWRITING DUPLICATION REOMVAL HERE
	  if( (bcid2-bcid1)==0 || (bcid2-bcid1)==1 || (bcid2==0 && bcid1==63)){
	    if( (winid2-winid1)==1 || (winid2==0 && winid1==7 )){
	      if( phase2==0 && (l1id1==l1id2)){
		if( phase1>=5) markDuplicate = true;
		else if(phase1>3){
		  markDuplicateNext = true;
		  markDuplicate = false;
		} else markDuplicate = false;
	      } else  markDuplicate = false;
	    } else markDuplicate = false;
	  } else  markDuplicate = false;
	  
	  if(isRealDuplicate==true){
	    markDuplicate = 1;
	    markDuplicateNext=false;
	  } else {
	    if(markDuplicate==true)  markDuplicate = 1;
	    else markDuplicate = 0;
	  }
	  
	  if (markDuplicate==false){
	    //from MD
	    for (unsigned h=0; h<md.getNhits(); h++) {
	      int LpixX=md2.getHitColumn(h);
	      int LpixY=md2.getHitRow(h);
	      /////////////cout << "      hit: " << h << " (" << LpixX << "," << LpixY << ")" << endl;
	      //sum number of hits
	      the_h2->Fill(LpixX,LpixY);
	      if (LpixX<256) the_h2_l->Fill(LpixX,LpixY);
	      if (LpixX>=256) the_h2_r->Fill(LpixX,LpixY);
	      singleRep++;//VD:?????
	    }
	  }
	  if (!noFile) ntuple->SetIsDuplicate(markDuplicate);
	  if (!noFile) ntuple->Fill();
	  event+=1;
	}
      }
      cout << "    Repetition: " << r << " has " << singleRep << " hits" << endl;
      if (singleRep > 50000) { //64000){
        cout<< "The FIFO is full, aborting scan"<<endl;
	abort=true;
        break;
      }
    } //end of repetition loop

    if (!noFile) ntuple->Close();
    delete ntuple;

    // fill hisotgrams
    cout << endl << "IDB: " << IDB << ":  " << the_h2->GetEntries() <<" hits, "<< (float)the_h2->GetEntries()/totTime/unit << " kHz rate"<< endl << endl;
    nhits_vs_idb->SetPoint(count,IDB,the_h2->GetEntries());
    nhits_vs_idb->SetPointError(count,0.0,sqrt(the_h2->GetEntries()));
    rate_vs_idb->SetPoint(count,IDB,(float)the_h2->GetEntries()/(float)totTime/unit );//totTime  //was 1e3
    rate_vs_idb->SetPointError( count,0.0,sqrt(the_h2->GetEntries())/(float)totTime/unit); //totTime //was 1e3

    nhits_vs_idb_l->SetPoint(count,IDB,the_h2_l->GetEntries());
    nhits_vs_idb_l->SetPointError(count,0.0,sqrt(the_h2_l->GetEntries()));
    rate_vs_idb_l->SetPoint(count,IDB,(float)the_h2_l->GetEntries()/(float)totTime/unit ); //totTime //was 1e3
    rate_vs_idb_l->SetPointError( count,0.0,sqrt(the_h2_l->GetEntries())/(float)totTime/unit);//totTime

    nhits_vs_idb_r->SetPoint(count,IDB,the_h2_r->GetEntries());
    nhits_vs_idb_r->SetPointError(count,0.0,sqrt(the_h2_r->GetEntries()));
    rate_vs_idb_r->SetPoint(count,IDB,(float)the_h2_r->GetEntries()/(float)totTime/unit );  //totTime//was 1e3
    rate_vs_idb_r->SetPointError( count,0.0,sqrt(the_h2_r->GetEntries())/(float)totTime/unit);//totTime


    rateDistribution[count] = new TH1D("rateDistribution_IDB"+IDB,"Single Pixel Noise;Rate (kHz);",100,0,  the_h2->GetEntries()/(float)totTime/unit);

    //float rate = 0;
    //float noisyPix = 0;
    std::vector<std::array<Int_t,3>> pixelHits, pixelHitsL, pixelHitsR;
    for (int x {0}; x < 512; x++){
      for (int y {0}; y < 512; y++){
	std::array<Int_t,3> pixel;
	pixel[0] = x;
        pixel[1] = y;
        pixel[2] = (Int_t)the_h2->GetBinContent(x+1,y+1);
        if(pixel[2] > 0) {
          pixelHits.push_back(pixel);
          if (x <  256) pixelHitsL.push_back(pixel);
          if (x >= 256) pixelHitsR.push_back(pixel);
        }
        rateDistribution[count]->Fill(pixel[2]/totTime/unit);
      }
    }

    // sort this pixels, noisiest first
    std::sort(pixelHits.begin(),pixelHits.end(),
          [](std::array<Int_t,3> p1, std::array<Int_t,3> p2){
		return p1[2]>p2[2];
	      }
	      );
    //make noise distribution
    noiseDistribution[count] = new TH1D("noiseDist"+IDB,"Number of hits in noisiest pixels; Rank; Entires",pixelHits.size(),0,pixelHits.size());
    Int_t bin = 1;
    for(auto p: pixelHits){
      noiseDistribution[count]->SetBinContent(bin,p[2]);
      ++bin;
    }

    string maskFileName = outdir+"/Mask_IDB"+to_string(IDB);
   // ofstream maskFile(maskFileName+".txt", ofstream::app); // append the list of pixels if
    systemcall("chmod 777 "+maskFileName+".txt");
    cout<<"chmod 777 "+maskFileName+".txt"<<endl;
    //create new file if file with the same name already exists



    ofstream maskFileNew(outdir+"/Mask_IDB"+to_string(IDB)+"_new.txt");
    ofstream spookyFileNew(outdir+"/SpookyMask_IDB"+to_string(IDB)+"_new.txt");
    //write n noisiest pixels to a mask file and printout
    if(cConfig.flags()&&CmdArg::GIVEN){
      for (int i{0}; i < int(configText.size() ); i++)
        maskFileNew<<configText.at(i)<<endl;
        //
    }
    maskFileNew<<endl;
    maskFileNew<<"#The noise rate of the entire chip is: " << (float)the_h2->GetEntries()/totTime/unit << " kHz" << endl;
    MaltaData testData;
    string testWord = "";



    for (int i = 0; i < min(nPixels, (int)pixelHits.size()); i++){
      if (((float)pixelHits[i][2]/totTime) >= maxrate/10. and not abort){  //rate has to be above 1 kHz
        //find the malta word based on the pixel coordinates, print this as along wirh rate and nhits
        testData.setHit(pixelHits[i][0],pixelHits[i][1]);
        testWord = testData.toString();
        maskFileNew<<"MASK_PIXEL: "<<pixelHits[i][0]<<", "<<pixelHits[i][1]<<" # entries " << pixelHits[i][2] << " rate " << pixelHits[i][2]/totTime/unit   << " kHz word \t"<<testWord << endl;
        spookyFileNew<<"MASK_PIXEL: "<<pixelHits[i][0]<<", "<<pixelHits[i][1]<<" # entries " << pixelHits[i][2] << " rate " << pixelHits[i][2]/totTime/unit   << " kHz word \t"<<testWord << endl;
        //cout<<"MASK_PIXEL: "<<pixelHits[i][0]<<", "<<pixelHits[i][1]<<" # entries " << pixelHits[i][2] << " rate " << pixelHits[i][2]/totTime/unit   << " kHz word \t"<<testWord << endl;
      }else{break;}
    }
    for (int i = 0; i < min(nPixels+10, (int)pixelHits.size()); i++){
      cout<<"MASK_PIXEL: "<<pixelHits[i][0]<<", "<<pixelHits[i][1]<<" # entries " << pixelHits[i][2] << " rate " << pixelHits[i][2]/totTime/unit   << " kHz word \t"<<testWord << endl;
    }
    maskFileNew.close();

     if (!noTimeFile){
       struct stat info;
       if( stat( (maskFileName+".txt").c_str(), &info ) == 0 ) {
         time_t tt = chrono::system_clock::to_time_t(chrono::system_clock::now());
         tm local_tm = *localtime(&tt);
         string year = to_string(local_tm.tm_year + 1900);
         string month = to_string(local_tm.tm_mon + 1);
         string day = to_string(local_tm.tm_mday);
         string hour = to_string(local_tm.tm_hour);
         string min =  to_string(local_tm.tm_min);
         string sec = to_string(local_tm.tm_sec);
         maskFileName = maskFileName+"_"+year+month+day+hour+min+sec;
         cout << year << month << day <<hour<<min<<sec<<endl;
         cout << maskFileName << endl;
       }
       if ( pixelHits.size()>0 ){
         ofstream maskFileNewT(maskFileName+".txt");
         if(cConfig.flags()&&CmdArg::GIVEN){
           for (int i{0}; i < int(configText.size() ); i++)
           maskFileNewT<<configText.at(i)<<endl;
         }
         maskFileNewT<<endl;
         maskFileNewT<<"#The noise rate of the entire chip is: " << (float)the_h2->GetEntries()/totTime/unit << " kHz" << endl;
         for (int i = 0; i < min(nPixels, (int)pixelHits.size()); i++){
           maskFileNewT<<"MASK_PIXEL: "<<pixelHits[i][0]<<", "<<pixelHits[i][1]<<" # entries " << pixelHits[i][2] << ", rate " << pixelHits[i][2]/totTime/unit   << " kHz word \t"<<testWord << endl;
         }
         maskFileNewT.close();
       }

    }

    if(pixelHits.size()>0){
      cout<<"Rate of noisiest= "<<(float)pixelHits[0][2]/(float)cRep/(float)cNdaq<<" kHz"<<endl;
      cout<<"Rate of "<<min(nPixels, (int)pixelHits.size())<<"th noisiest= "
          <<(float)pixelHits[min(nPixels, (int)pixelHits.size())-1][2]/(float)cRep/(float)cNdaq<<" kHz"<<endl;

    }

    npix_vs_idb->SetPoint( count, IDB, (int)pixelHits.size());
    npix_vs_idb->SetPointError( count, 0.0, sqrt( (float)pixelHits.size() ) );

    npix_vs_idb_l->SetPoint( count, IDB, (int)pixelHitsL.size());
    npix_vs_idb_l->SetPointError( count, 0.0, sqrt( (float)pixelHits.size() ) );

    npix_vs_idb_r->SetPoint( count, IDB, (int)pixelHitsR.size());
    npix_vs_idb_r->SetPointError( count, 0.0, sqrt( (float)pixelHits.size() ) );

    noise_sumf->cd();

    TCanvas *cNoise = new TCanvas("cNoise", "cNoise", 800, 600);
    noiseDistribution[count]->Draw("");
    cNoise->Print(  outdir+"/NoiseDistribution"+IDB+".pdf"  );
    cNoise->Write();
    //cNoise->Write("NoiseDistribution"+IDB);

    TCanvas *cRate = new TCanvas("cRate", "cRate", 800, 600);
    cRate->SetLogx(1);
    cRate->SetLogy(1);
    rateDistribution[count]->Draw("");
    string name = "rateDistribution_IDB_"+to_string(IDB);
    rateDistribution[count]->SetName(name.c_str());
    rateDistribution[count]->Write();
    cRate->Print( outdir+"/RateDistribution"+IDB+".pdf"  );
    cRate->Write();
  //  cRate->Write("RateDistribution"+IDB );

    gStyle->SetOptStat(11);
    gStyle->SetPalette(1);
    TCanvas* can=new TCanvas((thresh.str()+"_can").c_str(),thresh.str().c_str(),800,600);
    can->SetRightMargin(0.2);
    the_h2->SetMinimum(0);
    the_h2->Draw("COLZ");
    the_h2->Write();
    for(int i{0}; i<512;i++){
      for(int j{0}; j<512;j++){
         if (the_h2->GetBinContent(i,j) == 0){ the_h2_0->SetBinContent(i,j, 1);
         }else the_h2_0->SetBinContent(i,j, 0);
      }
    }

    the_h2_0->Write();
    l1.Draw(); l2.Draw(); l3.Draw(); l4.Draw(); l5.Draw(); l6.Draw(); l7.Draw();
    can->Update();
    can->Print( ( outdir+"/"+(thresh.str())+".pdf").c_str() );
    can->Write((thresh.str()+"_can").c_str() );

    // getting time information
    time(&end_pix);
  }// end of IDB loop


  TLegend *leg = new TLegend(0.65,0.25,0.85,0.45);
  leg->SetFillColor(0); leg->SetBorderSize(0); leg->SetTextSize(0.04); leg->SetTextFont(42);
  leg->AddEntry(nhits_vs_idb, "Full range", "lp");
  leg->AddEntry(nhits_vs_idb_l, "Left half", "lp");
  leg->AddEntry(nhits_vs_idb_r, "Right half", "lp");
  TCanvas* can2=new TCanvas("","",400,300);
  nhits_vs_idb->SetMinimum(10);
  can2->SetLogy(1);
  nhits_vs_idb->Draw("");
  nhits_vs_idb_l->SetMinimum(0.1);
  nhits_vs_idb_l->SetLineColor(4);
  nhits_vs_idb_l->SetMarkerColor(4);
  nhits_vs_idb_l->Draw("same&&lp");
  nhits_vs_idb_r->SetMinimum(0.1);
  nhits_vs_idb_r->SetLineColor(2);
  nhits_vs_idb_r->SetMarkerColor(2);
  nhits_vs_idb_r->Draw("same&&lp");
  leg->Draw();
  can2->Update();
  can2->Print( ( outdir+"/nhits_vs_idb.pdf").c_str() );
  can2->Write();

  TLegend *leg2 = new TLegend(0.65,0.25,0.85,0.45);
  leg2->SetFillColor(0); leg2->SetBorderSize(0); leg2->SetTextSize(0.04); leg2->SetTextFont(42);
  leg2->AddEntry(npix_vs_idb, "Full range", "lp");
  leg2->AddEntry(npix_vs_idb_l, "Left half", "lp");
  leg2->AddEntry(npix_vs_idb_r, "Right half", "lp");
  TCanvas* can3=new TCanvas("","",400,300);
  can3->Update();
  can3->SetLogy(0);
  npix_vs_idb->SetMinimum(0.1);
  npix_vs_idb->Draw("");
  npix_vs_idb_l->SetMinimum(0.1);
  npix_vs_idb_l->SetLineColor(4);
  npix_vs_idb_l->SetMarkerColor(4);
  npix_vs_idb_l->Draw("same&&lp");
  npix_vs_idb_r->SetMinimum(0.1);
  npix_vs_idb_r->SetLineColor(2);
  npix_vs_idb_r->SetMarkerColor(2);
  npix_vs_idb_r->Draw("same&&lp");
  leg2->Draw();
  can3->Update();
  can3->Print( ( outdir+"/npix_vs_idb.pdf").c_str() );
  can3->Write();

  TLegend *leg3 = new TLegend(0.65,0.25,0.85,0.45);
  leg3->SetFillColor(0); leg3->SetBorderSize(0); leg3->SetTextSize(0.04); leg3->SetTextFont(42);
  leg3->AddEntry(rate_vs_idb, "Full range", "lp");
  leg3->AddEntry(rate_vs_idb_l, "Left half", "lp");
  leg3->AddEntry(rate_vs_idb_r, "Right half", "lp");
  TCanvas* can4=new TCanvas("","",400,300);
  can4->SetLogy(1);
  rate_vs_idb->SetMinimum(1); //was 1e3
  rate_vs_idb->Draw("");
  rate_vs_idb_l->SetLineColor(4);
  rate_vs_idb_l->SetMarkerColor(4);
  rate_vs_idb_l->Draw("same&&lp");
  //rate_vs_idb_r->SetMinimum(0.1/(float)cRep/(float)cNdaq); //was 1e3
  rate_vs_idb_r->SetLineColor(2);
  rate_vs_idb_r->SetMarkerColor(2);
  rate_vs_idb_r->Draw("same&&lp");
  leg3->Draw();
  TLine l=TLine(vLow,1./(float)cRep/(float)cNdaq,vHigh,1./(float)cRep/(float)cNdaq); //was 1e3
  l.SetLineWidth(1);
  l.Draw("SAMEC");
  can4->Update();
  can4->Print( ( outdir+"/rate_vs_idb.pdf").c_str() );
  can4->Write();
  ///////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////
  time(&end);
  float Scantime=end-start;
  cout << "TOTAL scantime: " << Scantime << endl;
  TH2I* h2_maskedPixels = malta->GetMaskedPixels(0, 0, 512, 512);
  TH2I* h2_origMask = malta->GetPixelMask();
  h2_maskedPixels->SetName("MaskedPixGhosts");
  h2_maskedPixels->SetTitle("Masked pixels with ghosts;Pix X;Pix Y; Mask Layers");
  h2_origMask->SetName("MaskedPix");
  h2_origMask->SetTitle("Masked pixels with no ghosts;Pix X;Pix Y; Mask Layers");
  h2_origMask->SetMaximum(2);
  //write the ghosts+actual masks to file
  TCanvas* canMask = new TCanvas("canMask");
  h2_maskedPixels->Draw("colz");
  canMask->Write();
  h2_maskedPixels->Write();
  h2_origMask->Write();
  nhits_vs_idb->Write();
  npix_vs_idb->Write();
  rate_vs_idb->Write();
  nhits_vs_idb_l->Write();
  npix_vs_idb_l->Write();
  rate_vs_idb_l->Write();
  nhits_vs_idb_r->Write();
  npix_vs_idb_r->Write();
  rate_vs_idb_r->Write();
  noise_sumf->Close();

  cout << "Cleaning the house" << endl;
  malta->SetConfigMode(true,0.8);
  malta->SetIDB(100, true);
  //malta->Send();
  malta->SetConfigMode(false);

  delete noise_sumf;

  cout << "Have a nice day" << endl;
  return 0;
}
