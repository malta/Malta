#include "Malta/MaltaBase.h"
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <TH2.h>
#include <TFile.h>

using namespace std;

stringstream MaltaBase::systemcall(string cmd){
  ostringstream os;
  os << cmd << " >/tmp/systemcall.out";
  system(os.str().c_str());
  stringstream ss;
  ss << ifstream("/tmp/systemcall.out").rdbuf();
  return ss;
}

MaltaBase::MaltaBase(string address){

  m_command_names[SC_PULSE_WIDTH]="SC_PULSE_WIDTH";
  m_command_names[SC_ENABLE_MERGER]="SC_ENABLE_MERGER";
  m_command_names[SC_ENABLE_MON_DAC_CURRENT]="SC_ENABLE_MON_DAC_CURRENT";
  m_command_names[SC_ENABLE_MON_DAC_VOLTAGE]="SC_ENABLE_MON_DAC_VOLTAGE";
  m_command_names[SC_ENABLE_IDB]="SC_ENABLE_IDB";
  m_command_names[SC_ENABLE_ITHR]="SC_ENABLE_ITHR";
  m_command_names[SC_ENABLE_IBIAS]="SC_ENABLE_IBIAS";
  m_command_names[SC_ENABLE_IRESET]="SC_ENABLE_IRESET";
  m_command_names[SC_ENABLE_ICASN]="SC_ENABLE_ICASN";
  m_command_names[SC_ENABLE_VCASN]="SC_ENABLE_VCASN";
  m_command_names[SC_ENABLE_VCLIP]="SC_ENABLE_VCLIP";
  m_command_names[SC_ENABLE_VPULSE_HIGH]="SC_ENABLE_VPULSE_HIGH";
  m_command_names[SC_ENABLE_VPULSE_LOW]="SC_ENABLE_VPULSE_LOW";
  m_command_names[SC_ENABLE_VRESET_P]="SC_ENABLE_VRESET_P";
  m_command_names[SC_ENABLE_VRESET_D]="SC_ENABLE_VRESET_D";
  m_command_names[SC_CMOS_TOPLEFT_TO_TOPRIGHT]="SC_CMOS_TOPLEFT_TO_TOPRIGHT";
  m_command_names[SC_CMOS_BOTLEFT_TO_TOPRIGHT]="SC_CMOS_BOTLEFT_TO_TOPRIGHT";
  m_command_names[SC_CMOS_TOPRIGHT_TO_TOPLEFT]="SC_CMOS_TOPRIGHT_TO_TOPLEFT";
  m_command_names[SC_CMOS_BOTRIGHT_TO_TOPLEFT]="SC_CMOS_BOTRIGHT_TO_TOPLEFT";
  m_command_names[SC_CMOS_TOPLEFT_TO_BOTRIGHT]="SC_CMOS_TOPLEFT_TO_BOTRIGHT";
  m_command_names[SC_CMOS_BOTLEFT_TO_BOTRIGHT]="SC_CMOS_BOTLEFT_TO_BOTRIGHT";
  m_command_names[SC_CMOS_TOPRIGHT_TO_BOTLEFT]="SC_CMOS_TOPRIGHT_TO_BOTLEFT";
  m_command_names[SC_CMOS_BOTRIGHT_TO_BOTLEFT]="SC_CMOS_BOTRIGHT_TO_BOTLEFT";
  m_command_names[SC_LVDS_FROMBOTRIGHT]="SC_LVDS_FROMBOTRIGHT";
  m_command_names[SC_LVDS_FROMTOPRIGHT]="SC_LVDS_FROMTOPRIGHT";
  m_command_names[SC_LVDS_FROMBOTLEFT]="SC_LVDS_FROMBOTLEFT";
  m_command_names[SC_LVDS_FROMTOPLEFT]="SC_LVDS_FROMTOPLEFT";
  m_command_names[SC_MUTE_OUTPUT]="SC_MUTE_OUTPUT";
  m_command_names[SC_LINK_SC_ADDRESS]="SC_LINK_SC_ADDRESS";
  m_command_names[SC_SET_SC_ADDRESS]="SC_SET_SC_ADDRESS";
  m_command_names[SC_SET_ADDRESS]="SC_SET_ADDRESS";
  m_command_names[SC_IDB]="SC_IDB";
  m_command_names[SC_ITHR]="SC_ITHR";
  m_command_names[SC_IBIAS]="SC_IBIAS";
  m_command_names[SC_IRESET]="SC_IRESET";
  m_command_names[SC_ICASN]="SC_ICASN";
  m_command_names[SC_VCASN]="SC_VCASN";
  m_command_names[SC_VCLIP]="SC_VCLIP";
  m_command_names[SC_VPULSE_HIGH]="SC_VPULSE_HIGH";
  m_command_names[SC_VPULSE_LOW]="SC_VPULSE_LOW";
  m_command_names[SC_VRESET_P]="SC_VRESET_P";
  m_command_names[SC_VRESET_D]="SC_VRESET_D";
  m_command_names[SC_RESERVED_1]="SC_RESERVED_1";
  m_command_names[SC_RESERVED_2]="SC_RESERVED_2";
  m_command_names[SC_RESERVED_3]="SC_RESERVED_3";
  m_command_names[SC_RESERVED_4]="SC_RESERVED_4";
  m_command_names[SC_RESERVED_5]="SC_RESERVED_5";
  m_command_names[SC_PRECONFIG]="SC_PRECONFIG";
  m_command_names[MASK_DOUBLE_COLUMN]="MASK_DOUBLE_COLUMN";
  m_command_names[MASK_DOUBLE_COLUMNS]="MASK_DOUBLE_COLUMNS";
  m_command_names[MASK_PIXEL]="MASK_PIXEL";
  m_command_names[ROI]="ROI";
  m_command_names[MASK_PIXELS_FROM_TXT]="MASK_PIXELS_FROM_TXT";
  m_command_names[PULSE_PIXEL_ROW]="PULSE_PIXEL_ROW";
  m_command_names[PULSE_PIXEL_COLUMN]="PULSE_PIXEL_COLUMN";
  m_command_names[UNPULSE_PIXEL_ROW]="UNPULSE_PIXEL_ROW";
  m_command_names[UNPULSE_PIXEL_COLUMN]="UNPULSE_PIXEL_COLUMN";
  m_command_names[DUT]="DUT";
  m_command_names[TAP_FROM_TXT]="TAP_FROM_TXT";

  m_mask_row.resize(512,0);
  m_mask_col.resize(512,0);
  m_mask_diag.resize(512,0);
  m_mask_dc.resize(256,0);

  m_verbose=false;
  m_dut=false;
  m_fifoStatus = 0;
  m_configModeSleep = 200000;
  m_ipb = new ipbus::Uhal(address);
  m_msc = new MaltaSlowControl();
  GetVersion();

  //initiate chip 0 by default
  LinkChipToSCIO({0,4,5});
  SetSCIOToChip(0);
}

MaltaBase::~MaltaBase(){
  delete m_ipb;
  delete m_msc;
}

void MaltaBase::GetVersion() {
  uint32_t value;
  m_ipb->Read(0,value);
  cout << " CODE VERSION: " << value << endl;
}

void MaltaBase::SetVerbose(bool enable){
  //m_ipb->SetVerbose(enable);
  m_verbose=enable;
}

void MaltaBase::WriteConstDelays(uint32_t delay1, uint32_t delay2){
  for(uint32_t bit=0; bit<38; bit++){
    WriteTap(bit, delay1, delay2);
  }
}

void MaltaBase::WriteTap(uint32_t bit, uint32_t tap1, uint32_t tap2){
  if (bit<23 or (bit>26 and bit<34)) {
    cout << "MaltaBase::WriteTap "
	 << "bit: " << bit << "( " << TAP0_R+bit << ")"
	 << "tap1: " << tap1 << ", "
	 << "tap2: " << tap2 << endl;
  }
  uint32_t values[2];
  values[0]=((tap2&0x1F) << 5) | (tap1 & 0x1F);
  values[0]+=pow(2,31);
  values[1]=0;
  m_ipb->Write(TAP0+bit,values,2,true); //FIFO
}

vector<uint32_t> MaltaBase::ReadTap(uint32_t bit){
  if(bit>=NTAPS)bit=NTAPS-1;
  uint32_t value;
  m_ipb->Read(TAP0_R+bit,value);
  uint32_t tap1=((value>>0) & (0x1F));
  uint32_t tap2=((value>>5) & (0x1F));
  if (bit<23 or (bit>26 and bit<34)) {
    cout << "MaltaBase::ReadTap "
	 << "bit: " << bit << "( " << TAP0_R+bit << ")"
	 << "tap1: " << tap1 << ", "
	 << "tap2: " << tap2 << endl;
  }
  vector<uint32_t> ret;
  ret.push_back(tap1);
  ret.push_back(tap2);
  return ret;
}

void MaltaBase::ReadTapsFromFile(string fileName){
  //cout << "MaltaBase::ReadTapsFromFile " << endl;
  std::ifstream myfile(fileName);
  if(myfile.fail()){
    cout << "File: "<<fileName<< "does not exist, check the path, exit(0)!"<<endl;
    exit(0);
  }

  int bit, tap1, tap2;
  while ( myfile >> bit >> tap1 >> tap2){
    WriteTap(bit,tap1,tap2);
  }
  myfile.close();
}

void MaltaBase::WriteTapsToFile(string fileName){
  cout << "MaltaBase::WriteTapsToFile " << endl;
  ofstream myfile(fileName,ofstream::trunc);
  if(myfile.fail()){
    cout << "Cannot open file for writing: "<<fileName<<endl;
    return;
  }

  for(uint32_t bit=0;bit<NTAPS;bit++){
    vector<uint32_t> taps=ReadTap(bit);
    if(taps.size()==0) continue;
    myfile << bit << "\t" << taps.at(0) << "\t" << taps.at(1) << endl;
  }
  myfile.close();
}

void MaltaBase::Reset(){
  uint32_t mask= 0xFFFFFFFF ^ (1<<12);
  uint32_t cacheWord;
  m_ipb->Read(8,cacheWord);
  m_ipb->Write(8, cacheWord | (1<<12) );
  usleep(50000); // 20ms maybe longer will be better
  m_ipb->Write(8, cacheWord & mask );
}

void MaltaBase::ResetFifo(){
  uint32_t cacheWord;
  m_ipb->Read(0x8,cacheWord);
  vector<uint32_t> commands;
  uint32_t maskFIFO   =0xFFFFFFFF ^ (1<<2);
  uint32_t maskOSERDES=0xFFFFFFFF ^ (1<<3);
  commands.push_back(cacheWord | (1<<2) );
  commands.push_back(cacheWord & maskFIFO );
  commands.push_back(cacheWord | (1<<3) );
  commands.push_back(cacheWord & maskOSERDES );
  m_ipb->Write(CONTROL,commands,true);
}

void MaltaBase::SetHalfColumns(bool enable){
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  if(enable) cacheWord |= (1<<30);
  else cacheWord &= (~(1<<30));
  m_ipb->Write(CONTROL, cacheWord);
  usleep(2000);
}

void MaltaBase::SetReadoutDelay(uint32_t delay){
  if(delay > 255) delay = 255;
  m_ipb->Write(RODELAY,delay);
}

uint32_t MaltaBase::GetReadoutDelay(){
  uint32_t delay = 0;
  m_ipb->Read(RODELAY,delay);
  return delay;
}

void MaltaBase::SetHalfRows(bool enable){
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  if(enable) cacheWord |= (1<<31);
  else cacheWord &= (~(1<<31));
  m_ipb->Write(CONTROL, cacheWord);
  usleep(2000);
}

void MaltaBase::SetExternalL1A(bool enable){
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  if(enable) cacheWord |= (1<<18);
  else cacheWord &= (~(1<<18));
  m_ipb->Write(CONTROL, cacheWord);
  usleep(2000);
}

void MaltaBase::ReadoutEnableExternalL1A(){
  SetExternalL1A(true);
}

void MaltaBase::ReadoutDisableExternalL1A(){
  SetExternalL1A(false);
}

void MaltaBase::ResetL1Counter(){
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  m_ipb->Write(CONTROL, cacheWord | (1<<16) );
  usleep(2000);
  uint32_t maskRESET= 0xFFFFFFFF ^ (1<<16);
  m_ipb->Write(CONTROL, cacheWord & maskRESET );
  usleep(2000);
}

void MaltaBase::ReadoutOn(){
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  uint32_t mask=0xFFFFFFFF ^ ( 1<<29 );
  m_ipb->Write(CONTROL, cacheWord & mask);
}

void MaltaBase::ReadoutOff(){
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  m_ipb->Write(CONTROL, cacheWord | ( 1<<29 ));
  usleep(2000);
}

void MaltaBase::SetMaxROWindow(int val){
  int newVal=val;
  if (val>127) newVal=127;
  newVal=newVal/2;
  uint32_t newInput=newVal & 0x3F;
  uint32_t cacheWord;
  m_ipb->Read(DELAY,cacheWord);
  uint32_t newCache=cacheWord & 0xFFFFFFC0;
  m_ipb->Write(DELAY, newCache+newInput);
}

void MaltaBase::DisableFastSignal(){
  uint32_t cacheWord;
  m_ipb->Read(DELAY,cacheWord);
  uint32_t mask=0xFFFFFFFF ^ ( 1<<6 );
  m_ipb->Write(DELAY, cacheWord & mask);
}

void MaltaBase::EnableFastSignal(){
  uint32_t cacheWord;
  m_ipb->Read(DELAY,cacheWord);
  usleep(1000);
  //m_ipb->Write(DELAY, cacheWord | ( 1<<6 ));
  m_ipb->Write(DELAY, ( 1<<6 ) );
  usleep(1000);
  uint32_t cacheWord2;
  m_ipb->Read(DELAY,cacheWord2);
  cout << " ENABLE FAST: before: " << std::hex << cacheWord
       << " | after: " << cacheWord2 << std::dec << endl;

}

void MaltaBase::Sync(){
  m_ipb->Sync();
}

bool MaltaBase::IsFifo1Full(){
  return (m_fifoStatus & FIFO1_FULL)!=0;
}

bool MaltaBase::IsFifo1Empty(){
  return (m_fifoStatus & FIFO1_EMPTY)==0;
}

bool MaltaBase::IsFifo1Half(){
  return (m_fifoStatus & FIFO1_HALF)==0;
}

bool MaltaBase::IsFifo2Full(){
  return (m_fifoStatus & FIFO2_FULL)!=0;
}

bool MaltaBase::IsFifo2Empty(){
  return (m_fifoStatus & FIFO2_EMPTY)==0;
}

bool MaltaBase::IsFifoMonEmpty(){
  return (m_fifoStatus & FIFOM_EMPTY)==0;
}

uint32_t MaltaBase::GetL1ID(){
  uint32_t tmpStatus = m_fifoStatus;
  uint32_t mask = 0x00000FFF;
  return ( (tmpStatus>>20) & mask);
}

void MaltaBase::ReadMaltaWord(uint32_t * values, uint32_t numwords){
  m_ipb->Read(DATA,values,numwords,true);
}

void MaltaBase::ReadMonitorWord(uint32_t * values) {
  m_ipb->Read(7,values,37,true);
}

uint32_t MaltaBase::ReadFifoStatus(){
  m_ipb->Read(FIFOSTATUS, m_fifoStatus);
  return m_fifoStatus;
}

void MaltaBase::Trigger(uint32_t ntimes, bool withPulse){
  uint32_t value;
  m_ipb->Read(CONTROL,value);

  vector<uint32_t> commands;
  if (withPulse) {
    //this is nasty as fuck, but I don't have time to debug!!!
    /*
    commands.push_back(1572864);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(1048576);
    commands.push_back(0);
    */
    commands.push_back(CTRL_PULSE | CTRL_INCTRIG);
    //for(uint32_t i=0;i<13;i++){commands.push_back((uint32_t)CTRL_PULSE);}
    for(uint32_t i=0;i<20;i++){commands.push_back((uint32_t)CTRL_PULSE);}
    commands.push_back(0);
    /*
    for (unsigned int c=0; c<commands.size(); c++) {
      cout << commands.at(c) << " , ";
    }
    */
    ///cout << endl;
  } else {
    commands.push_back( value | CTRL_INCTRIG);
    for (unsigned int i=0; i<20; i++) commands.push_back( (value) & ~CTRL_INCTRIG);

    for (unsigned int i=0; i<4; i++) {
       commands.push_back( value | CTRL_INCTRIG);
       for (unsigned int j=0; j<20; j++) commands.push_back( (value) & ~CTRL_INCTRIG);
    }
  }

  for (unsigned int iC=0; iC<ntimes; iC++) {
    //usleep(50000);
    m_ipb->Write(CONTROL,commands,true);
  }

}

void MaltaBase::SetPixelPulse(uint32_t row, uint32_t column, bool enable){
  vector<uint32_t> commands;
  if(enable){
    commands.push_back(m_msc->enablePulsePixelCol(column));
    commands.push_back(m_msc->enablePulsePixelHor(row));
  }else{
    commands.push_back(m_msc->disablePulsePixelCol(column));
    commands.push_back(m_msc->disablePulsePixelHor(row));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);
}


void MaltaBase::SetPixelMaskRow(uint32_t row, bool enable){
  m_mask_row[row]=enable;
  vector<uint32_t> commands;
  if(enable){
    commands.push_back(m_msc->maskPixelHor(row));
  }else{
    commands.push_back(m_msc->unmaskPixelHor(row));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);
}

void MaltaBase::SetPixelMaskColumn(uint32_t column, bool enable){
  m_mask_col[column]=enable;
  vector<uint32_t> commands;
  if(enable){
    commands.push_back(m_msc->maskPixelCol(column));
  }else{
    commands.push_back(m_msc->unmaskPixelCol(column));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);
}

void MaltaBase::SetPixelMaskDiag(uint32_t diag, bool enable){
  m_mask_diag[diag]=enable;
  vector<uint32_t> commands;
  if(enable){
    commands.push_back(m_msc->maskPixelDiag(diag));
  }else{
    commands.push_back(m_msc->unmaskPixelDiag(diag));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);
}

void MaltaBase::SetPixelPulseRow(uint32_t row, bool enable){
  vector<uint32_t> commands;
  if(enable){
    commands.push_back(m_msc->enablePulsePixelHor(row));
  }else{
    commands.push_back(m_msc->disablePulsePixelHor(row));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);
}

void MaltaBase::SetPixelPulseColumn(uint32_t column, bool enable){
  vector<uint32_t> commands;
  if(enable){
    commands.push_back(m_msc->enablePulsePixelCol(column));
  }else{
    commands.push_back(m_msc->disablePulsePixelCol(column));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);
}

void MaltaBase::SetPixelMask(uint32_t column, uint32_t row, bool mask){
  uint32_t diag=m_msc->getDiagonal(column,row);
  m_mask_row[row]=mask;
  m_mask_col[column]=mask;
  m_mask_diag[diag]=mask;

  vector<uint32_t> commands = m_msc->maskPixel(column,row,mask);
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(1000);

  std::pair<uint32_t,uint32_t> pixel(column,row);
  m_maskedPixelList.push_back(pixel);
}

void MaltaBase::SetDoubleColumnMask(uint32_t dc, bool mask){
  m_mask_dc[dc]=mask;
  vector<uint32_t> commands;
  if(mask){
    commands.push_back(m_msc->maskDoubleColumn(dc));
  }else{
    commands.push_back(m_msc->unmaskDoubleColumn(dc));
  }
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  m_ipb->Write(SLOWCONTROL_W,commands,true);
  usleep(10000);
}

void MaltaBase::SetDoubleColumnMaskRange(uint32_t dc1, uint32_t dc2, bool mask){
  if(dc1>=dc2){
    cout << "MaltaBase::SetDoubleColumnMaskRange error: dc1:" << dc1 << ", dc2:" << dc2 << endl;
    return;
  }
  if(dc1>255) dc1=255;
  if(dc2>255) dc2=255;
  for(uint32_t dc=dc1;dc<=dc2;dc++){
    SetDoubleColumnMask(dc,mask);
  }
}

void MaltaBase::SetDoubleColumnMaskRange(vector<uint32_t> dcs, bool mask){
  if(dcs.size()<2){
    cout << "MaltaBase::SetDoubleMaskRange requires 2 parameters: dc1, dc2" << endl;
    return;
  }
  SetDoubleColumnMaskRange(dcs[0],dcs[1],mask);
}

void MaltaBase::SetFullPixelMaskFromFile(std::string fileName){

  fileName.erase( fileName.find_last_not_of(" \f\n\r\t\v")+1);
  fileName.erase(0, fileName.find_first_not_of(" \f\n\r\t\v"));

  std::vector<int> xpair, ypair;

  //reading from file; check if root or txt file exists
  if(fileName.find(".txt")!=string::npos){
    int ppx, ppy;
    std::ifstream myfile(fileName);
    if(myfile.fail()){
      cout << "File: "<<fileName<< "does not exist, check the path"<<endl;
      return;
    }

    while ( myfile >> ppx >> ppy){
      xpair.push_back(ppx);
      ypair.push_back(ppy);
    }
    myfile.close();
    cout << "Masking from TXT file; Total number of masked pixels is: " << xpair.size() << endl;
  }

  else if(fileName.find(".root")!=string::npos){
    TFile fr(fileName.c_str(), "READ");
    if(fr.IsZombie()){
      cout << "root file is corrupted or does not exist" << endl;
      return;
    }
    std::string noisemap="NoiseMap";
    if(!fr.GetListOfKeys()->Contains(noisemap.c_str())){
      cout << "histogram with name "<<noisemap<<" does not exists in the root file" << endl;
      return;
    }
    TH2I* h2 = (TH2I*)fr.Get(noisemap.c_str());

    for(int32_t i=0;i<h2->GetNbinsX();i++){
      for(int32_t j=0;j<h2->GetNbinsY();j++){
        uint32_t binn=h2->GetBin(i,j);
        if(h2->GetBinContent(binn)>0){
          xpair.push_back(i);
          ypair.push_back(j);
        }
      }
    }
    cout << "Masking from root file; Total number of masked pixels is: " << xpair.size() << endl;
  }

  else{
    cout << "Unrecognized file extension..." << endl;
    return;
  }


  if(xpair.size() != ypair.size()){
    cout << "Size of the passed pixel arrays for masking do not match!!!!" <<endl;
    return;
  }

  // actual masking
  for(uint32_t p=0; p<xpair.size();p++){
    int col = xpair.at(p);
    int row = ypair.at(p);
    int diag = m_msc->getDiagonal(col,row);
    cout << "  [ " << col << " , " << row << " ]  , diag=" << diag << endl;

    vector<uint32_t> commands = m_msc->maskPixel(col,row,true);
    m_ipb->Write(SLOWCONTROL_W,commands,true);
    m_ipb->Write(SLOWCONTROL_W,commands,true);
    usleep(2000);

    std::pair<uint32_t,uint32_t> pixel(col,row);
    m_maskedPixelList.push_back(pixel);
  }
}

void MaltaBase::SetROI(vector<uint32_t> &coords, bool mask){
  if(coords.size()<4){
    cout << "MaltaBase::SetROI requires 4 parameters: x1, y1, x2, y2" << endl;
    return;
  }
  SetROI(coords[0],coords[1],coords[2],coords[3],mask);
}

void MaltaBase::SetROI(uint32_t col1, uint32_t row1, uint32_t col2, uint32_t row2, bool mask){
  cout << "MaltaBase::SetROI " << col1 << ", " << row1 << ", " << col2 << ", " << row2 << endl;
  if(col2<=col1){
    cout << "MaltaBase::SetROI range error:" << " x1:" << col1 << " x2:" << col2 << endl;
    return;
  }
  if(row2<=row1){
    cout << "MaltaBase::SetROI range error:" << " y1:" << row1 << " y2:" << row2 << endl;
    return;
  }
  if(col1>511) col1=511;
  if(col2>511) col2=511;
  if(row1>511) row1=511;
  if(row2>511) row2=511;
  uint32_t dc1 = (col1>1)?(col1>>1):0;
  uint32_t dc2 = (col2>1)?(col2>>1):0;

  cout << "MaltaBase::SetROI masking double columns from " << 0 << " to " << ((dc1>0)?(dc1-1):0) << endl;
  //Mask double columns from 0 to dc1
  if(dc1>0) SetDoubleColumnMaskRange(0,dc1-1,mask);
  cout << "MaltaBase::SetROI masking double columns from " << ((dc2<255)?(dc2+1):255) << " to " << 255 << endl;
  //Mask double columns from dc2 to 255
  if(dc2<255) SetDoubleColumnMaskRange(dc2+1,255,mask);

  //VD last: I would prefer to mask ALL the columns!
  cout << "MaltaBase::SetROI masking columns from " << col1 << " to " << col2 << endl;
  //Mask col from col1 to col2
  for(uint32_t col=col1; col<=col2; col++){
    if(mask) m_commands.push_back(m_msc->maskPixelCol(col));
    else     m_commands.push_back(m_msc->unmaskPixelCol(col));
  }

  cout << "MaltaBase::SetROI masking rows from " << 0 << " to " << row1 << endl;
  //Mask row from 0 to row1
  for(uint32_t row=0; row<row1; row++){
    if(mask) m_commands.push_back(m_msc->maskPixelHor(row));
    else     m_commands.push_back(m_msc->unmaskPixelHor(row));
  }
  cout << "MaltaBase::SetROI masking rows from " << (row2+1) << " to " << 511 << endl;
  //Mask row from row2 to 511
  for(uint32_t row=row2+1; row<=511; row++){
    if(mask) m_commands.push_back(m_msc->maskPixelHor(row));
    else     m_commands.push_back(m_msc->unmaskPixelHor(row));
  }
  cout << "MaltaBase::SetROI masking all diagonals " << 0 << " to " << 511 << endl;
  //Mask diag from 0 to 511
  for(uint32_t diag=0; diag<=511; diag++){
    if(mask) m_commands.push_back(m_msc->maskPixelDiag(diag));
    else     m_commands.push_back(m_msc->unmaskPixelDiag(diag));
  }

  //Send all slow control commands
  Send();
  //cout << "SENT" << endl;
}

void MaltaBase::SetConfigModeSleep(uint32_t microseconds){
  m_configModeSleep=microseconds;
}

void MaltaBase::SetConfigMode(bool enable, float dvdd){
  if(enable){
    ostringstream os;
    if(m_dut){
      systemcall("MALTA_PSU.py -t D_ANAG_OFF.txt ");
      os << "MALTA_PSU.py -c setVoltage DUT_DVDD " << fixed << setprecision(2) << dvdd;
      systemcall(os.str());
    }else{
      os << "MALTA_PSU.py -c setVoltage DVDD " << fixed << setprecision(2) << dvdd; //VDLAST
      systemcall(os.str());                                                         //VDLAST
      systemcall("MALTA_PSU.py -t T_ANAG_OFF.txt ");                                //VDLAST
      //systemcall("MALTA_PSU.py -t T_CONFIG_ON.txt ");
    }
    SetClock(true);
    usleep(m_configModeSleep);
    usleep(1000);
  }else{
    usleep(m_configModeSleep);
    SetClock(false);
    usleep(1000);
    if(m_dut){
      systemcall("MALTA_PSU.py -c setVoltage DUT_DVDD 1.8 ");
      systemcall("MALTA_PSU.py -t D_ANAG_ON.txt ");
    }else{
      systemcall("MALTA_PSU.py -c setVoltage DVDD 1.8 "); //VDLAST
      systemcall("MALTA_PSU.py -t T_ANAG_ON.txt ");       //VDLAST
      //systemcall("MALTA_PSU.py -t T_CONFIG_OFF.txt ");
    }
  }
}

void MaltaBase::SetClock(bool enable) {
  uint32_t cacheWord;
  m_ipb->Read(CONTROL,cacheWord);
  //cout << "MaltaBase::SetClock CONTROL 0x" << hex << cacheWord << dec << endl;
  if (enable) {
    cacheWord |= (1<<21);
  } else {
    cacheWord &= ~(1<<21);
  }
  //cout << "MaltaBase::SetClock CONTROL 0x" << hex << cacheWord << dec << endl;
  m_ipb->Write(CONTROL, cacheWord );
  usleep(5000);
}

void MaltaBase::SetConfigFromString(const std::string& str){

  if(m_verbose) cout << "Parse: " << str << endl;

  vector<std::string> tokens;
  //std::string delimiters = " :=\n\r,;.-";
  std::string delimiters = ":;=\n\r";

  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos){
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }

  for(uint i=0; i<tokens.size();i+=2){
    try{
      string command=tokens.at(i);
      uint32_t value=0;
      std::string value_s;
      vector<uint32_t> values;
      // check if reg value is provided in hex format
      if(tokens.at(i+1).find("0x") != std::string::npos){
	value = std::stoi(tokens.at(i+1), 0, 16);
      }else if(  tokens.at(i+1).find("true") != string::npos ||
		 tokens.at(i+1).find("True") != string::npos ||
		 tokens.at(i+1).find("TRUE") != string::npos
		 ){
	value = 1;
      }else if(  tokens.at(i+1).find("false") != string::npos ||
		 tokens.at(i+1).find("False") != string::npos ||
		 tokens.at(i+1).find("FALSE") != string::npos
		 ){
	value = 0;
      }else if(tokens.at(i+1).find(",") != string::npos ){
	istringstream ss(tokens.at(i+1));
	string token;
	while(getline(ss, token, ',')){
	  value = std::stoi(token);
	  values.push_back(value);
	}
      }else if(tokens.at(i+1).find("txt") != string::npos){
        value_s = tokens.at(i+1);
	value_s.erase(value_s.find_last_not_of(" \f\n\r\t\v") + 1 );
	value_s.erase(0, value_s.find_first_not_of(" \f\n\r\t\v"));
      }else{
	value = std::stoi(tokens.at(i+1));
      }

      if(m_verbose){
	cout << "Command: " << command << endl;
	cout << "Value: " << value;
	for(uint32_t i=0;i<values.size();i++){
	  cout << " " << values.at(i);
	}
	cout << endl;
      }

      if     (command==m_command_names[SC_ENABLE_IDB])            { EnableIDB(value); }
      else if(command==m_command_names[SC_ENABLE_MERGER])         { EnableMerger(value); }
      else if(command==m_command_names[SC_ENABLE_MON_DAC_CURRENT]){ EnableMonDacCurrent(value); }
      else if(command==m_command_names[SC_ENABLE_MON_DAC_VOLTAGE]){ EnableMonDacVoltage(value); }
      else if(command==m_command_names[SC_ENABLE_ITHR])           { EnableITHR(value); }
      else if(command==m_command_names[SC_ENABLE_IBIAS])          { EnableIBIAS(value); }
      else if(command==m_command_names[SC_ENABLE_IRESET])         { EnableIRESET(value); }
      else if(command==m_command_names[SC_ENABLE_ICASN])          { EnableICASN(value); }
      else if(command==m_command_names[SC_ENABLE_VCASN])          { EnableVCASN(value); }
      else if(command==m_command_names[SC_ENABLE_VCLIP])          { EnableVCLIP(value); }
      else if(command==m_command_names[SC_ENABLE_VPULSE_HIGH])    { EnableVPULSE_HIGH(value); }
      else if(command==m_command_names[SC_ENABLE_VPULSE_LOW])     { EnableVPULSE_LOW(value); }
      else if(command==m_command_names[SC_ENABLE_VRESET_D])       { EnableVRESET_D(value); }
      else if(command==m_command_names[SC_ENABLE_VRESET_P])       { EnableVRESET_P(value); }
      else if(command==m_command_names[SC_CMOS_TOPLEFT_TO_TOPRIGHT]){ SetTransmitTopLeftToTopRight(value); }
      else if(command==m_command_names[SC_CMOS_BOTLEFT_TO_TOPRIGHT]){ SetTransmitBotLeftToTopRight(value); }
      else if(command==m_command_names[SC_CMOS_TOPRIGHT_TO_TOPLEFT]){ SetTransmitTopRightToTopLeft(value); }
      else if(command==m_command_names[SC_CMOS_BOTRIGHT_TO_TOPLEFT]){ SetTransmitBotRightToTopLeft(value); }
      else if(command==m_command_names[SC_CMOS_TOPLEFT_TO_BOTRIGHT]){ SetTransmitTopLeftToBotRight(value); }
      else if(command==m_command_names[SC_CMOS_BOTLEFT_TO_BOTRIGHT]){ SetTransmitBotLeftToBotRight(value); }
      else if(command==m_command_names[SC_CMOS_TOPRIGHT_TO_BOTLEFT]){ SetTransmitTopRightToBotLeft(value); }
      else if(command==m_command_names[SC_CMOS_BOTRIGHT_TO_BOTLEFT]){ SetTransmitBotRightToBotLeft(value); }
      else if(command==m_command_names[SC_LVDS_FROMBOTRIGHT])       { SetTransmitFromBotRightToLVDS(value); }
      else if(command==m_command_names[SC_LVDS_FROMTOPRIGHT])       { SetTransmitFromTopRightToLVDS(value); }
      else if(command==m_command_names[SC_LVDS_FROMBOTLEFT])        { SetTransmitFromBotLeftToLVDS(value); }
      else if(command==m_command_names[SC_LVDS_FROMTOPLEFT])        { SetTransmitFromTopLeftToLVDS(value); }
      else if(command==m_command_names[SC_MUTE_OUTPUT])             { SetMute(value); }
      else if(command==m_command_names[SC_LINK_SC_ADDRESS])         { LinkChipToSCIO(values); }
      else if(command==m_command_names[SC_SET_SC_ADDRESS])          { SetSCIOToChip(value); }
      else if(command==m_command_names[SC_SET_ADDRESS])             { SetSlowControlIO(values); }
      else if(command==m_command_names[SC_IDB])                   { SetIDB(value); }
      else if(command==m_command_names[SC_ITHR])                  { SetITHR(value); }
      else if(command==m_command_names[SC_IBIAS])                 { SetIBIAS(value); }
      else if(command==m_command_names[SC_IRESET])                { SetIRESET(value); }
      else if(command==m_command_names[SC_ICASN])                 { SetICASN(value); }
      else if(command==m_command_names[SC_VCASN])                 { SetVCASN(value); }
      else if(command==m_command_names[SC_VCLIP])                 { SetVCLIP(value); }
      else if(command==m_command_names[SC_VPULSE_HIGH])           { SetVPULSE_HIGH(value); }
      else if(command==m_command_names[SC_VPULSE_LOW])            { SetVPULSE_LOW(value); }
      else if(command==m_command_names[SC_VRESET_D])              { SetVRESET_D(value); }
      else if(command==m_command_names[SC_VRESET_P])              { SetVRESET_P(value); }
      else if(command==m_command_names[SC_RESERVED_1])            { SetReservedRegister(0,value); }
      else if(command==m_command_names[SC_RESERVED_2])            { SetReservedRegister(1,value); }
      else if(command==m_command_names[SC_RESERVED_3])            { SetReservedRegister(2,value); }
      else if(command==m_command_names[SC_RESERVED_4])            { SetReservedRegister(3,value); }
      else if(command==m_command_names[SC_RESERVED_5])            { SetReservedRegister(4,value); }
      else if(command==m_command_names[SC_PULSE_WIDTH])           { SetPulseWidth(value);}
      else if(command==m_command_names[SC_PRECONFIG])               { PreConfigMaltaC(); }
      else if(command==m_command_names[MASK_DOUBLE_COLUMN])       { SetDoubleColumnMask(value,true);}
      else if(command==m_command_names[MASK_DOUBLE_COLUMNS])      { SetDoubleColumnMaskRange(values,true);}
      else if(command==m_command_names[MASK_PIXEL])               { SetPixelMask(values[0],values[1],true);}
      else if(command==m_command_names[ROI])                      { SetROI(values,true);}
      else if(command==m_command_names[MASK_PIXELS_FROM_TXT])     { SetFullPixelMaskFromFile(value_s);}
      else if(command==m_command_names[PULSE_PIXEL_ROW])          { SetPixelPulseRow(value,true);}
      else if(command==m_command_names[PULSE_PIXEL_COLUMN])       { SetPixelPulseColumn(value,true);}
      else if(command==m_command_names[UNPULSE_PIXEL_ROW])        { SetPixelPulseRow(value,false);}
      else if(command==m_command_names[UNPULSE_PIXEL_COLUMN])     { SetPixelPulseColumn(value,false);}
      else if(command==m_command_names[DUT])                      { SetDUT(value);}
      else if(command==m_command_names[TAP_FROM_TXT])             { ReadTapsFromFile(value_s);}
      else{ cout << "Command not found: " << command << endl; }

    }catch(std::invalid_argument& ia){
      cout << "Malta::SetConfigFromTextFile: "
	   << "Unable to parse [" << tokens.at(i) << " " << tokens.at(i+1) << "]" << endl;
    }
  }

  //Send to the FPGA
  Send();
}

void MaltaBase::SetConfigFromFile(const std::string& fileName){

  ifstream fr;
  cout << "MaltaBase::SetConfigFromFile " << fileName << endl;
  fr.open(fileName.c_str());
  if (!fr.is_open()){
    cout << "#####################################################"<<endl;
    cout <<      "##  The configuration file cannot be opened        ##" << endl;
    cout << "#####################################################"<<endl;
  }else{
    while (fr.good()){

      string buffer;
      getline(fr, buffer);

      // Ignore if is a comment
      size_t end = buffer.find_first_of('#');
      if (end==0) continue;

      // Ignore if line is empty
      if(buffer.length()==0) continue;

      // Configure MALTA
      SetConfigFromString(buffer);
    }
  }
  fr.close();
}

uint32_t MaltaBase::GetFIFO2WordCount(){
  uint32_t count = 0;
  m_ipb->Read(WORDCOUNT,count);
  if (count%2!=0 and count >0) count = count-1;
  return count;
}

void MaltaBase::Send(){
  if(m_commands.size()==0) return;
  uint32_t reply;
  //if ( m_commands.size()>10 ) cout << "Number of commands to send: " << m_commands.size() << endl;
  //m_verbose=true;
  for(uint32_t i=0;i<m_commands.size();i++){
    //Leyre and Roberto says: "we need to send each command twice"
    m_ipb->Write(SLOWCONTROL_W,m_commands.at(i));
    m_ipb->Write(SLOWCONTROL_W,m_commands.at(i));
    usleep(1000); //VD was 10ms
    if(m_verbose){
      m_ipb->Read(SLOWCONTROL_R,reply);
      cout << "MaltaBase::Send Commamd: "
	   << hex
	   << "0x" << setw(8) << setfill('0') << m_commands[i]
	   << " == "
	   << "0x" << setw(8) << setfill('0') << reply
	   << dec << endl;
    }
  }
  m_commands.clear();

  //cout << "MaltaBase::Send Register: 0x" << hex << SLOWCONTROL_W << " write value: 0x" << hex << m_msc->readRegister(21) << dec << endl;
  m_ipb->Write(SLOWCONTROL_W, m_msc->readRegister(21));
  m_ipb->Read(SLOWCONTROL_R, reply);
  ////cout << endl;
  if ( reply!=0x80000004 ) cout << "MaltaBase::Send Register: 0x" << hex << SLOWCONTROL_R << " read  value: 0x" << reply << dec << endl;
  ////cout << endl;
}

void MaltaBase::EnableMerger(bool enable, bool force){
  m_commands.push_back(m_msc->enableMerger(enable));
  if(force) Send();
}

void MaltaBase::EnableMonDacCurrent(bool enable, bool force){
  m_commands.push_back(m_msc->switchDACMONI(enable));
  if(force) Send();
}

void MaltaBase::EnableMonDacVoltage(bool enable, bool force){
  m_commands.push_back(m_msc->switchDACMONV(enable));
  if(force) Send();
}

void MaltaBase::EnableIDB(bool enable, bool force){
  m_commands.push_back(m_msc->switchIDB(enable));
  if(force) Send();
}

void MaltaBase::EnableITHR(bool enable, bool force){
  m_commands.push_back(m_msc->switchITHR(enable));
  if(force) Send();
}

void MaltaBase::EnableIBIAS(bool enable, bool force){
  m_commands.push_back(m_msc->switchIBIAS(enable));
  if(force) Send();
}

void MaltaBase::EnableIRESET(bool enable, bool force){
  m_commands.push_back(m_msc->switchIRESET(enable));
  if(force) Send();
}

void MaltaBase::EnableICASN(bool enable, bool force){
  m_commands.push_back(m_msc->switchICASN(enable));
  if(force) Send();
}

void MaltaBase::EnableVCASN(bool enable, bool force){
  m_commands.push_back(m_msc->switchVCASN(enable));
  if(force) Send();
}

void MaltaBase::EnableVCLIP(bool enable, bool force){
  m_commands.push_back(m_msc->switchVCLIP(enable));
  if(force) Send();
}

void MaltaBase::EnableVPULSE_HIGH(bool enable, bool force){
  m_commands.push_back(m_msc->switchVPULSE_HIGH(enable));
  if(force) Send();
}

void MaltaBase::EnableVPULSE_LOW(bool enable, bool force){
  m_commands.push_back(m_msc->switchVPULSE_LOW(enable));
  if(force) Send();
}

void MaltaBase::EnableVRESET_D(bool enable, bool force){
  m_commands.push_back(m_msc->switchVRESET_D(enable));
  if(force) Send();
}

void MaltaBase::EnableVRESET_P(bool enable, bool force){
  m_commands.push_back(m_msc->switchVRESET_P(enable));
  if(force) Send();
}

void MaltaBase::SetIDB(uint32_t value, bool force){
  m_commands.push_back(m_msc->setIDB(value));
  if(force) Send();
}

void MaltaBase::SetITHR(uint32_t value, bool force){
  m_commands.push_back(m_msc->setITHR(value));
  if(force) Send();
}

void MaltaBase::SetIBIAS(uint32_t value, bool force){
  m_commands.push_back(m_msc->setIBIAS(value));
  if(force) Send();
}

void MaltaBase::SetIRESET(uint32_t value, bool force){
  m_commands.push_back(m_msc->setIRESET(value));
  if(force) Send();
}

void MaltaBase::SetICASN(uint32_t value, bool force){
  m_commands.push_back(m_msc->setICASN(value));
  if(force) Send();
}

void MaltaBase::SetVCASN(uint32_t value, bool force){
  m_commands.push_back(m_msc->setVCASN(value));
  if(force) Send();
}

void MaltaBase::SetVCLIP(uint32_t value, bool force){
  m_commands.push_back(m_msc->setVCLIP(value));
  if(force) Send();
}

void MaltaBase::SetVPULSE_HIGH(uint32_t value, bool force){
  m_commands.push_back(m_msc->setVPULSE_HIGH(value));
  if(force) Send();
}

void MaltaBase::SetVPULSE_LOW(uint32_t value, bool force){
  m_commands.push_back(m_msc->setVPULSE_LOW(value));
  if(force) Send();
}

void MaltaBase::SetVRESET_D(uint32_t value, bool force){
  m_commands.push_back(m_msc->setVRESET_D(value));
  if(force) Send();
}

void MaltaBase::SetVRESET_P(uint32_t value, bool force){
  m_commands.push_back(m_msc->setVRESET_P(value));
  if(force) Send();
}

void MaltaBase::SetPulseWidth(uint32_t value, bool force){
  m_commands.push_back(m_msc->delay(value));
  if(force) Send();
}

TH2I* MaltaBase::GetPixelMask(){
  TH2I * m_pixelMask = new TH2I("pixelmask","pixelmask",512,-0.5,511.5,512,-0.5,511.5);
  for(auto pix: m_maskedPixelList){
    if(m_pixelMask->GetBinContent(pix.first+1,pix.second+1) == 0){
      m_pixelMask->Fill(pix.first,pix.second);
    }
  }
  return m_pixelMask;
}

TH1D* MaltaBase::GetDColMask(){
  TH1D * m_dcMask = new TH1D("dcolmask","dcolmask",256,0,256);

  for(uint32_t dc=0;dc<256;dc++){
    if(m_mask_dc[dc]){m_dcMask->Fill(dc);}
  }

  return m_dcMask;
}

TH2I* MaltaBase::GetMaskedPixels(int roiXmin, int roiYmin, int roiXmax, int roiYmax){

  TH2I * m_maskedPixels = new TH2I("maskedPixels","maskedPixels",512,-0.5,511.5,512,-0.5,511.5);

  std::vector<int> maskedCols, maskedRows, maskedDiags;

  if(!(roiXmin == 0 && roiXmax == 0 && roiYmin == 0 && roiYmax == 0)){
    //mask double columns outright
    for(int i = 0; i < roiXmin; i+=2){
      for(int j = 0; j < 511; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
    }
    for(int i = roiXmax; i < 511; i+=2){
      for(int j = 0; j < 511; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
    }

    for(int i = 0; i < 511; i+=2){
      for(int j = 0; j < roiYmin; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
      for(int j = roiYmax; j < 511; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
    }
   }

  for(auto pix: m_maskedPixelList){
    int diag = (pix.second - pix.first)%512;
    //int diag = m_msc->getDiagonal( pix.first, pix.second);
    if  (std::find(maskedCols.begin(),maskedCols.end(),pix.first) == maskedCols.end()){
      for(int i = 0; i < 512; ++i) m_maskedPixels->Fill(pix.first, i);
      maskedCols.push_back(pix.first);
    }
    if (std::find(maskedRows.begin(),maskedRows.end(),pix.second) == maskedRows.end()){
      for(int i = 0; i < 512; ++i) m_maskedPixels->Fill(i, pix.second);
      maskedRows.push_back(pix.second);
    }
    if (std::find(maskedDiags.begin(),maskedDiags.end(),diag) == maskedDiags.end() ){
      for(int j = 0; j < 512; ++j)    m_maskedPixels->Fill(j,(j+diag)%512);
      maskedDiags.push_back(diag);
    }
  }

  Int_t nMaskedPixels;
  Double_t nMaskedPixelsFrac = 0.0;
  if(!(roiXmin == 0 && roiXmax == 0 && roiYmin == 0 && roiYmax == 0)){
    nMaskedPixels = (Int_t)m_maskedPixels->Integral(roiXmin,roiXmax,roiYmin,roiYmax);
    nMaskedPixelsFrac = (Double_t)nMaskedPixels/((roiXmax-roiXmin)*(roiYmax-roiYmin))*100.0;
  } else {
    nMaskedPixels = (Int_t)m_maskedPixels->Integral();
    nMaskedPixelsFrac = (Double_t)nMaskedPixels/(512.*512.)*100.0;
  }

  for(int i = 0; i < 513; ++i){
    for(int j = 0; j < 513; ++j){
      if (m_maskedPixels->GetBinContent(i,j) >= 3) {m_maskedPixels->SetBinContent(i,j,1);
      }else m_maskedPixels->SetBinContent(i,j,0);
    }
  }
  cout<< "The total number of masked pixels is : "<< m_maskedPixels->Integral()<<endl;
  m_maskedPixels->SetMaximum(3);
  m_maskedPixels->SetTitle(("Masked Pixels (N = "+std::to_string(m_maskedPixels->Integral())+" = "+std::to_string(nMaskedPixelsFrac).substr(0,5)+"%); pixX; pixY").c_str());
  return m_maskedPixels;
}

void MaltaBase::SetDUT(bool isdut){
  m_dut=isdut;
}


void MaltaBase::PreConfigMaltaC(){

  //Default for all MALTA versions
  EnableMerger(false,true);//false
  SetPulseWidth(500,true);
  //SetPulseWidth(750,true);
  //SetPulseWidth(1000,true);
  //SetPulseWidth(2000,true);
  EnableMonDacCurrent(false,true);
  EnableMonDacVoltage(false,true);   /// keep it false -> boh?
  EnableIDB(false,true);
  EnableITHR(false,true);
  EnableIRESET(false,true);
  EnableICASN(false,true);
  EnableVCASN(false,true);
  EnableVCLIP(false,true);
  EnableVPULSE_HIGH(false,true);
  EnableVPULSE_LOW(false,true);
  EnableVRESET_P(false,true);
  EnableVRESET_D(false,true);

}

void MaltaBase::SetReservedRegister(uint32_t reg, uint32_t value){
  if(reg>4) return;
  uint32_t command=m_msc->writeReservedRegister(reg,value);
  m_ipb->Write(SLOWCONTROL_W,command);
  uint32_t reply;
  m_ipb->Read(SLOWCONTROL_R,reply);
}

uint32_t MaltaBase::GetReservedRegister(uint32_t reg){
  if(reg>4) return 0;
  uint32_t command=m_msc->readRegister(reg+12);
  m_ipb->Write(SLOWCONTROL_W,command);
  uint32_t reply;
  m_ipb->Read(SLOWCONTROL_R,reply);
  return (0xFFFF&reply);
}

uint32_t MaltaBase::GetRegister(uint32_t reg){
  if(reg>23) return 0;
  uint32_t command=m_msc->readRegister(reg);
  m_ipb->Write(SLOWCONTROL_W,command);
  uint32_t reply;
  m_ipb->Read(SLOWCONTROL_R,reply);
  return (0xFFFF&reply);
}

void MaltaBase::SetTimeout(uint32_t ms){
  m_ipb->SetTimeout(ms);
}

/*
Methods for multi module integration

In order to talk to each chip it is necessary to be able to change the slow control
register address.

Chips can receive data from either top or bottom CMOS on either the left or right side.
Chips can send data to either top or bottom CMOS on either left or right side or to the LVDS.
*/

void MaltaBase::SetSlowControlIO(std::vector<uint32_t> values){
  if(values.size() != 2){
    std::cout << "MaltaBase::SetSlowControlIO : "
              << "argument vector does not have correct size of 2!" << std::endl;
    std::cout << "Passed arguments: " << std::endl;
    for(auto val: values) std::cout << val << "   " << std::endl;
    std::cout << "Ignoring command..." << std::endl;
  } else {
    SLOWCONTROL_R = values[0];
    SLOWCONTROL_W = values[1];
  }
}

void MaltaBase::SetSCIOToChip(uint32_t chipID){
  if(m_chipID_to_SCIO.find(chipID) == m_chipID_to_SCIO.end()){
    std::cout << "ChipID " << chipID << " not recognized!" << std::endl;
    std::cout << "Ignoring command..." << std::endl;
  } else {
    std::cout << "Talk to chip " << chipID << std::endl;
    std::cout << "R: " << m_chipID_to_SCIO[chipID].first << ", W: " << m_chipID_to_SCIO[chipID].second << std::endl;
    m_currentChipID = chipID;
    SLOWCONTROL_R = m_chipID_to_SCIO[chipID].first;
    SLOWCONTROL_W = m_chipID_to_SCIO[chipID].second;
  }
}

void MaltaBase::LinkChipToSCIO(std::vector<uint32_t> values){
  if(values.size() != 3){
    std::cout << "MaltaBase::LinkChipToSCIO : "
              << "argument vector does not have correct size of 3!" << std::endl;
    std::cout << "Passed arguments: " << std::endl;
    for(auto val: values) std::cout << val << "   " << std::endl;
    std::cout << "Ignoring command..." << std::endl;
  } else {
    if(std::find(m_chipIDs.begin(),m_chipIDs.end(),values[0]) == m_chipIDs.end()){
      m_chipIDs.push_back(values[0]);
      m_chipID_to_SCIO[values[0]] = std::make_pair(values[1],values[2]);
    }
  }
}

std::vector<uint32_t> MaltaBase::GetListOfChipIDs(){
  return m_chipIDs;
}

uint32_t MaltaBase::GetCurrentChipID(){
  return m_currentChipID;
}

void MaltaBase::SetTransmitTopLeftToTopRight(bool force){
  std::cout << "Configure chip to transmit from top left to top right CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(true));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(true));
  m_commands.push_back(m_msc->enableLeftCMOSTop(true)); //changed from true based on python config script
  m_commands.push_back(m_msc->enableRightCMOSTop(true));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(true));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitBotLeftToTopRight(bool force){
  std::cout << "Configure chip to transmit from bottom left to top right CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(true));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(true));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(true));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(true));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitTopLeftToBotRight(bool force){
  std::cout << "Configure chip to transmit from top left to bottom right CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(true));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(true));
  m_commands.push_back(m_msc->enableLeftCMOSTop(true));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(true));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitBotLeftToBotRight(bool force){
  std::cout << "Configure chip to transmit from bottom left to bottom right CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(true));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(true));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(true));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitTopRightToTopLeft(bool force){
  std::cout << "Configure chip to transmit from top right to top left CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(true));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(true));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(true));
  m_commands.push_back(m_msc->enableRightCMOSTop(true));
  m_commands.push_back(m_msc->enableCMOSLeft(true));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitBotRightToTopLeft(bool force){
  std::cout << "Configure chip to transmit from bottom right to top left CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(true));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(true));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(true));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(true));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitTopRightToBotLeft(bool force){
  std::cout << "Configure chip to transmit from top right to bottom left CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(true));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(true));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(true));
  m_commands.push_back(m_msc->enableCMOSLeft(true));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitBotRightToBotLeft(bool force){
  std::cout << "Configure chip to transmit from bottom right to bottom left CMOS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(true));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(true));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(true));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitFromTopRightToLVDS(bool force){
  std::cout << "Configure chip to transmit from top right to LVDS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(true));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(true));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(true));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(true));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitFromBotRightToLVDS(bool force){
  std::cout << "Configure chip to transmit from bottom right to LVDS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(true));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(true));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(true));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitFromTopLeftToLVDS(bool force){
  std::cout << "Configure chip to transmit from top left to LVDS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(true));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(true));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(true)); //changed from true based on python config script
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(true));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetTransmitFromBotLeftToLVDS(bool force){
  std::cout << "Configure chip to transmit from bottom left to LVDS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(true));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(true));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(true));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}

void MaltaBase::SetMute(bool force){
  std::cout << "Configure chip to transmit from bottom left to LVDS." << std::endl;
  m_commands.push_back(m_msc->enableMergerToLeft(false));
  m_commands.push_back(m_msc->enableMergerToRight(false));
  m_commands.push_back(m_msc->enableLeftMergerToLVDS(false));
  m_commands.push_back(m_msc->enableLeftMergerToCMOS(false));
  m_commands.push_back(m_msc->enableRightMergerToLVDS(false));
  m_commands.push_back(m_msc->enableRightMergerToCMOS(false));
  m_commands.push_back(m_msc->enableLeftCMOSTop(false));
  m_commands.push_back(m_msc->enableRightCMOSTop(false));
  m_commands.push_back(m_msc->enableCMOSLeft(false));
  m_commands.push_back(m_msc->enableCMOSRight(false));
  m_commands.push_back(m_msc->enableDataflowLVDS(false));
  m_commands.push_back(m_msc->enableMerger(false));
  if(force) Send();
}
