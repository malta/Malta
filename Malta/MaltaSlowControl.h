#ifndef MALTASLOWCONTROL_H
#define MALTASLOWCONTROL_H "MaltaSlowControl.hh"

#include <iostream>
#include <string>
#include <vector>

/**
 * This class provides a user friendly access to the MALTA Slow Control commands.
 * THIS IS NOT THE SLOW CONTROL SOFTWARE. 
 * The member functions of this class merely generate the 16-bit patterns to be 
 * sent to the slow control using the dedicated ipbus library.
 * 
 * @brief Tool to encode and decode MALTA slow control commands
 * @author Enrico Jr. Schioppa, CERN 
 * @date 2018-02-01
 **/


class MaltaSlowControl{
public:

  ////////////////////////////
  // constructors / destructor
  ////////////////////////////

  /**
   * @brief Constructor
   */
  MaltaSlowControl();

  /**
   * @brief Destructor
   */
  ~MaltaSlowControl();

  //////////////////
  // word generators
  //////////////////

  // reset

  /**
     All the slow control registers are reset to their default values. This happens synchronously, i.e. with the rising edge of the slow control clock.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int resetCMU() const;

  /**
     A pulse active low of programmable duration is generated. This pulse is used to reset the logic outside the slow control.
     @param nClockCycles length of the pulse in clock cycles. Ranges from 0 to 1023.     
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int generateReset(const unsigned int nClockCycles) const;

  /**
     A synchronous reset is applied to a specific register. Registers with ID ranging from 0 to 22 can be readout.
     @param registerID target register. Ranges from 0 to 39. WARNING: values outside this range may result in uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int resetSCLRRegister(const unsigned int registerID) const;

  // mask

  /**
     Sets the column address of the pixel to be masked (analog masking: the signal is not propagated through the matrix). WARNING: in order to actually mask a target pixel, its full address (column, row, diagonal) must specified
     @param col column address of the target pixel. Ranges from 0 to 511. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int maskPixelCol(const unsigned int col) const;

  /**
     Sets the row address of the pixel to be masked (analog masking: the signal is not propagated through the matrix). WARNING: in order to actually mask a target pixel, its full address (column, row, diagonal) must specified
     @param hor row address of the target pixel. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int maskPixelHor(const unsigned int hor) const;

  /**
     Sets the diagonal address of the pixel to be masked (analog masking: the signal is not propagated through the matrix). WARNING: in order to actually mask a target pixel, its full address (column, row, diagonal) must specified
     @param diag diagonal address of the target pixel. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int maskPixelDiag(const unsigned int diag) const;

  /**
     Sets the column address of the pixel to be unmasked (analog masking: the signal is not propagated through the matrix). WARNING: to avoid inconsistencies, it is good practice to specify the full address (column, row, diagonal) of the target pixel to be unmasked
     @param col column address of the target pixel. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int unmaskPixelCol(const unsigned int col) const;

  /**
     Sets the row address of the pixel to be unmasked (analog masking: the signal is not propagated through the matrix). WARNING: to avoid inconsistencies, it is good practice to specify the full address (column, row, diagonal) of the target pixel to be unmasked
     @param hor row address of the target pixel. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int unmaskPixelHor(const unsigned int hor) const;

  /**
     Sets the diagonal address of the pixel to be unmasked (analog masking: the signal is not propagated through the matrix). WARNING: to avoid inconsistencies, it is good practice to specify the full address (column, row, diagonal) of the target pixel to be unmasked
     @param diag diagonal address of the target pixel. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int unmaskPixelDiag(const unsigned int diag) const;

  /**
     Sets the address of the double column to be masked (digital masking: the signal is not propagated through the periphery).
     @param doubleColumn address of the target double column. Ranges from 0 to 255. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int maskDoubleColumn(const unsigned int doubleColumn) const;

  /**
     Sets the address of the double column to be unmasked (digital masking: the signal is not propagated through the periphery).
     @param doubleColumn address of the target double column. Ranges from 0 to 255. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int unmaskDoubleColumn(const unsigned int doubleColumn) const;

  // pulse

  /**
     Sets the column address of the pixel to be pulsed. Pulsing is not effective until an external source is applied. WARNING: in order to actually pulse a target pixel, its full address (column, row) must specified.
     @param col column address of the target pixel. Ranges from 0 to 511. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enablePulsePixelCol(const unsigned int col) const;

  /**
     Sets the row address of the pixel to be pulsed. Pulsing is not effective until an external source is applied. WARNING: in order to actually pulse a target pixel, its full address (column, row) must specified.
     @param hor row address of the target pixel. Ranges from 0 to 511. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enablePulsePixelHor(const unsigned int hor) const;

  /**
     Sets the column address of the pixel for which pulsing must be disabled. WARNING: to avoid inconsistencies, it is good practice to specify the full address (column, row) of the target pixel.
     @param col column address of the target pixel. Ranges from 0 to 511. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int disablePulsePixelCol(const unsigned int col) const;

  /**
     Sets the row address of the pixel for which pulsing must be disabled. WARNING: to avoid inconsistencies, it is good practice to specify the full address (column, row) of the target pixel.
     @param hor row address of the target pixel. Ranges from 0 to 511. WARNING: values outside this range may produce an uncontrolled address.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int disablePulsePixelHor(const unsigned int hor) const;

  /**
     Enables pixels (0,5), (251,5) and (511,5) for pulsing. This option is useful when communication problems are found, in order to have a minimal operative point for pulsing. These pixels have been chosen in order to guarantee non-overlapping signals (neither in the matrix, nor in the periphery) to propagate to the chip output. Pulsing is not effective until an external source is applied.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enablePulseFixedPattern() const;

  /**
     Generates a vector of four words to be used to disable pixels (0,5), (251,5) and (511,5) for pulsing: disablePulsePixelCol(5), disablePulsePixelHor(511), disablePulsePixelHor(251), disablePulsePixelHor(0). These words can be sent to the slow control in a loop to disable pulsing of these pixels.
     @return vector of four integer numbers corresponding to the 16-bit slow control commands
   */
  std::vector<unsigned int> disablePulseFixedPattern() const;

  // delay

  /**
     Sets the pulse width of the digital signals.
     @param picoseconds pulse width in picoseconds. Four values are possible: 500, 750, 1000, 2000. WARNING: values different from these will generate a warning message and may result in uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int delay(const unsigned int picoseconds) const;

  // DAC

  /**
     Sets the DAC value for VCASN.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setVCASN(const unsigned int value) const;

  /**
     Sets the DAC value for VCLIP.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setVCLIP(const unsigned int value) const;

  /**
     Sets the DAC value for VPLSE_HIGH.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setVPULSE_HIGH(const unsigned int value) const;

  /**
     Sets the DAC value for VPLSE_LOW.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setVPULSE_LOW(const unsigned int value) const;

  /**
     Sets the DAC value for VRESET_P.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setVRESET_P(const unsigned int value) const;

  /**
     Sets the DAC value for VRESET_D.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setVRESET_D(const unsigned int value) const;

  /**
     Sets the DAC value for ICASN.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setICASN(const unsigned int value) const;

  /**
     Sets the DAC value for IRESET.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIRESET(const unsigned int value) const;

  /**
     Sets the DAC value for ITHR.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setITHR(const unsigned int value) const;

  /**
     Sets the DAC value for IBIAS.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIBIAS(const unsigned int value) const;

  /**
     Sets the DAC value for IDB.
     @param value DAC value. Ranges from 0 to 127. WARNING: values outside this range may produce uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIDB(const unsigned int value) const;

  /**
     Configures the IBUFP_MON0 current to drive the monitoring pixels.
     @param value integer value corresponding, in binary, to the pixel pattern to be enabled. Ranges from 0 to 15. WARNING: values outside this range may produce an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIBUFP_MON0(const unsigned int value) const;

  /**
     Configures the IBUFN_MON0 current to drive the monitoring pixels.
     @param value integer value corresponding, in binary, to the pixel pattern to be enabled. Ranges from 0 to 15. WARNING: values outside this range may produce an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIBUFN_MON0(const unsigned int value) const;

  /**
     Configures the IBUFP_MON1 current to drive the monitoring pixels.
     @param value integer value corresponding, in binary, to the pixel pattern to be enabled. Ranges from 0 to 15. WARNING: values outside this range may produce an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIBUFP_MON1(const unsigned int value) const;

  /**
     Configures the IBUFN_MON1 current to drive the monitoring pixels.
     @param value integer value corresponding, in binary, to the pixel pattern to be enabled. Ranges from 0 to 15. WARNING: values outside this range may produce an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIBUFN_MON1(const unsigned int value) const;

  /**
     Enables/disabled the DAC monitoring switch for DACMONI.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchDACMONI(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for DACMONV.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchDACMONV(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for IRESET_BIT.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int setIRESET_BIT(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for IREF.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchIREF(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for IDB.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchIDB(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for ITHR.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchITHR(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for IBIAS.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchIBIAS(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for IRSET.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchIRESET(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for ICASN.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchICASN(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for VRESET_D.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchVRESET_D(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for VRESET_P.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchVRESET_P(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for VPLSE_LOW.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchVPULSE_LOW(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for VPLSE_HIGH.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchVPULSE_HIGH(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for VCLIP.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchVCLIP(const bool on) const;

  /**
     Enables/disabled the DAC monitoring switch for VCASN.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int switchVCASN(const bool on) const;

  // LVDS

  /**
     Enables/disables the LVDS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableLVDS(const bool on) const;

  /**
     Selects the LVDS pre-emphasis current sources to enable/disable. The 16 current sources can be set in three groups: 5 down to 0, 10 down to 6, 15 down to 11.
     @param type group identifier. Three possible values are accepted: "5=>0", "10=>6" or "15=>11". WARNING: different strings will produce a warning and may result in uncontrolled behavior.
     @param value integer value corresponding, in binary, to the source pattern to be enabled/disabled for the specified group. Ranges from 0 to 63 in the case of "5=>0". Ranges from 0 to 31 in the case of "10=>6" or "15=>11". WARNING: different values may result in an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configPreEmphasis(const std::string type,
                                 const unsigned int value) const;

  /**
     Selects the LVDS H-bridges to enable/disable.
     @param value integer value corresponding, in binary, to the H-bridge pattern to be enabled/disabled. Ranges from 0 to 31. WARNING: different values may result in an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configHBridge(const unsigned int value) const;


  /**
     Selects the LVDS common mode feedback units to enable/disable.
     @param value integer value corresponding, in binary, to the common mode feedback pattern to be enabled/disabled. Ranges from 0 to 31. WARNING: different values may result in an uncontrolled pattern.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configCMFB(const unsigned int value) const;

  /**
     Selects the LVDS IBCMFB current units to enable/disable.
     @param value integer value corresponding, in binary, to the IBCMFB pattern to be enabled/disabled. Ranges from 0 to 15. WARNING: different values may result in an uncontrolled pattern.     
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configIBCMFB(const unsigned int value) const;

  /**
     Selects the LVDS IVPH current units to enable/disable.
     @param value integer value corresponding, in binary, to the IVPH pattern to be enabled/disabled. Ranges from 0 to 15. WARNING: different values may result in an uncontrolled pattern.     
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configIVPH(const unsigned int value) const;

  /**
     Selects the LVDS IVPL current units to enable/disable.
     @param value integer value corresponding, in binary, to the IVPL pattern to be enabled/disabled. Ranges from 0 to 15. WARNING: different values may result in an uncontrolled pattern.     
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configIVPL(const unsigned int value) const;

  /**
     Selects the LVDS IVNH current units to enable/disable.
     @param value integer value corresponding, in binary, to the IVNH pattern to be enabled/disabled. Ranges from 0 to 15. WARNING: different values may result in an uncontrolled pattern.     
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configIVNH(const unsigned int value) const;

  /**
     Selects the LVDS IVNL current units to enable/disable.
     @param value integer value corresponding, in binary, to the IVNL pattern to be enabled/disabled. Ranges from 0 to 15. WARNING: different values may result in an uncontrolled pattern.     
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int configIVNL(const unsigned int value) const;

  // data flow

  /**
     Elables/disables the merger logic in the periphery. Disabling the merger can be useful to test data transmission without depending on the performance of the merger control flow.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableMerger(const bool on) const;

  /**
     Enables/disables the data flow in the right CMOS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableCMOSRight(const bool on) const;

  /**
     Enables/disables the data flow in the left CMOS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableCMOSLeft(const bool on) const;

  /**
     Enables/disables the data flow in the LVDS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableDataflowLVDS(const bool on) const;

  /**
     Enables/disables the top-right CMOS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableRightCMOSTop(const bool on) const;

  /**
     Enables/disables the top-left CMOS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableLeftCMOSTop(const bool on) const;

  /**
     Enables/disables the merger output to the right CMOS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableRightMergerToCMOS(const bool on) const;

  /**
     Enables/disables the merger output to the right LVDS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableRightMergerToLVDS(const bool on) const;

  /**
     Enables/disables the merger output to the left CMOS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableLeftMergerToCMOS(const bool on) const;

  /**
     Enables/disables the merger output to the left LVDS drivers.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableLeftMergerToLVDS(const bool on) const;

  /**
     Enables/disables the merger output to the right.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableMergerToRight(const bool on) const;

  /**
     Enables/disables the merger output to the left.
     @param on ON boolean: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enableMergerToLeft(const bool on) const;

  // power switch

  /**
     Configures the interconnection of the power supply pads. The combination of left and right interconnections allows for four different cases. When no interconnection is enabled, an external power supply is required. The default value is left matrix and DAC power supply interconnected, right power supplies not interconnected.
     @param left ON boolean for the left pads: true => enable, false => disable.
     @param right ON boolean for the right pads: true => enable, false => disable.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int enablePowerSwitch(const bool left,
                                 const bool right) const;

  // read register

  /**
     Sends a read request for a specific register. WARNING: only the registers with ID ranging from 0 to 22 can be readout.
     @param registerID target register. Ranges from 0 to 22. WARNING: values outside this range may result in uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int readRegister(const unsigned int registerID) const;

  // write reserved register

  /**
     Writes a user-defined value in the one of the spare registers. The bits of these registers are not connected to any output line.
     @param registerID target register. Ranges from 0 to 4. WARNING: values outside this range will produce a warning and may result in uncontrolled behavior.
     @param value value to be stored in the register. Ranges from 0 to 511. WARNING: values outside this range may result in uncontrolled behavior.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int writeReservedRegister(const unsigned int registerID,
                                     const unsigned int value) const;

  // self test

  /**
     Enables the self-test of the slow control logic. After this command, the slow control issues the 0xAAAA default word.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int selfTest() const;

  // read SEU counter

  /**
     Sends a read request for the SEU counter. In the first version of MALTA, only SEUs occurring inside the slow control logic are counted. SEUs are evaluated via a majority voting logic on a Triple Modular Redundancy (TMR) scheme. The SEU counter is asynchronous. This means that if multiple SEUs take place simultaneously in different locations, the counter is incremented by only one unit.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int readSEUCounter() const;

  // read chipID

  /**
     Sends a read request for the chip ID.
     @return integer number corresponding to the 16-bit slow control command
   */
  unsigned int readChipID() const;

  ////////////////////////
  // higher level commands
  ////////////////////////

  /**
     Computes the diagonal coordinate of a given pixel specified bz col and hor. 
     @param col column coordinate of target pixel. Ranges from 0 to 511. WARNING: different values may result in an uncontrolled behavior. 
     @param hor row coordinate of target pixel. Ranges from 0 to 511. WARNING: different values may result in an uncontrolled behavior. 
     @return the diagonal coordinate
   */
  unsigned int getDiagonal(const unsigned int col,
                           const unsigned int hor) const;

  /**
     Produces the three commands (maskCol, maskHor, maskDiag) to fully mask a given pixel
     @param col column coordinate of target pixel. Ranges from 0 to 511. WARNING: different values may result in an uncontrolled behavior. 
     @param hor row coordinate of target pixel. Ranges from 0 to 511. WARNING: different values may result in an uncontrolled behavior. 
     @param mask true if mask, false if unmask
     @return vector of three integer numbers corresponding to the 16-bit slow control commands
   */
  std::vector<unsigned int> maskPixel(const unsigned int col,
                                      const unsigned int hor, 
                                      bool mask=true) const;

private:
  
  /////////////////////
  // options dictionary
  /////////////////////
  
  // pOPCODE_RESET
  const static unsigned int pOPCODE_RESET = 0x0;
  // pOPCODE_RESET suboptions
  const static unsigned int pRESET_CMU = 0x0;
  const static unsigned int pGENERATE_RST_N = 0x1;
  const static unsigned int pSCLRREGISTER = 0x2;

  // pOPCODE_MASK
  const static unsigned int pOPCODE_MASK = 0x1;
  // pOPCODE_MASK suboptions  
  const static unsigned int pMASK_COL = 0x0;
  const static unsigned int pMASK_HOR = 0x1;
  const static unsigned int pMASK_DIAG = 0x2;
  const static unsigned int pMASK_FULLCOL = 0x3;

  // pOPCODE_PULSE
  const static unsigned int pOPCODE_PULSE = 0x2;
  // pOPCODE_PULSE suboptions  
  const static unsigned int pPULSE_COL = 0x0;
  const static unsigned int pPULSE_HOR = 0x1;
  const static unsigned int pPULSE_FIXED = 0x2;
  
  // pOPCODE_DELAY
  const static unsigned int pOPCODE_DELAY = 0x3;
  // pOPCODE_DELAY suboptions
  const static unsigned int delay2000 = 0x7;
  const static unsigned int delay1000 = 0xB;
  const static unsigned int delay0750 = 0xD;
  const static unsigned int delay0500 = 0xE;
  
  // pOPCODE_DAC
  const static unsigned int pOPCODE_DAC = 0x4;
  // pOPCODE_DAC suboptions
  const static unsigned int pRSET_VCASN = 0x0;
  const static unsigned int pRSET_VCLIP = 0x1;
  const static unsigned int pRSET_VPULSE_HIGH = 0x2;
  const static unsigned int pRSET_VPULSE_LOW = 0x3;
  const static unsigned int pRSET_VRESET_P = 0x4;
  const static unsigned int pRSET_VRESET_D = 0x5;
  const static unsigned int pRSET_ICASN = 0x6;
  const static unsigned int pRSET_IRESET = 0x7;
  const static unsigned int pRSET_ITHR = 0x8;
  const static unsigned int pRSET_IBIAS = 0x9;
  const static unsigned int pRSET_IDB = 0xA;
  const static unsigned int pRSET_IBUFP_MON0 = 0x11;
  const static unsigned int pRSET_IBUFN_MON0 = 0x12;
  const static unsigned int pRSET_IBUFP_MON1 = 0x13;
  const static unsigned int pRSET_IBUFN_MON1 = 0x14;
  const static unsigned int pRSET_RDACSWITCH = 0x15;
  // pRSET_RDACSWITCH sub-suboptions
  const static unsigned int pPOS_SWCNTL_DACMONI = 0x0;
  const static unsigned int pPOS_SWCNTL_DACMONV = 0x1;
  const static unsigned int pPOS_SET_IRESET_BIT = 0x2;
  const static unsigned int pPOS_SWCNTL_IREF = 0x3;
  const static unsigned int pPOS_SWCNTL_IDB = 0x4;
  const static unsigned int pPOS_SWCNTL_ITHR = 0x5;
  const static unsigned int pPOS_SWCNTL_IBIAS = 0x6;
  const static unsigned int pPOS_SWCNTL_IRESET = 0x7;
  const static unsigned int pPOS_SWCNTL_ICASN = 0x8;
  const static unsigned int pPOS_SWCNTL_VRESET_D = 0x9;
  const static unsigned int pPOS_SWCNTL_VRESET_P = 0xA;
  const static unsigned int pPOS_SWCNTL_VPULSE_LOW = 0xB;
  const static unsigned int pPOS_SWCNTL_VPULSE_HIGH = 0xC;
  const static unsigned int pPOS_SWCNTL_VCLIP = 0xD;
  const static unsigned int pPOS_SWCNTL_VCASN = 0xE;
  
  // pOPCODE_LVDS
  const static unsigned int pOPCODE_LVDS = 0x5;
  // pOPCODE_LVDS suboptions
  const static unsigned int pCONFIG_LVDS_ENABLE = 0x0;
  const static unsigned int pCONFIG_EN_PRE = 0x1;
  const static unsigned int pCONFIG_EN_HBRIDGE = 0x2;
  const static unsigned int pCONFIG_EN_CMFB = 0x3;
  const static unsigned int pCONFIG_SET_IBCMFB = 0x4;
  const static unsigned int pCONFIG_SET_IVPH = 0x5;
  const static unsigned int pCONFIG_SET_IVPL = 0x6;
  const static unsigned int pCONFIG_SET_IVNH = 0x7;
  const static unsigned int pCONFIG_SET_IVNL = 0x8;
  // pCONFIG_EN_PRE sub-suboptions
  const static unsigned int pEN_PRE_5downto0 = 0x0;
  const static unsigned int pEN_PRE_10downto6 = 0x1;
  const static unsigned int pEN_PRE_15downto11 = 0x2;
  
  // pOPCODE_DATAFLOW
  const static unsigned int pOPCODE_DATAFLOW = 0x6;
  // pOPCODE_DATAFLOW suboptions
  const static unsigned int pPOS_RDATAFLOW_ENABLEMERGER = 0x0;
  const static unsigned int pPOS_RDATAFLOW_ENABLECMOSRIGHT = 0x1;
  const static unsigned int pPOS_RDATAFLOW_ENABLECMOSLEFT = 0x2;
  const static unsigned int pPOS_RDATAFLOW_ENABLELVDS = 0x3;
  const static unsigned int pPOS_RDATAFLOW_RIGHTCMOSTOP = 0x4;
  const static unsigned int pPOS_RDATAFLOW_LEFTCMOSTOP = 0x5;
  const static unsigned int pPOS_RDATAFLOW_RIGHTMERGERTOCMOS = 0x6;
  const static unsigned int pPOS_RDATAFLOW_RIGHTMERGERTOLVDS = 0x7;
  const static unsigned int pPOS_RDATAFLOW_LEFTMERGERTOCMOS = 0x8;
  const static unsigned int pPOS_RDATAFLOW_LEFTMERGERTOLVDS = 0x9;
  const static unsigned int pPOS_RDATAFLOW_MERGERTORIGHT = 0xA;
  const static unsigned int pPOS_RDATAFLOW_MERGERTOLEFT = 0xB;
  
  // pOPCODE_POWERSWITCH
  const static unsigned int pOPCODE_POWERSWITCH = 0x7;
  
  // pOPCODE_READREGISTER
  const static unsigned int pOPCODE_READREGISTER = 0x8;
  
  // pOPCODE_WRRESERVEDREG
  const static unsigned int pOPCODE_WRRESERVEDREG = 0x9;
  // pOPCODE_WRRESERVEDREG suboptions
  const static unsigned int pWRITE_RESERVEDREG0 = 0x0;
  const static unsigned int pWRITE_RESERVEDREG1 = 0x1;
  const static unsigned int pWRITE_RESERVEDREG2 = 0x2;
  const static unsigned int pWRITE_RESERVEDREG3 = 0x3;
  const static unsigned int pWRITE_RESERVEDREG4 = 0x4;
  
  // pOPCODE_SELFTEST
  const static unsigned int pOPCODE_SELFTEST = 0xA;
  
  // pOPCODE_READSEUCOUNTER
  const static unsigned int pOPCODE_READSEUCOUNTER = 0xB;
  
  // pOPCODE_READCHIPID
  const static unsigned int pOPCODE_READCHIPID = 0xC;

  /////////////////////////////////
  // list of non-encoded registers
  // (can be read by SERIAL_OUTPUT)
  /////////////////////////////////

  // identifier
  const static unsigned int pRDELAY = 0x0;
  const static unsigned int pRDATAFLOW = 0x1;
  const static unsigned int pRPOWER_SWITCH = 0x2;
  const static unsigned int pRLVDS_ENABLE = 0x3;
  const static unsigned int pREN_PRE = 0x4;
  const static unsigned int pREN_HBRIDGE = 0x5;
  const static unsigned int pREN_CMBF = 0x6;
  const static unsigned int pRSET_IBCMFB = 0x7;
  const static unsigned int pRSET_IVPH = 0x8;
  const static unsigned int pRSET_IVPL = 0x9;
  const static unsigned int pRSET_IVNH = 0xA;
  const static unsigned int pRSET_IVNL = 0xB;
  const static unsigned int pRRESERVED0 = 0xC;
  const static unsigned int pRRESERVED1 = 0xD;
  const static unsigned int pRRESERVED2 = 0xE;
  const static unsigned int pRRESERVED3 = 0xF;
  const static unsigned int pRRESERVED4 = 0x10;
  const static unsigned int pRSET_IBUFP_MON_0 = 0x11;
  const static unsigned int pRSET_IBUFN_MON_0 = 0x12;
  const static unsigned int pRSET_IBUFP_MON_1 = 0x13;
  const static unsigned int pRSET_IBUFN_MON_1 = 0x14;
  const static unsigned int pRDACSWITCH = 0x15;
  const static unsigned int pRSELFTEST = 0x16;

  // default value
  const static unsigned int defaultRDELAY = 0x7;
  const static unsigned int defaultRDATAFLOW = 0xB0D;
  const static unsigned int defaultRPOWER_SWITCH = 0x2;
  const static unsigned int defaultRLVDS_ENABLE = 0x1;
  const static unsigned int defaultREN_PRE = 0xFE00;
  const static unsigned int defaultREN_HBRIDGE = 0x1C;
  const static unsigned int defaultREN_CMBF = 0x10;
  const static unsigned int defaultRSET_IBCMFB = 0x7;
  const static unsigned int defaultRSET_IVPH = 0x2;
  const static unsigned int defaultRSET_IVPL = 0xD;
  const static unsigned int defaultRSET_IVNH = 0xD;
  const static unsigned int defaultRSET_IVNL = 0x7;
  const static unsigned int defaultRRESERVED0 = 0x0;
  const static unsigned int defaultRRESERVED1 = 0x0;
  const static unsigned int defaultRRESERVED2 = 0x0;
  const static unsigned int defaultRRESERVED3 = 0x0;
  const static unsigned int defaultRRESERVED4 = 0x0;
  const static unsigned int defaultRSET_IBUFP_MON_0 = 0x5;
  const static unsigned int defaultRSET_IBUFN_MON_0 = 0x9;
  const static unsigned int defaultRSET_IBUFP_MON_1 = 0x5;
  const static unsigned int defaultRSET_IBUFN_MON_1 = 0x9;
  const static unsigned int defaultRDACSWITCH = 0x4;
  const static unsigned int defaultRSELFTEST = 0xAAAA;  

  ////////////////////////////////////  
  // list of encoded registers
  // (cannot be read by SERIAL_OUTPUT)
  ////////////////////////////////////

  // identifier
  const static unsigned int pSCLR_MASKCOL = 0x17;
  const static unsigned int pSCLR_MASKHOR = 0x18;
  const static unsigned int pSCLR_MASKDIAG = 0x19;
  const static unsigned int pSCLR_MASKFULLCOL = 0x1A;
  const static unsigned int pSCLR_PULSCOL = 0x1B;
  const static unsigned int pSCLR_PULSHOR = 0x1C;
  const static unsigned int pSCLR_SET_VCASN = 0x1D;
  const static unsigned int pSCLR_SET_VCLIP = 0x1E;
  const static unsigned int pSCLR_SET_VPLSE_HIGH = 0x1F;
  const static unsigned int pSCLR_SET_VPLSE_LOW = 0x20;
  const static unsigned int pSCLR_SET_VRESET_P = 0x21;
  const static unsigned int pSCLR_SET_VRESET_D = 0x22;
  const static unsigned int pSCLR_SET_ICASN = 0x23;
  const static unsigned int pSCLR_SET_IRESET = 0x24;
  const static unsigned int pSCLR_SET_ITHR = 0x25;
  const static unsigned int pSCLR_SET_IBIAS = 0x26;
  const static unsigned int pSCLR_SET_IDB = 0x27;

};

#endif
