#ifndef MALTATREE_H
#define MALTATREE_H

#include "Malta/MaltaData.h"
#include "TFile.h"
#include "TTree.h"
#include <string>
#include <chrono>

/**
 * MaltaTree is a tool to store MALTA data as a ROOT TTree.
 * A new file can be openned with MaltaTree::Open.
 * Parameters that will be stored for every entry until
 * changed are run number (MaltaTree::SetRunNumber),
 * ITHRES (MaltaTree::SetIthres), L1ID (MaltaTree::SetL1idC),
 * duplicate flag (MaltaTree::SetIsDuplicate).
 * MaltaData (MaltaTree::Set) will be decoded and stored as
 * different branches in the tree, as well as a time stamp
 * (timer). After adding the parameters, it is necessary
 * to call MaltaTree::Fill.
 * MaltaData can be written like the following:
 *
 * @verbatim
   MaltaTree * mt = new MaltaTree();
   mt->Open("file.root","RECREATE");

   mt->SetIthres(200);
   mt->SetRunNumber(5001);

   for(...){
     MaltaData * md=...
     mt->Add(md);
     mt->Fill();
   }

   mt->Close();
   delete mt;
   @endverbatim
 *
 * MaltaData can be read like the following:
 *
 * @verbatim
   MaltaTree * mt = new MaltaTree();
   mt->Open("file.root","READ");

   while(mt->Next()){
     MaltaData * md = mt->Get()
     uint32_t l1id = mt->GetL1idC();
     float timer = mt->GetTimer();
   }

   mt->Close();
   delete mt;
   @endverbatim
 *
 * @brief Tool to store MaltaData as a ROOT TTree.
 * @author Carlos.Solans@cern.ch
 * @date October 2018
 **/

class MaltaTree{

 public:

  /**
   * @brief Create an empty MaltaTree.
   **/
  MaltaTree();

  /**
   * @brief Delete the MaltaTree. Close any open file.
   **/
  ~MaltaTree();

  /**
   * @brief Open a ROOT file.
   * @param filename The path to the file as a string.
   * @param options ROOT options for the file.
   **/
  void Open(std::string filename,std::string options);

  /**
   * @brief Close the ROOT file.
   **/
  void Close();

  /**
   * @brief Add a new entry to the Tree.
   **/
  void Fill();

  /**
   * @brief Add the contents of MaltaData to the current entry.
   *        This also adds a time stamp (timer) to the entry.
   * @param data The MaltaData that needs to be stored in the tree entry.
   **/
  void Set(MaltaData * data, int modulesize = 1);

  /**
   * @brief Set the ITHRES in the current entry.
   * @param v The value of the ITHRES.
   **/
  void SetIthres(int v);

  /**
   * @brief Set the run number in the current entry.
   * @param v The value of the run number.
   **/
  void SetRunNumber(int v);

  /**
   * @brief Set the L1ID in the current entry.
   * @param v The value of the L1ID.
   **/
  void SetL1idC(uint32_t v);

  /**
   * @brief Mark the MALTA word as duplicate.
   * @param v 1 if duplicate, 0 if not.
   **/
  void SetIsDuplicate(int v);

  /**
   * @brief Get the current entry as a MaltaData object.
   * @return A MaltaData object.
   **/
  MaltaData * Get();

  /**
   * @brief Get the ITHRES in the current entry.
   * @return The value of the ITHRES.
   **/
  int GetIthres();

  /**
   * @brief Get the run number in the current entry.
   * @return The value of the run number.
   **/
  int GetRunNumber();

  /**
   * @brief Get the L1ID of the current entry.
   * @return The value of the L1ID.
   **/
  uint32_t GetL1idC();

  /**
   * @brief Get the duplicate flag of the entry.
   * @return The duplicate flag.
   **/
  int GetIsDuplicate();

  /**
   * @brief Get the timestamp of the entry.
   * @return The timestamp of the run number.
   **/
  float GetTimer();

  /**
   * @brief Get which bits are high, which bits are low.
   * @return Which bits are high, which bits are low.
   **/
  float GetBitsHiLo();

  /**
   * @brief Get the length of the MALTA word.
   * @return The length of the MALTA word.
   **/
  float GetWordLength();

  /**
   * @brief Move the internal pointer to the next entry in the tree.
   * @return 0 if there are no more entries to read, -1 if there was an error.
   **/
  int Next();

  /**
   * @brief Get underlaying ROOT file used by MaltaTree.
   * @return The ROOT TFile object.
   **/
  TFile* GetFile();

 private:

  TFile * m_file;
  TTree * m_tree;
  bool m_readonly;
  std::chrono::steady_clock::time_point m_t0;

  uint32_t pixel;
  uint32_t group;
  uint32_t parity;
  uint32_t delay;
  uint32_t dcolumn;
  uint32_t chipbcid;
  uint32_t chipid;
  uint32_t phase;
  uint32_t winid;
  uint32_t bcid;
  uint32_t l1id;
  uint32_t run;
  uint32_t l1idC;
  float    timer;
  uint32_t word1;
  uint32_t word2;
  uint32_t coordX;
  uint32_t coordY;
  uint32_t ithres;
  uint32_t vlow;
  uint32_t isDuplicate;
  uint32_t bitsHiLo;
  uint32_t wordLength;

  MaltaData m_data;
  uint64_t m_entry;
};

#endif
