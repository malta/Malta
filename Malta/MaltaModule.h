#ifndef MALTAMODULE_H
#define MALTAMODULE_H

#include "MaltaDAQ/ReadoutModule.h"
#include "Malta/MaltaBase.h"
#include "Malta/MaltaTree.h"

#include <string>
#include <thread>
#include <map>

#include "TString.h"

/**
 * MaltaModule is an implementation of a ReadoutModule for
 * the test-beam data-taking application (MaltaMultiDAQ)
 * to control MALTA.
 * MaltaModule can be loaded dynamically thrhough the ModuleLoader
 * as a ReadoutModule object.
 *
 * @verbatim

   ReadoutModule * malta = ModuleLoader::getInstance()->newReadoutModule("MaltaDAQ","MaltaModule",
                                                                         "PLANE_1","192.168.200.10","");

   @endverbatim
 *
 * MaltaModule connects to the MALTA FPGA on creation, and configures it
 * for operation with MaltaModule::Configure.
 * The configuration is steered from the ReadoutConfig object that is
 * loaded from a configuration text file through ConfigParser.
 *
 * @verbatim

   [PLANE 1]
   type: MALTA
   wafer: W4R10
   address: 192.168.200.10
   CONFIGURATION_VOLTAGE: 0.8
   SC_ENABLE_MON_DAC_CURRENT: false
   SC_ENABLE_MON_DAC_VOLTAGE: false
   SC_IDB: 100
   SC_ITHR: 30
   SC_IRESET: 10
   SC_ICASN: 10
   SC_VCASN: 64
   SC_VCLIP: 127
   SC_VPULSE_HIGH: 127
   SC_VPULSE_LOW: 127
   SC_VRESET_P: 45
   SC_VRESET_D: 64
   @endverbatim
 *
 * MaltaModule fills 3 histograms, the distribution of the
 * time of the hits (ReadoutModule::GetTimingHisto),
 * the number of hits per event (ReadoutModule::GetNHitHisto),
 * the hit map (ReadoutModule::GetHitMapHisto).

 * The methods implemented in this class are the following:
 * * MaltaModule::SetInternalTrigger configures the plane for internal triggering.
 * * MaltaModule::Configure configures the plane for data taking.
 * * MaltaModule::Start starts the data taking on the plane by spawning the Run method.
 * * MaltaModule::Stop stops the data taking on the plane by stopping the Run thread.
 * * MaltaModule::EnableFastSignal enables the trigger signal out from the plane if any.
 * * MaltaModule::DisableFastSignal disables the trigger signal out from the plane if any.
 *
 *
 * @brief MALTA ReadoutModule for MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @date October 2018
 **/

class MaltaModule: public ReadoutModule{

 public:

  /**
   * brief: create a malta module
   * param name the module name
   * param address the ipbus connection string
   * param outdir the output directory
   **/
  MaltaModule(std::string name, std::string address, std::string outdir);

  /**
   * brief: delete pointers
   **/
  ~MaltaModule();

  /**
   * brief: Set the module for internal triggering
   **/
  void SetInternalTrigger();

  /**
   * Brief: Configure MALTA for data taking
   **/
  void Configure();

  /**
   * Brief: Start data acquisition thread
   * param run the run number as a string
   * param usePath use the path
   **/
  void Start(std::string run, bool usePath = false);

  /**
   * Brief: Stop the data acquisition thread
   **/
  void Stop();

  /**
   * Brief: The data acquisition thread
   **/
  void Run();

  /**
   * brief: get the L1A from the FPGA
   * return L1A
   **/
  uint32_t GetNTriggers();

  /**
   * brief: enable fast signal
   **/
  void EnableFastSignal();

  /**
   * brief: disable fast signal
   **/
  void DisableFastSignal();

 private:


  MaltaBase * m_malta;
  MaltaTree * m_ntuple;

	int m_modulesize;

};

#endif
