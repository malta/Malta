#ifndef MALTADAQ_MALTATHRESHOLDANALYSIS_H
#define MALTADAQ_MALTATHRESHOLDANALYSIS_H

#include "Malta/MaltaData.h"
#include "TH1F.h"
#include "TTree.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include <string>
#include <map>

/**
 * @brief MALTA threshold scan analysis algorithm
 * @todo: Update documentation
 **/
class MaltaThresholdAnalysis{

 public:

  MaltaThresholdAnalysis(uint32_t nbins, float minVal, float maxVal, std::string folderName);
  
  ~MaltaThresholdAnalysis();

  void process(MaltaData *md, uint32_t column, uint32_t row, float param, bool isDuplicate);

  bool check_next_empty(int , TH1F* );  
  double find_middlePoint(TGraphErrors *);

  void end(float Scantime,float mScantime_pix[], TTree* config_tree);
  
  void setNPulse(int pulse);

  float getValueEl(int VL, int VH);

 private:

  TH1F * m_histos[512][512];
  TH1F * m_hThres;
  uint32_t m_nbins;
  float m_minVal;
  float m_maxVal;
  TFile * m_file;
  int m_npulse;
  std::string m_folderName; 

  std::map<int,float> m_conversion;
};


#endif

