#ifndef MALTABASE_H
#define MALTABASE_H

#include <vector>
#include <map>
#include <bitset>
#include <cstdint>
#include <string>
#include <sstream>
#include "ipbus/Uhal.h"
#include "Malta/MaltaSlowControl.h"

#include "TH2D.h"
#include "TH1D.h"

/**
 * MaltaBase contains all the necessary methods to control the MALTA
 * Pixel detector prototypes through ipbus.
 * Upon creation of a new MaltaBase, the connection to the FPGA
 * is established through ipbus.
 * The version of the FW running in the FPGA can be retrieved with
 * MaltaBase::GetVersion.
 * In order to configure MALTA for operation, the slow control registers
 * must be defined.
 * There are 11 DACs that need to be configured ( \c IDB, \c ITHR, \c IBIAS,
 * \c IRESET, \c ICASN, \c VCASN, \c VCLIP, \c VPULSE_HIGH, \c VPULSE_LOW,
 * \c VRESET_P, \c VRESET_D ).
 * And in particular, the monitoring must be disabled for MALTA C,
 * and enabled for MALTA B (MLVLC).
 * This can be done through dedicated methods, where the first parameter
 * represents the desired state of the DAC, and the second parameter is a
 * boolean value indicating if the configuration should be
 * sent immediately or not. Stored slow control commands can be sent anytime
 * to the FPGA by MaltaBase::Send.
 * One important aspect of the configuration of MALTA prototypes is the
 * configuration voltage. This is the value of DVDD powering the chip
 * while the configuration commands are been transmitted to it.
 * The method MaltaBase::SetConfigMode allows to specify a non standard
 * configuration voltage. This method makes use of the MALTA_PSU functionality.
 *
 * @verbatim

   MaltaBase * malta = new MaltaBase("udp://192.168.200.10");
   malta->SetConfigMode(true, 0.9);

   malta->EnableMerger(false,true);
   malta->SetPulseWidth(750,true);

   malta->EnableMonDacCurrent(false,true);
   malta->EnableMonDacVoltage(true,true);
   malta->EnableIDB(false,true);
   malta->EnableITHR(false,true);
   malta->EnableIRESET(false,true);
   malta->EnableICASN(false,true);
   malta->EnableVCASN(false,true);
   malta->EnableVCLIP(false,true);
   malta->EnableVPULSE_HIGH(false,true);
   malta->EnableVPULSE_LOW(false,true);
   malta->EnableVRESET_P(false,true);
   malta->EnableVRESET_D(false,true);

   malta->SetConfigMode(false);
   @endverbatim
 *
 *
 * Additionally, the configuration of the chip can be carried out by configuration
 * strings with MaltaBase::SetConfigFromString. The statements have the same format
 * as the ConfigParser statements (\c SB_IDB: \c 100 ).
 * The available registers are listed in MaltaBase::COMMANDS.
 *
 * @verbatim

   MaltaBase * malta = new MaltaBase("udp://192.168.200.10");
   malta->SetConfigMode(true, 0.9);

   malta->EnableMerger(false,true);
   malta->SetPulseWidth(750,true);

   malta->SetConfigFromString("SC_ENABLE_MON_DAC_CURRENT: false");
   malta->SetConfigFromString("SC_ENABLE_MON_DAC_VOLTAGE: false");
   malta->SetConfigFromString("SC_IDB: 100");
   malta->SetConfigFromString("SC_ITHR: 30");
   malta->SetConfigFromString("SC_IRESET: 10");
   malta->SetConfigFromString("SC_ICASN: 10");
   malta->SetConfigFromString("SC_VCASN: 64");
   malta->SetConfigFromString("SC_VCLIP: 127");
   malta->SetConfigFromString("SC_VPULSE_HIGH: 127");
   malta->SetConfigFromString("SC_VPULSE_LOW: 127");
   malta->SetConfigFromString("SC_VRESET_P: 45");
   malta->SetConfigFromString("SC_VRESET_D: 64");

   malta->SetConfigMode(false);
   @endverbatim
 *
 * Other useful string commands are the following:
 * * PULSE_PIXEL_ROW: (row)[0,511]
 * * PULSE_PIXEL_COLUMN: (col)[0,511]
 * * UNPULSE_PIXEL_ROW: (row)[0,511]
 * * UNPULSE_PIXEL_COLUMN: (col)[0,511]
 * * MASK_PIXEL: (row)[0,511], (col)[0,511]
 * * MASK_DOUBLE_COLUMN: (dcol)[0,255]
 * * MASK_DOUBLE_COLUMNS: (dcol1)[0,255], (dcol2)[0,255]
 * * ROI: (row1)[0,511], (col1)[0,511], (row2)[0,511], (col2)[0,511]
 *
 *
 * @brief MALTA control and read-out class
 * @author Abhishek.Sharma@cern.ch
 * @author Carlos.Solans@cern.ch
 * @date May 2019
 **/
class MaltaBase{

 public:

  //! FIFO status register
  static const uint32_t FIFOSTATUS    = 0x3;
  //! Read slow control
  uint32_t SLOWCONTROL_R = 0x4;
  //! Write slow control
  uint32_t SLOWCONTROL_W = 0x5;
  //! Slow/big FIFO readout
  static const uint32_t DATA          = 0x6;
  //! Control register
  static const uint32_t CONTROL       = 0x8;
  //! Pipeline depth control and fast signal enable
  static const uint32_t DELAY         = 0x9;
  static const uint32_t TAP0          = 0xA;
  static const uint32_t NTAPS         = 40;
  static const uint32_t TAP0_R        = 0x32;

  //FIFO 2 word count
  static const uint32_t WORDCOUNT     = 0x5B;
  static const uint32_t RODELAY       = 0x5C;

  static const uint32_t CTRL_INCTRIG  = 0x080000;
  static const uint32_t CTRL_PULSE    = 0x100000;

  //! FIFO 1 full
  static const uint32_t FIFO1_FULL  = 0x000001;
  //! FIFO 2 full
  static const uint32_t FIFO2_FULL  = 0x000002;
  //! FIFO 1 empty
  static const uint32_t FIFO1_EMPTY = 0x000008;
  //! FIFO 2 empty
  static const uint32_t FIFO2_EMPTY = 0x000010;
  //! FIFO 1 half
  static const uint32_t FIFO1_HALF  = 0x000040;
  //! FIFO 2 half
  static const uint32_t FIFO2_HALF  = 0x000080;

  static const uint32_t FIFOM_EMPTY = 0x000020;

  enum COMMANDS {
    SC_ENABLE_MERGER,
    SC_ENABLE_MON_DAC_CURRENT,
    SC_ENABLE_MON_DAC_VOLTAGE,
    SC_ENABLE_IDB,
    SC_ENABLE_ITHR,
    SC_ENABLE_IBIAS,
    SC_ENABLE_IRESET,
    SC_ENABLE_ICASN,
    SC_ENABLE_VCASN,
    SC_ENABLE_VCLIP,
    SC_ENABLE_VPULSE_HIGH,
    SC_ENABLE_VPULSE_LOW,
    SC_ENABLE_VRESET_P,
    SC_ENABLE_VRESET_D,
    SC_CMOS_TOPLEFT_TO_TOPRIGHT,
    SC_CMOS_BOTLEFT_TO_TOPRIGHT,
    SC_CMOS_TOPRIGHT_TO_TOPLEFT,
    SC_CMOS_BOTRIGHT_TO_TOPLEFT,
    SC_CMOS_TOPLEFT_TO_BOTRIGHT,
    SC_CMOS_BOTLEFT_TO_BOTRIGHT,
    SC_CMOS_TOPRIGHT_TO_BOTLEFT,
    SC_CMOS_BOTRIGHT_TO_BOTLEFT,
    SC_LVDS_FROMBOTRIGHT,
    SC_LVDS_FROMTOPRIGHT,
    SC_LVDS_FROMBOTLEFT,
    SC_LVDS_FROMTOPLEFT,
    SC_MUTE_OUTPUT,
    SC_LINK_SC_ADDRESS,
    SC_SET_SC_ADDRESS,
    SC_SET_ADDRESS,
    SC_IDB,
    SC_ITHR,
    SC_IBIAS,
    SC_IRESET,
    SC_ICASN,
    SC_VCASN,
    SC_VCLIP,
    SC_VPULSE_HIGH,
    SC_VPULSE_LOW,
    SC_VRESET_P,
    SC_VRESET_D,
    SC_PULSE_WIDTH,
    SC_RESERVED_1,
    SC_RESERVED_2,
    SC_RESERVED_3,
    SC_RESERVED_4,
    SC_RESERVED_5,
    SC_PRECONFIG,
    MASK_DOUBLE_COLUMN,
    MASK_DOUBLE_COLUMNS,
    MASK_PIXEL,
    ROI,
    MASK_PIXELS_FROM_TXT,
    PULSE_PIXEL_ROW,
    PULSE_PIXEL_COLUMN,
    UNPULSE_PIXEL_ROW,
    UNPULSE_PIXEL_COLUMN,
    DUT,
    TAP_FROM_TXT,
  };

  /**
   * Initialize commands vector, create new MaltaSlowControl, and ipbus::Uhal objects.
   * @param address The address of the FPGA for commnunication through ipbus.
   **/
  MaltaBase(std::string address);

  /**
   * Delete Uhal and MaltaSlowControl objects
   **/
  ~MaltaBase();

  /**
   * Read the version from the FPGA
   **/
  void GetVersion();

  /**
   * Get the number of words currently available to read in the IPB FIFO (FIFO2)
   * @return The number of words available in the FIFO.
   **/
  uint32_t GetFIFO2WordCount();

  /**
   * Get the current L1ID from the FIFOSTATUS register (busy register)
   * @return the current L1ID.
   **/
  uint32_t GetL1ID();

  /**
   * Enable/Disable verbose mode. Does not enable Uhal verbose mode.
   * @param enable Enable verbose mode if true
   **/
  void SetVerbose(bool enable);

  /**
   * Change the default sleep when the config mode is changed (default 600000).
   * @param microseconds The sleep value in microseconds.
   **/
  void SetConfigModeSleep(uint32_t microseconds);

  /**
   * Enable/Disable configuration mode of MALTA
   * by lowering DVDD to given value
   * @param enable Enable if true, else disable
   * @param dvdd DVDD voltage to use in Volts if enable is true
   **/
  void SetConfigMode(bool enable, float dvdd=0.9);

  /**
   * Write the same tap delays for all bits
   * @param tap1 Tap value for 0 degree data copy
   * @param tap2 Tap value for 90 degree data copy
   **/
  void WriteConstDelays(uint32_t tap1, uint32_t tap2);

  /**
   * Write the tap delays for a given bit
   * @param bit Integer value of the bit
   * @param tap1 Tap value for 0 degree data copy
   * @param tap2 Tap value for 90 degree data copy
   **/
  void WriteTap(uint32_t bit, uint32_t tap1, uint32_t tap2);

  /**
   * Read the tap delays for a given bit
   * @param bit Integer value of the bit
   * @return vector with tap1 followed by tap2
   **/
  std::vector<uint32_t> ReadTap(uint32_t bit);

  /**
   * Read the taps for all bits from a file
   * @param fileName path to the file
   **/
  void ReadTapsFromFile(std::string fileName);

  /**
   * Write the taps for all bits to a file
   * @param fileName path to the file
   **/
  void WriteTapsToFile(std::string fileName);

  /**
   * Reset the FPGA.
   **/
  void Reset();

  /**
   * Reset only the FIFO.
   **/
  void ResetFifo();

  /**
   * @brief Enable/Disable the readout of the top half of the matrix columns. Columns 256 to 511.
   * @param enable Enable the readout if true
   **/
  void SetHalfColumns(bool enable);

  /**
   * @brief Enable/Disable the readout of the top half of the matrix columns. Columns 256 to 511.
   * @param enable Enable the readout if true
   **/
  void SetHalfRows(bool enable);

  /**
   * @brief set readout delay, values larger than 255 are set to 255
   * @param delay the size of the pipeline
   **/
  void SetReadoutDelay(uint32_t delay);

  /**
   * @brief Get readout delay currently set on the FPGA
   * @return The size of the pipeline
   **/
  uint32_t GetReadoutDelay();

  /**
   * @brief Enable/Disable the external L1A signal.
   * @param enable Enable the readout if true
   **/
  void SetExternalL1A(bool enable);

  /**
   * @brief Enable the external L1A signal.
   * @deprecated
   **/
  void ReadoutEnableExternalL1A();

  /**
   * @brief Disable the external L1A signal.
   * @deprecated
   **/
  void ReadoutDisableExternalL1A();

  /**
   * @brief Enable the small FIFO
   **/
  void ReadoutOn();

  /**
   * @brief Disable the small FIFO
   **/
  void ReadoutOff();

  /**
   * @brief Disable the small FIFO
   **/
  void SetMaxROWindow(int val);

  /**
   * @brief Enable the output of the trigger signal
   **/
  void EnableFastSignal();

  /**
   * @brief Disable the output of the trigger signal
   **/
  void DisableFastSignal();

  /**
   * @brief Reset the L1ID counter
   **/
  void ResetL1Counter();

  /**
   * @brief Read a MALTA word in pairs of 32-bit words
   * @param values unsigned 32-bit pointer to the buffer to fill
   * @param numwords number of words to read (always multiple of 2)
   **/
  void ReadMaltaWord(uint32_t * values, uint32_t numwords=2);

  /**
   * @brief Read a MALTA monitor set (37 of 32-bit words)
   * @param values unsigned 32-bit pointer to the buffer to fill
   **/
  void ReadMonitorWord(uint32_t * values);

  /**
   * @brief Force the IPBUS synchronization
   **/
  void Sync();

  /**
   * @brief Chanbe the IPBUS timeout
   * @param ms The IPBUS timeout in miliseconds
   */

  void SetTimeout(uint32_t ms);

  /**
   * @brief Read the status of the FIFOs
   */
  uint32_t ReadFifoStatus();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the small/fast FIFO is full
   **/
  bool IsFifo1Full();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the small/fast FIFO is empty
   **/
  bool IsFifo1Empty();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the small/fast FIFO is half full
   **/
  bool IsFifo1Half();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the big/slow FIFO is full
   **/
  bool IsFifo2Full();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the big/slow FIFO is empty
   **/
  bool IsFifo2Empty();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the mon FIFO is empty
   **/
  bool IsFifoMonEmpty();

  /**
   * Requires calling ReadFifoStatus to update the value
   * @brief Check if the big/slow FIFO is half full
   **/
  bool IsFifo2Half();

  /**
   * @brief Generate a number of L1A internally
   * @param ntimes Number of triggers to generate
   * @param widthPulse Use predefined pulse width if true
   **/
  void Trigger(uint32_t ntimes=1, bool widthPulse=true);

  /**
   * @brief Enable/Disable pulsing on a given pixel
   * @param row Pixel row
   * @param column Pixel column
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulse(uint32_t row, uint32_t column, bool enable);

  /**
   * @brief Enable/Disable pulsing on a given pixel row
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulseRow(uint32_t row, bool enable);

  /**
   * @brief Enable/Disable pulsing on a given pixel column
   * @param column Pixel column
   * @param enable Enable if true, disable if false
   **/
  void SetPixelPulseColumn(uint32_t column, bool enable);

  /**
   * @brief Enable/Disable mask on a given pixel
   * @param column Pixel column
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMask(uint32_t column, uint32_t row, bool enable);

  /**
   * @brief Enable/Disable masking on a given pixel row
   * @param row Pixel row
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskRow(uint32_t row, bool enable);

  /**
   * @brief Enable/Disable masking on a given pixel column
   * @param column Pixel column
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskColumn(uint32_t column, bool enable);

  /**
   * @brief Enable/Disable masking on a given pixel diagonal
   * @param diag Pixel diagonal
   * @param enable Enable if true, disable if false
   **/
  void SetPixelMaskDiag(uint32_t diag, bool enable);

  /**
   * @brief Enable/Disable mask on double column
   * @param dc Pixel double column
   * @param enable Enable if true, disable if false
   **/
  void SetDoubleColumnMask(uint32_t dc, bool enable);

  /**
   * @brief Enable/Disable mask on a range of double columms
   * @param dc1 First double column to mask
   * @param dc2 Last double column to mask
   * @param mask Mask the double column in range if true, otherwise un-mask
   **/
  void SetDoubleColumnMaskRange(uint32_t dc1, uint32_t dc2, bool mask);

  /**
   * @brief Enable/Disable mask on a range of double columms
   * @param dcs Vector of double columns: dc1, dc2
   * @param mask Mask the double column in range if true, otherwise un-mask
   **/
  void SetDoubleColumnMaskRange(std::vector<uint32_t> dcs, bool mask);

  /**
   * @brief Set Full mask map from the text file
   * @param fileName Absolute path for the file that contains the mask list
   **/
  void SetFullPixelMaskFromFile(std::string fileName);

  /**
   * @brief Enable/Disable the analog masking outside of this columns
   * @param col1 Mask from 0 to this column
   * @param row1 Mask from 0 to this row
   * @param col2 Mask from this column to 511
   * @param row2 Mask from this row to 511
   * @param mask Enable if true, disable if false
   **/
  void SetROI(uint32_t col1, uint32_t row1, uint32_t col2, uint32_t row2, bool mask);

  /**
   * @brief Enable/Disable the analog masking outside of this columns
   * @param coords Vector of coordinates: x1, y1, x2, y2
   * @param mask Enable if true, disable if false
   **/
  void SetROI(std::vector<uint32_t> &coords, bool mask);

  /**
   * @brief Enable/Disable the slow control clock
   * @param enable Enable if true
   **/
  void SetClock(bool enable);

  /**
   * @brief Send the available slow control commands to the FPGA
   **/
  void Send();

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableMerger(bool enable, bool force=false);

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableMonDacCurrent(bool enable, bool force=false);

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableMonDacVoltage(bool enable, bool force=false);

  /**
   * @brief Enable/Disable the merger
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableIDB(bool enable, bool force=false);

  /**
   * @brief Enable/Disable ITHR monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableITHR(bool enable, bool force=false);

  /**
   * @brief Enable/Disable IBIAS monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableIBIAS(bool enable, bool force=false);

  /**
   * @brief Enable/Disable IRESET monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableIRESET(bool enable, bool force=false);

  /**
   * @brief Enable/Disable ICASN monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableICASN(bool enable, bool force=false);

  /**
   * @brief Enable/Disable VCASN monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableVCASN(bool enable, bool force=false);

  /**
   * @brief Enable/Disable VCLIP monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableVCLIP(bool enable, bool force=false);

  /**
   * @brief Enable/Disable VPULSE_HIGH monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableVPULSE_HIGH(bool enable, bool force=false);

  /**
   * @brief Enable/Disable VPULSE_LOW monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableVPULSE_LOW(bool enable, bool force=false);

  /**
   * @brief Enable/Disable VRESET_D monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableVRESET_D(bool enable, bool force=false);

  /**
   * @brief Enable/Disable VRESET_P monitoring
   * @param enable Enable if true
   * @param force Send the command immediately through the slow control if true
   **/
  void EnableVRESET_P(bool enable, bool force=false);

  /**
   * @brief Set DAC value for IDB
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetIDB(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for ITHR
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetITHR(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for IBIAS
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetIBIAS(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for IRESET
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetIRESET(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for ICASN
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetICASN(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for VCASN
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetVCASN(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for VCLIP
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetVCLIP(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for VPULSE_HIGH
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetVPULSE_HIGH(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for VPULSE_LOW
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetVPULSE_LOW(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for VRESET_D
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetVRESET_D(uint32_t value, bool force=false);

  /**
   * @brief Set DAC value for VRESET_P
   * @param value DAC value from 0 to 255
   * @param force Send the command immediately through the slow control if true
   **/
  void SetVRESET_P(uint32_t value, bool force=false);

  /**
   * @brief Set the pulse width
   * @param value pulse width in ps (500, 750, 1000, 1500)
   * @param force Send the command immediately through the slow control if true
   **/
  void SetPulseWidth(uint32_t value, bool force=false);

  /**
   * @brief setting full config from text file (for AIDA telescope)
   * @param str string to parse "SC_IDB=100"
   **/
  void SetConfigFromString(const std::string& str);

  /**
   * @brief setting full config from text file (for AIDA telescope)
   * @param fileName absolute path to the file containing the configuration to parse
   **/
  void SetConfigFromFile(const std::string& fileName);

  /**
   * @brief returns a TH2D* histogram with all pixels masked based on configuration
   **/
  TH2I* GetPixelMask();

  /**
   * @brief returns a TH1D* histogram with all double columnds masked based on configuration
   **/
  TH1D* GetDColMask();

  /**
   * @brief returns a TH2D* histogram with all actually masked pixels including ghost masking
   **/
  TH2I* GetMaskedPixels(int roiXmin=0, int roiYmin=0, int roiXmax=0, int roiYmax=0);

  /**
   * @brief This method changes the behaviour of MaltaBase::SetConfigMode, and executes D_ANAG_OFF.txt instead of T_ANAG_OFF.txt if the dut flag is set.
   * @param isdut Enable the dut flag if true, else disable the flag.
   **/
  void SetDUT(bool isdut);

  /**
   * @brief send the default configuration for MALTA-C, including disable the merger, pulse width, and dac switches
   **/
  void PreConfigMaltaC();

  /**
   * @brief Write the reserved registers
   * @param reg register target register 0 to 4
   * @param value the value of the register
   **/
  void SetReservedRegister(uint32_t reg, uint32_t value);

  /**
   * @brief Read the reserved registers
   * @param reg register target register 0 to 4
   * @return the value of the register
   **/
  uint32_t GetReservedRegister(uint32_t reg);

  /**
   * @brief Read any of the registers
   * @param reg register target register 0 to 23
   * @return the value of the register
   **/
  uint32_t GetRegister(uint32_t reg);

  /*******************************************************

  From here on new methods are added to allow the integration of multi-chip modules into MaltaDAQ
  These changes should be exclusively pushed to branch "MultiChipModule"

  The dataflow settings are grouped into several common setting to reduce the amount of commands needed to configure a chip.

  Slave configurations:
  1) Transmit data: Either from the bottom left to top right CMOS or bottom right to top left CMOS. This way multiple transmitting chips can be chained together.

  Master configurations:
  1) Send data to LVDS: Only send data from the pixel matrix to the LVDS. This is the default single chip module configuration. CMOS input is ignored.
  2) Transmit left data to LVDS: Send data from the matrix and the top left CMOS to the LVDS output. This is used for the master which has a slave connected on the left side.
  3) Transmit right data to LVDS: Send data from the matrix and the top right CMOS to the LVDS output. This is used for the master which has a slave connected on the right side.

  About "left" and "right":
  A top-down view of the chip with the periphery of the chip at the bottom is assumed.

        -----------------
        |               | TOP CMOS
        |    MATRIX     |
   LEFT |               | RIGHT
        |               |
        |               | BOTTOM CMOS
        |---------------|
        |   PERIPHERY   |
        -----------------


  *******************************************************/

  /**
   * @brief Slave config: transmit data from top left CMOS and matrix to top right CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitTopLeftToTopRight(bool force=false);

  /**
   * @brief Slave config: transmit data from bottom left CMOS and matrix to top right CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitBotLeftToTopRight(bool force=false);

  /**
   * @brief Slave config: transmit data from top left CMOS and matrix to bottom right CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitTopLeftToBotRight(bool force=false);

  /**
   * @brief Slave config: transmit data from bottom left CMOS and matrix to bottom right CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitBotLeftToBotRight(bool force=false);

  /**
   * @brief Slave config: transmit data from top right CMOS and matrix to top left CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitTopRightToTopLeft(bool force=false);

  /**
   * @brief Slave config: transmit data from bottom right CMOS and matrix to top left CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitBotRightToTopLeft(bool force=false);

  /**
   * @brief Slave config: transmit data from top right CMOS and matrix to bottom left CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitTopRightToBotLeft(bool force=false);

  /**
   * @brief Slave config: transmit data from bottom right CMOS and matrix to bottom left CMOS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitBotRightToBotLeft(bool force=false);

  /**
   * @brief Slave config: transmit data from top right CMOS and matrix to LVDS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitFromTopRightToLVDS(bool force=false);

  /**
   * @brief Slave config: transmit data from bottom right CMOS and matrix to LVDS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitFromBotRightToLVDS(bool force=false);

  /**
   * @brief Slave config: transmit data from top left CMOS and matrix to LVDS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitFromTopLeftToLVDS(bool force=false);

  /**
   * @brief Slave config: transmit data from bottom left CMOS and matrix to LVDS
   * @param force Send the command immediately through the slow control if true
   **/
  void SetTransmitFromBotLeftToLVDS(bool force=false);

  /**
   * @brief Mute config: all outputs are disabled and no data is sent from the chip
   * @param force Send the command immediately through the slow control if true
   **/
  void SetMute(bool force=false);

  /**
   * @brief Set slow control register addresses to given values.
   * @param values Give values in order (READ,WRITE). Argument vector must have exactly size 2.
   **/
  void SetSlowControlIO(std::vector<uint32_t> values);

  /**
   * @brief Set slow control IO that corresponds to given chipID. ChipID must first be linked with the LinkChipToSCIO method.
   * @param chipID Chip ID that corresponds to a specific physical chip within a MALTA module. Given in each MALTA word produced by that chip.
   **/
  void SetSCIOToChip(uint32_t chipID);

  /**
   * @brief Link a given chipID (identifies chip in MALTA word, set by jumpers on PCB) to the given slow control register addresses.
   * @param values Give values in order (chipID, READ, WRITE). Argument vector must have exactly size 3.
   **/
  void LinkChipToSCIO(std::vector<uint32_t> values);

  /**
   * @brief Return a vector of all registered chip IDs
   **/
  std::vector<uint32_t> GetListOfChipIDs();

  /**
   * @brief Get the currently set chip ID
   **/
  uint32_t GetCurrentChipID();


 private:

  bool m_verbose;
  bool m_dut;
  ipbus::Uhal * m_ipb;
  uint32_t m_fifoStatus;
  uint32_t m_configModeSleep;
  MaltaSlowControl * m_msc;
  std::vector<uint32_t> m_commands;
  std::map<uint32_t,std::string> m_command_names;

  std::vector<uint32_t> m_mask_row;
  std::vector<uint32_t> m_mask_col;
  std::vector<uint32_t> m_mask_diag;
  std::vector<uint32_t> m_mask_dc;
  std::vector<std::pair<uint32_t,uint32_t>> m_maskedPixelList;
  std::map<uint32_t,std::pair<uint32_t,uint32_t>> m_chipID_to_SCIO;
  std::vector<uint32_t> m_chipIDs;
  uint32_t m_currentChipID;

  std::stringstream systemcall(std::string cmd);

};

#endif
